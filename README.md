# Metagenedb

[![pipeline status](https://gitlab.pasteur.fr/metagenomics/metagenedb/badges/dev/pipeline.svg)](https://gitlab.pasteur.fr/metagenomics/metagenedb/commits/dev)
[![coverage report](https://gitlab.pasteur.fr/metagenomics/metagenedb/badges/dev/coverage.svg)](https://gitlab.pasteur.fr/metagenomics/metagenedb/commits/dev)

## The project

The main motivation behind MetageneDB is to provide a support for all the analysis that are based on gene catalogs.
It is composed of both an API and a client side for visualization and interaction with the DB.

* Graphical interface to browse through the catalog
* REST API to programmatically query and retrieve information from the database
* (not implemented) Interface to perform analysis from gene counts present on the catalog

## Wiki & Documentation

For more information, please have a look at our [Wiki](https://gitlab.pasteur.fr/metagenomics/metagenedb/-/wikis/home)
