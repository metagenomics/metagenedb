#!/usr/bin/env python
import argparse
import logging
import sys
from itertools import islice

from dabeplech import MetageneDBCatalogTaxonomyAPI

from metagenedb.common.utils.parsers import NCBITaxonomyLineParser

logging.basicConfig()
logger = logging.getLogger()

SELECT_RELATED_PARENT = "parent{}".format("__parent" * 40)


class ImportNCBITaxonomy(object):
    METAGENEDB_TAX_API = MetageneDBCatalogTaxonomyAPI
    FOREIGN_KEY_FIELDS = ['parent_tax_id']

    def __init__(self, url, jwt_token, tax_names_file, tax_nodes_file):
        self.metagenedb_tax_api = self.METAGENEDB_TAX_API(base_url=url, jwt_token=jwt_token)
        self.tax_names_file = tax_names_file
        self.tax_nodes_file = tax_nodes_file
        self.total_tax = self._get_number_nodes()
        self._reset_counters()

    def _reset_counters(self):
        self.processed_tax = 0
        self.created_tax = 0
        self.updated_tax = 0
        self.skipped_tax = 0

    def _get_number_nodes(self):
        with open(self.tax_nodes_file) as f:
            for i, l in enumerate(f):
                pass
        return i + 1

    def import_names(self, select_class="scientific name"):
        """
        Build and return a DICT {tax_id: taxe_name} for the chosen select_class
        """
        logger.info("Importing %s from %s...", select_class, self.tax_names_file)
        taxo_name_dict = {}
        with open(self.tax_names_file, "r") as file:
            for line in file:
                if select_class in line:
                    name = NCBITaxonomyLineParser.name(line)
                    taxo_name_dict[name.get('tax_id')] = name.get('name_txt')
        return taxo_name_dict

    def _process_nodes_for_creation(self, nodes, taxo_name_dict):
        for node in nodes:
            node['name'] = taxo_name_dict.get(node['tax_id'], "No name")
            for key in self.FOREIGN_KEY_FIELDS:
                del node[key]
        return nodes

    def create_taxo_nodes(self, taxo_name_dict, chunk_size=1000):
        logger.info("Create taxonomy objects from %s...", self.tax_nodes_file)
        with open(self.tax_nodes_file, "r") as f:
            while True:
                next_nodes = list(islice(f, chunk_size))
                if not next_nodes:
                    break
                nodes = [NCBITaxonomyLineParser.node(i) for i in next_nodes]
                nodes = self._process_nodes_for_creation(nodes, taxo_name_dict)
                response = self.metagenedb_tax_api.put(nodes)
                self.created_tax += response.get('created').get('count')
                self.updated_tax += response.get('updated').get('count')
                self.processed_tax += len(nodes)
                logger.info("%s/%s Taxonomy processed so far...", self.processed_tax, self.total_tax)
        logger.info("[DONE] %s/%s Taxonomy created.", self.created_tax, self.total_tax)
        logger.info("[DONE] %s/%s Taxonomy updated.", self.updated_tax, self.total_tax)
        logger.info("[DONE] %s/%s Taxonomy skipped.", self.skipped_tax, self.total_tax)

    def update_taxo_nodes(self, chunk_size=1000):
        self._reset_counters()
        logger.info("Linking taxonomy objects to direct parental node from %s...", self.tax_nodes_file)
        with open(self.tax_nodes_file, "r") as f:
            while True:
                next_nodes = list(islice(f, chunk_size))
                if not next_nodes:
                    break
                nodes = [NCBITaxonomyLineParser.node(i) for i in next_nodes]
                response = self.metagenedb_tax_api.put(nodes)
                self.created_tax += response.get('created').get('count')
                self.updated_tax += response.get('updated').get('count')
                self.processed_tax += len(nodes)
                logger.info("%s/%s Taxonomy processed so far...", self.processed_tax, self.total_tax)
        logger.info("[DONE] %s/%s Taxonomy created.", self.created_tax, self.total_tax)
        logger.info("[DONE] %s/%s Taxonomy updated.", self.updated_tax, self.total_tax)
        logger.info("[DONE] %s/%s Taxonomy skipped.", self.skipped_tax, self.total_tax)


def parse_arguments():
    """
    Defines parser.
    """
    parser = argparse.ArgumentParser(description='Populate database from a given NCBI taxonomy files.')
    # Common arguments for analysis and annotations
    parser.add_argument('--nodes', help='nodes.dmp file from ncbi_taxonomy', required=True)
    parser.add_argument('--names', help='names.dmp file from ncbi_taxonomy', required=True)
    parser.add_argument('--skip_creation', action='store_true', help='Skip taxonomy creation.')
    parser.add_argument('--url', help='base URL of the instance.', default='http://localhost/')
    parser.add_argument('-t', '--jwt_token', help='your JWT token obtain from web app', required=True)
    parser.add_argument('-v', '--verbose', action='store_true')

    try:
        return parser.parse_args()
    except SystemExit:
        sys.exit(1)


def run():
    args = parse_arguments()
    if args.verbose:
        logger.setLevel(logging.INFO)
    import_ncbi_tax = ImportNCBITaxonomy(args.url, args.jwt_token, args.names, args.nodes)
    taxonomy_names = import_ncbi_tax.import_names()
    if not args.skip_creation:
        import_ncbi_tax.create_taxo_nodes(taxonomy_names)
        import_ncbi_tax.update_taxo_nodes()


if __name__ == "__main__":
    run()
