#!/bin/bash

SCRIPTS_PATH=scripts

if [ -z $PORT ];then PORT=8000;fi  # Need to fix to get value from .env file

python ${SCRIPTS_PATH}/manage.py collectstatic --no-input
python ${SCRIPTS_PATH}/manage.py makemigrations
python ${SCRIPTS_PATH}/manage.py migrate
python ${SCRIPTS_PATH}/manage.py runserver 0.0.0.0:${PORT}
