from setuptools import setup, find_packages

setup(name="metagenedb",
      version="0.0.1",
      description='Organise metagenomics gene catalog and expose API to front apps.',
      author='Kenzo-Hugo Hillion',
      author_email='kehillio@pasteur.fr',
      install_requires=[
          'django>=2.1',
          'psycopg2',
          'djangorestframework'
      ],
      packages=find_packages(),
      scripts=['scripts/manage.py'])
