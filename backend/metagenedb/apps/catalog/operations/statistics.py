from abc import ABC
from collections import defaultdict

from django.db.models import Max

from metagenedb.apps.catalog.models import Gene, Taxonomy
from metagenedb.common.utils.color_generator import generate_color_code
from metagenedb.common.utils.dict import extract_labels_and_values


class Statistics(ABC):
    model = None

    def __init__(self, filters=None):
        if filters is None:
            self.filters = {}
        else:
            self.filters = filters

    def get_queryset(self, filters=None):
        add_filters = filters
        if add_filters is None:
            add_filters = {}
        return self.model.objects.filter(**self.filters).filter(**add_filters)


class GeneStatistics(Statistics):
    model = Gene

    def count_all(self):
        return self.get_queryset().count()

    def count_has_function(self, source=None):
        if source is None:
            all_genes_count = self.get_queryset().count()
            filters = {'functions__isnull': True}
            no_functions_genes_count = self.get_queryset(filters=filters).count()
            return all_genes_count - no_functions_genes_count
        else:
            filters = {'functions__source': source}
        return self.get_queryset(filters=filters).distinct().count()

    def count_has_taxonomy(self):
        filters = {'taxonomy__isnull': False}
        return self.get_queryset(filters=filters).count()

    def count_has_function_has_taxonomy(self):
        filters = {
            'taxonomy__isnull': False,
            'functions__isnull': False
        }
        return self.get_queryset(filters=filters).distinct().count()

    def taxonomy_repartition(self, level="phylum"):
        queryset = self.get_queryset().select_related(f'taxonomy__{level}')
        filter_no_annotation = {f"taxonomy__hierarchy__{level}__isnull": True}
        filter_annotation = {f"taxonomy__hierarchy__{level}__isnull": False}
        value_to_retrieve = f'taxonomy__hierarchy__{level}__name'
        taxonomy_counts = defaultdict(lambda: 0)
        taxonomy_counts['No annotation'] = queryset.filter(**filter_no_annotation).values().count()
        if taxonomy_counts['No annotation'] == 0:
            del taxonomy_counts['No annotation']
        for value in queryset.filter(**filter_annotation).values(value_to_retrieve):
            tax_name = value[value_to_retrieve]
            taxonomy_counts[tax_name] += 1
        results = extract_labels_and_values(taxonomy_counts, sort=True, reverse=True)
        colors = [generate_color_code(label) for label in results[0]]
        return {
            'labels': results[0],
            'colors': colors,
            'counts': results[1],
        }

    def present_taxonomy(self, level="phylum"):
        queryset = self.get_queryset().select_related(f'taxonomy__{level}')
        filter_annotation = {f"taxonomy__hierarchy__{level}__isnull": False}
        value_to_retrieve = f'taxonomy__hierarchy__{level}__tax_id'
        all_tax_ids = [el[0] for el in queryset.filter(**filter_annotation).values_list(value_to_retrieve)]
        all_unique_tax_ids = list(set(all_tax_ids))
        results = {
            'tax_ids': {}
        }
        for taxonomy in Taxonomy.objects.filter(tax_id__in=all_unique_tax_ids):
            results['tax_ids'][taxonomy.name] = taxonomy.tax_id
        return results


class GeneLengthDistribution(Statistics):
    model = Gene

    def __init__(self, window_size=100, stop_at=5000, filters=None, counts_key='counts', windows_key='labels'):
        self.base_window_size = window_size
        self.base_stop_at = stop_at
        self.current_stop_at = stop_at
        if filters is None:
            filters = {}
        self.filters = filters
        self.counts_key = counts_key
        self.windows_key = windows_key

    def _update_first_window(self, window_name):
        return f"<{window_name.split('-')[1]}"

    def _update_last_window(self, window_name):
        return f">{window_name.split('-')[0]}"

    def _update_first_last_windows(self, windows):
        windows[0] = self._update_first_window(windows[0])
        windows[-1] = self._update_last_window(windows[-1])
        return windows

    def _perform_query(self):
        """
        Count how many gene by window of gene length.
        """
        if self.filters:
            queryset = self.get_queryset(filters=self.filters).distinct().only('length')
        else:
            queryset = self.get_queryset().only('length')
        length_max = queryset.aggregate(Max('length')).get('length__max', 0)
        self.current_stop_at = length_max if length_max < self.base_stop_at else self.base_stop_at
        all_ranges = [[i, i + self.base_window_size] for i in range(0, self.current_stop_at + 1, self.base_window_size)]
        all_ranges[-1][1] = length_max + 1  # last should contain all above the stop_at
        data = []
        windows = []
        for rg in all_ranges:
            windows.append(f"{rg[0]/1000}k-{rg[1]/1000}k")
            data.append(queryset.filter(length__gte=rg[0], length__lt=rg[1]).count())
        # Change labels
        windows = self._update_first_last_windows(windows)
        self._counts = data
        self._windows = windows

    @property
    def counts(self):
        if getattr(self, '_counts', None) is None:
            self._perform_query()
        return self._counts

    @property
    def windows(self):
        if getattr(self, '_windows', None) is None:
            self._perform_query()
        return self._windows

    def validate_arguments(self, window_size, stop_at):
        if window_size < self.base_window_size or window_size % self.base_window_size != 0:
            raise Exception(f"window_size has to be >= {self.base_window_size} and a multiple of it.")
        if stop_at > self.base_stop_at or stop_at % window_size != 0:
            raise Exception(f"stop_at needs be <= {self.base_stop_at} and a multiple of {window_size}.")

    def _reduce_stop_at(self, counts, windows, window_size, stop_at):
        indice_stop_at = int(stop_at / window_size)
        new_last_window = self._update_last_window(windows[indice_stop_at])
        return {
            self.counts_key: counts[:indice_stop_at] + [sum(counts[indice_stop_at:])],
            self.windows_key: windows[:indice_stop_at] + [new_last_window]
        }

    def _group_by_different_window_size(self, counts, windows, window_size):
        group_nb = window_size // self.base_window_size
        new_counts = []
        new_windows = []
        for i in range(0, len(counts), group_nb):
            new_counts.append(sum(counts[i:i + group_nb]))
            new_windows.append(
                f"{(self.base_window_size * i)/1000}k-{(self.base_window_size * (i + group_nb))/1000}k"
            )
        return {
            self.counts_key: new_counts,
            self.windows_key: self._update_first_last_windows(new_windows)
        }

    def _handle_diff_window_diff_stop_at(self, counts, windows, window_size, stop_at):
        new_group = self._group_by_different_window_size(counts, windows, window_size)
        new_stop_at = self._reduce_stop_at(
            new_group[self.counts_key], new_group[self.windows_key], window_size, stop_at
        )
        return new_stop_at

    def _format_dict(self, window_size, stop_at):
        if window_size == self.base_window_size:
            if stop_at >= self.current_stop_at:
                return {
                    self.counts_key: self.counts,
                    self.windows_key: self.windows
                }
            return self._reduce_stop_at(self.counts, self.windows, window_size, stop_at)
        elif stop_at >= self.current_stop_at:
            return self._group_by_different_window_size(self.counts, self.windows, window_size)
        else:
            return self._handle_diff_window_diff_stop_at(self.counts, self.windows, window_size, stop_at)

    def get_distribution(self, window_size=100, stop_at=5000):
        if not self.get_queryset().exists():
            return {
                'counts': [],
                'labels': []
            }
        self.validate_arguments(window_size, stop_at)
        return self._format_dict(window_size, stop_at)
