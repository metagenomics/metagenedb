from rest_framework.test import APITestCase

from metagenedb.common.utils.color_generator import generate_color_code
from metagenedb.apps.catalog.factory import (
    GeneFactory, GeneWithEggNOGFactory, GeneWithKeggFactory, GeneWithTaxonomyFactory, TaxonomyFactory
)

from .statistics import GeneStatistics, GeneLengthDistribution


class BaseTestGeneStatistics(APITestCase):

    def setUp(self):
        self.gene_stats = GeneStatistics()


class BaseTestTaxonomy(BaseTestGeneStatistics):

    @classmethod
    def setUpTestData(cls):
        cls.parent_root = TaxonomyFactory(rank="root")
        cls.phylum = TaxonomyFactory(rank='phylum')
        cls.phylum.parent = cls.parent_root
        cls.phylum.save()
        cls.phylum.build_hierarchy()
        cls.class_tax = TaxonomyFactory(rank='class')
        cls.class_tax.parent = cls.phylum
        cls.class_tax.save()
        cls.class_tax.build_hierarchy()


class TestTaxonomyRepartition(BaseTestTaxonomy):

    def test_taxonomy_counts_no_content(self):
        expected_dict = {
            'labels': [],
            'counts': [],
            'colors': []
        }
        self.assertDictEqual(self.gene_stats.taxonomy_repartition(), expected_dict)

    def test_taxonom_counts_no_annotation(self):
        gene = GeneFactory.create()  # noqa
        expected_dict = {
            'labels': ['No annotation'],
            'counts': [1],
            'colors': [generate_color_code('No annotation')]
        }
        self.assertDictEqual(self.gene_stats.taxonomy_repartition(), expected_dict)

    def test_taxonomy_repartition(self):
        gene = GeneFactory.create(taxonomy=self.phylum)  # noqa
        expected_dict = {
            'labels': [self.phylum.name],
            'counts': [1],
            'colors': [generate_color_code(self.phylum.name)]
        }
        self.assertDictEqual(self.gene_stats.taxonomy_repartition(), expected_dict)

    def test_taxonomy_counts_class_level(self):
        gene = GeneFactory.create(taxonomy=self.class_tax)  # noqa
        expected_dict = {
            'labels': [self.class_tax.name],
            'counts': [1],
            'colors': [generate_color_code(self.class_tax.name)]
        }
        self.assertDictEqual(self.gene_stats.taxonomy_repartition(level='class'), expected_dict)


class TestPresentTaxonomy(BaseTestTaxonomy):

    def test_present_taxonomy_no_content(self):
        expected_dict = {
            'tax_ids': {},
        }
        self.assertDictEqual(self.gene_stats.present_taxonomy(), expected_dict)

    def test_present_taxonomy(self):
        gene = GeneFactory.create(taxonomy=self.phylum)  # noqa
        expected_dict = {
            'tax_ids': {
                self.phylum.name: self.phylum.tax_id
            }
        }
        self.assertDictEqual(self.gene_stats.present_taxonomy(), expected_dict)

    def test_present_taxonomy_multiple_genes(self):
        # Create 10 genes with the same taxonomy
        GeneFactory.create_batch(10, taxonomy=self.phylum)  # noqa
        expected_dict = {
            'tax_ids': {
                self.phylum.name: self.phylum.tax_id
            }
        }
        self.assertDictEqual(self.gene_stats.present_taxonomy(), expected_dict)

    def test_taxonomy_counts_class_level(self):
        gene = GeneFactory.create(taxonomy=self.class_tax)  # noqa
        expected_dict = {
            'tax_ids': {
                self.class_tax.name: self.class_tax.tax_id
            }
        }
        self.assertDictEqual(self.gene_stats.present_taxonomy(level='class'), expected_dict)


class TestCounts(BaseTestGeneStatistics):

    @classmethod
    def setUpTestData(cls):
        """
        Creation of 20 genes:
          - 5 without functions
          - 5 with KEGG functions
          - 10 with EggNOG functions

        They all have a taxonomy
        """
        cls.genes_no_function = GeneWithTaxonomyFactory.create_batch(5)
        cls.keggs = GeneWithKeggFactory.create_batch(5)
        cls.eggnogs = GeneWithEggNOGFactory.create_batch(10)

    def test_count(self):
        self.assertEqual(self.gene_stats.count_all(), 20)

    def test_has_taxonomy(self):
        self.assertEqual(self.gene_stats.count_has_taxonomy(), 5)

    def test_count_has_function(self):
        self.assertEqual(self.gene_stats.count_has_function(), 15)

    def test_count_has_function_kegg(self):
        self.assertEqual(self.gene_stats.count_has_function(source='kegg'), 5)

    def test_count_has_function_eggnog(self):
        self.assertEqual(self.gene_stats.count_has_function(source='eggnog'), 10)

    def test_count_has_function_wrong(self):
        self.assertEqual(self.gene_stats.count_has_function(source='wrong_source'), 0)

    def test_count_has_taxonomy_has_function(self):
        self.assertEqual(self.gene_stats.count_has_function_has_taxonomy(), 0)


class TestCountWindows(APITestCase):

    def test_gene_length_no_content(self):
        gene_stats = GeneLengthDistribution()
        expected_dict = {
            'counts': [],
            'labels': []
        }
        self.assertDictEqual(gene_stats.get_distribution(), expected_dict)

    def test_get_distribution(self):
        for i in range(100, 400, 60):
            GeneFactory.create(length=i)
        gene_stats = GeneLengthDistribution()
        expected_dict = {
            'counts': [0, 2, 2, 1],
            'labels': ['<0.1k', '0.1k-0.2k', '0.2k-0.3k', '>0.3k']
        }
        self.assertDictEqual(gene_stats.get_distribution(), expected_dict)

    def test_gene_length_stop_at_200(self):
        for i in range(100, 400, 60):
            GeneFactory.create(length=i)
        gene_stats = GeneLengthDistribution()
        expected_dict = {
            'counts': [0, 2, 3],
            'labels': ['<0.1k', '0.1k-0.2k', '>0.2k']
        }
        self.assertDictEqual(gene_stats.get_distribution(stop_at=200), expected_dict)

    def test_gene_length_window_size_200(self):
        for i in range(100, 400, 60):
            GeneFactory.create(length=i)
        gene_stats = GeneLengthDistribution()
        expected_dict = {
            'counts': [2, 3],
            'labels': ['<0.2k', '>0.2k']
        }
        self.assertDictEqual(gene_stats.get_distribution(window_size=200), expected_dict)

    def test_gene_length_window_size_200_stop_at_200(self):
        for i in range(100, 500, 60):
            GeneFactory.create(length=i)
        gene_stats = GeneLengthDistribution()
        expected_dict = {
            'counts': [2, 5],
            'labels': ['<0.2k', '>0.2k']
        }
        self.assertDictEqual(gene_stats.get_distribution(window_size=200, stop_at=200), expected_dict)

    def test_gene_length_with_functions(self):
        for i in range(100, 400, 60):
            GeneFactory.create(length=i)
            GeneWithKeggFactory(length=i)
        gene_stats = GeneLengthDistribution()
        expected_dict = {
            'counts': [0, 4, 4, 2],
            'labels': ['<0.1k', '0.1k-0.2k', '0.2k-0.3k', '>0.3k']
        }
        self.assertDictEqual(gene_stats.get_distribution(), expected_dict)
        filters = {
            'functions__isnull': False
        }
        gene_stats = GeneLengthDistribution(filters=filters)
        expected_dict = {
            'counts': [0, 2, 2, 1],
            'labels': ['<0.1k', '0.1k-0.2k', '0.2k-0.3k', '>0.3k']
        }
        self.assertDictEqual(gene_stats.get_distribution(), expected_dict)
