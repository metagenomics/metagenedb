from rest_framework import serializers
from metagenedb.apps.catalog.models import Taxonomy

from .asymetricslugrelatedfield import AsymetricSlugRelatedField
from .bulk_list import BulkListSerializer


class SimpleTaxonomySerializer(serializers.ModelSerializer):

    class Meta:
        model = Taxonomy
        fields = ('tax_id', 'name', 'rank')


class TaxonomyHierarchySerializer(serializers.ModelSerializer):

    class Meta:
        model = Taxonomy
        fields = ('rank', 'name', 'parent')


class TaxonomyListSerializer(BulkListSerializer):

    class Meta:
        model = Taxonomy


class TaxonomySerializer(serializers.ModelSerializer):
    rank = serializers.CharField(required=False)
    parent_tax_id = AsymetricSlugRelatedField.from_serializer(
        SimpleTaxonomySerializer,
        queryset=Taxonomy.objects.all(),
        slug_field='tax_id',
        source='parent',
        required=False,
    )

    class Meta:
        model = Taxonomy
        list_serializer_class = TaxonomyListSerializer
        fields = (
            'tax_id', 'name', 'rank', 'parent_tax_id', 'hierarchy'
        )
