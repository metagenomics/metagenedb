from copy import deepcopy
from unittest import TestCase
from unittest.mock import Mock

from rest_framework.test import APITestCase
from rest_framework.exceptions import ValidationError

from metagenedb.apps.catalog.serializers import FunctionSerializer
from metagenedb.apps.catalog.serializers.bulk_list import BulkListSerializer
from metagenedb.apps.catalog.factory import FunctionFactory
from metagenedb.common.utils.mocks.metagenedb import MetageneDBCatalogFunctionAPIMock


class BulkListSerializerTestExtractManyToMany(BulkListSerializer):
    """
    overload to skip __init__() and just test the _extract_many_to_many method.
    """

    def __init__(self):
        pass


class BaseTestBulkListSerializerMethods(TestCase):

    def setUp(self):
        self.data = [
            {'id': 'entry_1', 'field1': 'value1', 'field2': 'value2'},
            {'id': 'entry_2', 'field1': 'value3', 'field2': 'value4'}
        ]
        self.bulk_list_serializer = BulkListSerializerTestExtractManyToMany()
        self.info = Mock()


class TestExtractManyToMany(BaseTestBulkListSerializerMethods):

    def test_extract_many_to_many(self):
        self.info.relations = {
            'field1': Mock(to_many=True),
            'field2': Mock(to_many=False)
        }
        ori_list = deepcopy(self.data)
        expected_dict = {
            'keys': {'field1'},
            'values': {
                'field1': [
                    {'field1': 'value1', 'id': 'entry_1'},
                    {'field1': 'value3', 'id': 'entry_2'}
                ]
            }
        }
        tested_dict = self.bulk_list_serializer._extract_many_to_many(self.data, self.info, 'id')
        self.assertDictEqual(tested_dict, expected_dict)
        self.assertNotEqual(ori_list, self.data)

    def test_extract_many_to_many_two_fields(self):
        self.info.relations = {
            'field1': Mock(to_many=True),
            'field2': Mock(to_many=True)
        }
        ori_list = deepcopy(self.data)
        expected_dict = {
            'keys': {'field1', 'field2'},
            'values': {
                'field1': [
                    {'field1': 'value1', 'id': 'entry_1'},
                    {'field1': 'value3', 'id': 'entry_2'}
                ],
                'field2': [
                    {'field2': 'value2', 'id': 'entry_1'},
                    {'field2': 'value4', 'id': 'entry_2'}
                ]
            }
        }
        tested_dict = self.bulk_list_serializer._extract_many_to_many(self.data, self.info, 'id')
        self.assertDictEqual(tested_dict, expected_dict)
        self.assertNotEqual(ori_list, self.data)

    def test_extract_no_many_to_many(self):
        self.info.relations = {
            'field1': Mock(to_many=False),
            'field2': Mock(to_many=False)
        }
        ori_list = deepcopy(self.data)
        expected_dict = {
            'keys': set(),
            'values': {}
        }
        tested_dict = self.bulk_list_serializer._extract_many_to_many(self.data, self.info, 'id')
        self.assertDictEqual(tested_dict, expected_dict)
        self.assertListEqual(ori_list, self.data)


class TestGetAllKeyFields(BaseTestBulkListSerializerMethods):

    def test_get_all_key_fields(self):
        expected_keys = ['field1', 'field2']
        tested_keys = self.bulk_list_serializer._get_all_key_fields(self.data)
        for key in expected_keys:
            self.assertIn(key, tested_keys)


class TestGetDbIndexFields(BaseTestBulkListSerializerMethods):

    def test_get_db_index_fields(self):
        self.info.fields = {
            'field1': Mock(db_index=True),
            'field2': Mock(db_index=False)
        }
        expected_keys = ['field1']
        tested_keys = self.bulk_list_serializer._get_db_index_fields(self.info)
        self.assertListEqual(tested_keys, expected_keys)

    def test_get_db_index_fields_no_keys(self):
        self.info.fields = {
            'field1': Mock(db_index=False),
            'field2': Mock(db_index=False)
        }
        expected_keys = []
        tested_keys = self.bulk_list_serializer._get_db_index_fields(self.info)
        self.assertListEqual(tested_keys, expected_keys)


class TestOperationsBulk(APITestCase):
    """
    We are going to use a Serializer based on a real model since there are interactions with the DB.
    We are using the FunctionListSerializer for the moment (through FunctionSerializer(many=True)).
    """

    def setUp(self):
        self.function_api = MetageneDBCatalogFunctionAPIMock(self.client)

    def test_create_functions(self):
        validated_data = [
            {
                "function_id": "k_test1",
                "source": "kegg",
                "name": "Kegg Test 1"
            },
            {
                "function_id": "k_test2",
                "source": "kegg",
                "name": "Kegg Test 2"
            }
        ]
        serializer = FunctionSerializer(many=True)
        tested_instances = serializer.create(validated_data)
        self.assertEqual(self.function_api.get_all()['count'], 2)
        self.assertEqual(len(tested_instances), len(validated_data))

    def test_update_existing_functions(self):
        functions = FunctionFactory.create_batch(2)
        validated_data = [
            {
                "function_id": functions[0].function_id,
                "source": functions[0].source,
                "name": "Test 1"
            },
            {
                "function_id": functions[1].function_id,
                "source": functions[1].source,
                "name": "Test 2"
            }
        ]
        functions = {item.function_id: item for item in functions}
        for data in validated_data:
            self.assertNotEqual(self.function_api.get(data['function_id']), data)
        serializer = FunctionSerializer(many=True)
        serializer.update(functions, deepcopy(validated_data))
        self.assertEqual(self.function_api.get_all()['count'], 2)
        for data in validated_data:
            self.assertDictEqual(self.function_api.get(data['function_id']), data)

    def test_to_internal_value_duplicate(self):
        functions = FunctionFactory.build_batch(2)
        data = [
            {
                "function_id": functions[0].function_id,
                "source": functions[0].source,
                "name": "Test 1"
            },
            {
                "function_id": functions[0].function_id,
                "source": functions[0].source,
                "name": "Test 1"
            },
            {
                "function_id": functions[1].function_id,
                "source": functions[1].source,
                "name": "Test 2"
            }
        ]
        serializer = FunctionSerializer(many=True)
        with self.assertRaises(ValidationError):
            serializer.to_internal_value(data)
