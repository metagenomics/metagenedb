from .function import EggNOGSerializer, FunctionSerializer, KeggOrthologySerializer  # noqa
from .gene import GeneSerializer  # noqa
from .statistics import StatisticsSerializer  # noqa
from .taxonomy import TaxonomySerializer  # noqa
