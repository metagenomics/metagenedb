from rest_framework import serializers
from metagenedb.apps.catalog.models import (
    EggNOG, EggNogFunctionalCategory, Function, KeggOrthology
)

from .asymetricslugrelatedfield import AsymetricSlugRelatedField
from .bulk_list import BulkListSerializer


class FunctionListSerializer(BulkListSerializer):

    class Meta:
        model = Function


class FunctionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Function
        list_serializer_class = FunctionListSerializer
        fields = ('function_id', 'source', 'name')


class EggNOGListSerializer(BulkListSerializer):

    class Meta:
        model = EggNOG


class EggNogFunctionalCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = EggNogFunctionalCategory
        fields = ('category_id', 'name', 'group')


class EggNOGSerializer(serializers.ModelSerializer):
    functional_categories = AsymetricSlugRelatedField.from_serializer(
        EggNogFunctionalCategorySerializer,
        queryset=EggNogFunctionalCategory.objects.all(),
        slug_field='category_id',
        many=True,
        required=False,
    )

    class Meta:
        model = EggNOG
        list_serializer_class = EggNOGListSerializer
        fields = ('function_id', 'name', 'functional_categories', 'version')


class KeggOrthologyListSerializer(BulkListSerializer):

    class Meta:
        model = KeggOrthology


class KeggOrthologySerializer(serializers.ModelSerializer):
    ec_number = serializers.CharField(required=False)

    class Meta:
        model = KeggOrthology
        list_serializer_class = KeggOrthologyListSerializer
        fields = ('function_id', 'name', 'long_name', 'ec_number')
