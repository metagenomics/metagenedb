from unittest import TestCase

from rest_framework.test import APITestCase

from metagenedb.apps.catalog.factory import FunctionFactory, GeneFactory
from metagenedb.apps.catalog.models import GeneFunction
from metagenedb.apps.catalog.serializers.gene import GeneListSerializer


class GeneListSerializerTest(GeneListSerializer):
    """
    overload to skip __init__() and just test the _extract_many_to_many method.
    """

    def __init__(self):
        pass


class TestGenerateGeneFunctionMapping(TestCase):

    def test_generate_gene_function_mapping(self):
        genes = [GeneFactory.build(gene_id='gene_1')]
        values = [{
            'gene_id': 'gene_1',
            'functions': [FunctionFactory.build()]
        }]
        expected_object = GeneFunction(gene=genes[0], function=values[0]['functions'][0])
        serializer = GeneListSerializerTest()
        tested_object = serializer._generate_gene_function_mapping(values, genes)[0]
        self.assertEqual(tested_object.gene, expected_object.gene)
        self.assertEqual(tested_object.function, expected_object.function)


class TestHandleFunctions(APITestCase):

    def test_handle_functions(self):
        """
        @TODO function in the gene object is not found but the link is created
        """
        gene = GeneFactory.create()
        function = FunctionFactory.create()
        values = [{
            'gene_id': gene.gene_id,
            'functions': [function]
        }]
        self.assertNotEqual(gene.functions, [function])
        self.assertEqual(GeneFunction.objects.all().count(), 0)
        serializer = GeneListSerializerTest()
        serializer._handle_functions(values)
        link = GeneFunction.objects.all()
        self.assertEqual(link.count(), 1)
        self.assertEqual(link[0].gene.gene_id, gene.gene_id)
        self.assertEqual(link[0].function.function_id, function.function_id)
