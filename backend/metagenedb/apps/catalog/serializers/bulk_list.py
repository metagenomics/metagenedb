from collections import defaultdict

from rest_framework import serializers
from rest_framework.exceptions import ValidationError, ErrorDetail
from rest_framework.fields import SkipField
from rest_framework.settings import api_settings
from rest_framework.utils import html, model_meta


class BulkListSerializer(serializers.ListSerializer):

    def _extract_many_to_many(self, validated_data, info, lookup_field):
        many_to_many = {
            'keys': set(),
            'values': defaultdict(list)
        }
        for field_name, relation_info in info.relations.items():
            if relation_info.to_many:
                for data_item in validated_data:
                    if field_name in data_item:
                        many_to_many['keys'].add(field_name)
                        many_to_many['values'][field_name].append({
                            field_name: data_item.pop(field_name),
                            lookup_field: data_item[lookup_field]
                        })
        return many_to_many

    def _get_db_index_fields(self, info):
        db_index_keys = []
        for field_name, field in info.fields.items():
            if field.db_index:
                db_index_keys.append(field_name)
        return db_index_keys

    def _get_all_key_fields(self, validated_data):
        """
        Retrieve all keys that are within the payload.

        :return: list of keys found in payload.
        :rtype: List
        """
        all_keys = []
        for validated_data_element in validated_data:
            for key in validated_data_element.keys():
                all_keys.append(key)
        return list(set(all_keys))

    def to_internal_value(self, data):
        """
        Copy of original method to be overloaded when performing put to instances
        List of dicts of native values <- List of dicts of primitive datatypes.
        """
        ModelClass = self.Meta.model
        info = model_meta.get_field_info(ModelClass)
        db_index_fields = self._get_db_index_fields(info)
        lookup_field = db_index_fields[0]

        if html.is_html_input(data):
            data = html.parse_html_list(data, default=[])

        if not isinstance(data, list):
            message = self.error_messages['not_a_list'].format(
                input_type=type(data).__name__
            )
            raise ValidationError({
                api_settings.NON_FIELD_ERRORS_KEY: [message]
            }, code='not_a_list')

        if not self.allow_empty and len(data) == 0:
            if self.parent and self.partial:
                raise SkipField()

            message = self.error_messages['empty']
            raise ValidationError({
                api_settings.NON_FIELD_ERRORS_KEY: [message]
            }, code='empty')

        ret = []
        errors = []
        primary_keys = set()
        for item in data:
            try:
                if isinstance(self.instance, dict):
                    self.child.instance = self.instance[item[lookup_field]]
                    validated = self.child.run_validation(item)
                else:
                    validated = self.child.run_validation(item)
                if item[lookup_field] not in primary_keys:
                    primary_keys.add(item[lookup_field])
                else:
                    errors.append(ErrorDetail(item[lookup_field], code='duplicate'))
            except ValidationError as exc:
                errors.append(exc.detail)
            else:
                ret.append(validated)
                errors.append({})

        if any(errors):
            raise ValidationError(errors)

        return ret

    def create(self, validated_data):
        ModelClass = self.Meta.model
        info = model_meta.get_field_info(ModelClass)
        lookup_field = self._get_db_index_fields(info)[0]
        many_to_many = self._extract_many_to_many(validated_data, info, lookup_field)
        instances = ModelClass.objects.bulk_create(
            [ModelClass(**item) for item in validated_data]
        )
        if many_to_many:
            for field in many_to_many['keys']:
                getattr(self, f'_handle_{field}', None)(many_to_many['values'][field])
        return instances

    def update(self, instances, validated_data):
        """
        :param instances: instances to update
        :type instances: DICT of instance object
        """
        ModelClass = self.Meta.model
        info = model_meta.get_field_info(ModelClass)
        lookup_field = self._get_db_index_fields(info)[0]
        many_to_many = self._extract_many_to_many(validated_data, info, lookup_field)
        updated_keys = self._get_all_key_fields(validated_data)
        validated_data = {item.pop(lookup_field): item for item in validated_data}
        for item_id, validated_data_element in validated_data.items():
            for key, value in validated_data_element.items():
                setattr(instances[item_id], key, value)
        ModelClass.objects.bulk_update(
            list(instances.values()),
            updated_keys
        )
        # Link existing many-to-many relationships.
        if many_to_many:
            for field in many_to_many['keys']:
                getattr(self, f'_handle_{field}', None)(many_to_many['values'][field])
        return list(instances.values())

    class Meta:
        model = NotImplemented
