import logging
import traceback

from rest_framework import serializers
from rest_framework.utils import model_meta
from metagenedb.apps.catalog.models import Function, Gene, GeneFunction, Taxonomy

from .asymetricslugrelatedfield import AsymetricSlugRelatedField
from .bulk_list import BulkListSerializer
from .function import FunctionSerializer
from .taxonomy import SimpleTaxonomySerializer


_LOGGER = logging.getLogger(__name__)


class GeneListSerializer(BulkListSerializer):

    class Meta:
        model = Gene

    def _generate_gene_function_mapping(self, values, genes):
        """
        Generate a list of GeneFunction pair to create relation between them
        """
        genes_dict = {gene.gene_id: gene for gene in genes}
        mapping = []
        for value in values:
            for function in value['functions']:
                mapping.append(GeneFunction(gene=genes_dict[value['gene_id']],
                                            function=function))
        return mapping

    def _handle_functions(self, values):
        """
        :param values: each dictionnary has the 'functions' and 'gene_id' keys
        :type values: LIST of DICT
        """
        # Get all Gene objects
        gene_ids = [item['gene_id'] for item in values]
        # Get all link with corresponding genes & Delete them
        genes = Gene.objects.filter(gene_id__in=gene_ids)
        GeneFunction.objects.filter(gene__in=genes).delete()
        # Generate table for bulk_create of function <-> gene and create it
        GeneFunction.objects.bulk_create(
            self._generate_gene_function_mapping(values, genes)
        )

    def create(self, validated_data):
        instances = super().create(validated_data)
        return instances


class GeneSerializer(serializers.ModelSerializer):
    functions = AsymetricSlugRelatedField.from_serializer(
        FunctionSerializer,
        queryset=Function.objects.all(),
        slug_field='function_id',
        many=True,
        required=False,
    )
    taxonomy = AsymetricSlugRelatedField.from_serializer(
        SimpleTaxonomySerializer,
        queryset=Taxonomy.objects.all(),
        slug_field='tax_id',
        required=False,
    )

    class Meta:
        model = Gene
        list_serializer_class = GeneListSerializer
        fields = ('gene_id', 'name', 'length', 'functions', 'taxonomy', 'sequence', 'source')

    def _extract_many_to_many(self, validated_data, info):
        many_to_many = {}
        for field_name, relation_info in info.relations.items():
            if relation_info.to_many and (field_name in validated_data):
                many_to_many[field_name] = validated_data.pop(field_name)
        return many_to_many

    def _handle_functions(self, functions, instance):
        for function in functions:
            try:
                function = Function.objects.get(function_id=function)
                instance.functions.add(function)
                instance.full_clean()
                instance.save()
            except Function.DoesNotExist:
                _LOGGER.warning("%s not found for %s. Function ignored", function, instance.gene_id)

    def create(self, validated_data):
        ModelClass = self.Meta.model
        info = model_meta.get_field_info(ModelClass)
        # Remove many-to-many relationships from validated_data.
        many_to_many = self._extract_many_to_many(validated_data, info)

        try:
            instance = ModelClass._default_manager.create(**validated_data)
        except TypeError:
            tb = traceback.format_exc()
            msg = (
                'Got a `TypeError` when calling `%s.%s.create()`. '
                'This may be because you have a writable field on the '
                'serializer class that is not a valid argument to '
                '`%s.%s.create()`. You may need to make the field '
                'read-only, or override the %s.create() method to handle '
                'this correctly.\nOriginal exception was:\n %s' %
                (
                    ModelClass.__name__,
                    ModelClass._default_manager.name,
                    ModelClass.__name__,
                    ModelClass._default_manager.name,
                    self.__class__.__name__,
                    tb
                )
            )
            raise TypeError(msg)

        # Link existing many-to-many relationships after the instance is created.
        if many_to_many:
            for field_name, value in many_to_many.items():
                getattr(self, f'_handle_{field_name}', None)(value, instance)
        return instance

    def update(self, instance, validated_data):
        ModelClass = self.Meta.model
        info = model_meta.get_field_info(ModelClass)
        # Remove many-to-many relationships from validated_data.
        many_to_many = self._extract_many_to_many(validated_data, info)

        for attr, value in validated_data.items():
            if attr in info.relations and info.relations[attr].to_many:
                field = getattr(instance, attr)
                field.set(value)
            else:
                setattr(instance, attr, value)

        # Link existing many-to-many relationships.
        if many_to_many:
            for field_name, value in many_to_many.items():
                getattr(self, f'_handle_{field_name}', None)(value, instance)
        instance.save()

        return instance
