from rest_framework import serializers
from metagenedb.apps.catalog.models import Statistics


class StatisticsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Statistics
        fields = ('stats_id', 'body')
