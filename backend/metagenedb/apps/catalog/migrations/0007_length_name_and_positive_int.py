# Generated by Django 2.2.1 on 2019-08-07 14:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0006_gene_taxonomy'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gene',
            name='gene_length',
            field=models.PositiveIntegerField(),
        ),
        migrations.RenameField(
            model_name='gene',
            old_name='gene_length',
            new_name='length',
        ),
    ]
