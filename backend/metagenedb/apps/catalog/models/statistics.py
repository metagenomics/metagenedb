from django.db import models


class Statistics(models.Model):
    """
    Model for the different static statistics that can be computed in a daily manner about the catalog
    """

    stats_id = models.SlugField(max_length=400, db_index=True, unique=True)
    body = models.JSONField()

    class Meta:
        verbose_name_plural = "Statistics"
