from itertools import repeat

from django.db import models

from .function import Function


class Gene(models.Model):
    UNDEFINED = 'undef'
    IGC = 'igc'
    VIRGO = 'virgo'
    SOURCE_CHOICES = [
        (UNDEFINED, 'Undefined'),
        (IGC, 'IGC'),
        (VIRGO, 'Virgo'),
    ]
    CSV_HEADER = ','.join([
        'gene_id', 'gene_name', 'gene_source', 'length',
        'tax_id', 'tax_name', 'tax_rank', 'tax_full',
        'kegg_id', 'eggnog_id',
    ])

    gene_id = models.SlugField(max_length=100, db_index=True, unique=True)
    name = models.CharField(max_length=100, unique=True)
    sequence = models.TextField(blank=True)
    length = models.PositiveIntegerField(db_index=True)
    functions = models.ManyToManyField(Function, db_index=True, through='GeneFunction')
    taxonomy = models.ForeignKey(
        'Taxonomy', related_name='genes',
        on_delete=models.SET_NULL,
        null=True, blank=True
    )
    source = models.CharField(max_length=10, db_index=True, choices=SOURCE_CHOICES, default=UNDEFINED)

    def __str__(self) -> str:
        return self.gene_id

    @property
    def fasta(self) -> str:
        return f">{self.gene_id}\n{self.sequence}\n"

    @property
    def csv_header(self) -> str:
        return self.CSV_HEADER

    @property
    def csv_gene(self) -> str:
        return ",".join([
            self.name, self.source, str(self.length),
        ])

    @property
    def csv_tax(self) -> str:
        if self.taxonomy is None:
            return ",".join(list(repeat('', 4)))
        else:
            return self.taxonomy.csv

    @property
    def csv_functions(self) -> str:
        if not self.functions.all():
            function_list = list(repeat('', 2))
        else:

            function_ids = {
                'kegg': [],
                'eggnog': []
            }
            for function in self.functions.all():
                function_ids.get(function.source).append(function.function_id)
            function_list = [
                ';'.join(function_ids['kegg']),
                ';'.join(function_ids['eggnog'])
            ]
        return ",".join(function_list)

    @property
    def csv(self) -> str:
        return ",".join(
            [self.gene_id, self.csv_gene, self.csv_tax, self.csv_functions]
        )

    class Meta:
        ordering = ['-gene_id']


class GeneFunction(models.Model):
    gene = models.ForeignKey(Gene, on_delete=models.CASCADE)
    function = models.ForeignKey(Function, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.gene.gene_id} <-> {self.function.function_id}"

    class Meta:
        unique_together = [
            'gene', 'function'
        ]
