from django.db import models


class Taxonomy(models.Model):
    """
    Taxonomy is based on NCBI taxonomy: https://www.ncbi.nlm.nih.gov/taxonomy
    """
    NAME_DEFAULT = "No scientific name"
    RANK_CHOICES = [
        ('infraclass', 'Infraclass'),
        ('class', 'Class'),
        ('forma', 'Forma'),
        ('phylum', 'Phylum'),
        ('species_subgroup', 'Species subgroup'),
        ('genus', 'Genus'),
        ('parvorder', 'Parvorder'),
        ('subcohort', 'Subcohort'),
        ('subtribe', 'Subtribe'),
        ('superphylum', 'Superphylum'),
        ('subgenus', 'Subgenus'),
        ('superorder', 'Superorder'),
        ('species', 'Species'),
        ('subphylum', 'Subphylum'),
        ('infraorder', 'Infraorder'),
        ('section', 'Section'),
        ('tribe', 'Tribe'),
        ('cohort', 'Cohort'),
        ('subsection', 'Subsection'),
        ('series', 'Series'),
        ('order', 'Order'),
        ('subclass', 'Subclass'),
        ('superfamily', 'Superfamily'),
        ('superclass', 'Superclass'),
        ('superkingdom', 'Superkingdom'),
        ('kingdom', 'Kingdom'),
        ('family', 'Family'),
        ('suborder', 'Suborder'),
        ('subkingdom', 'Subkingdom'),
        ('subspecies', 'Subspecies'),
        ('no_rank', 'No rank'),
        ('subfamily', 'Subfamily'),
        ('varietas', 'Varietas'),
        ('species_group', 'Species group'),
    ]
    CSV_HEADER = ','.join([
        'tax_id', 'tax_name', 'tax_rank', 'tax_full',
    ])

    tax_id = models.CharField(max_length=20, unique=True, db_index=True)
    name = models.CharField(max_length=200, default=NAME_DEFAULT)
    rank = models.CharField(max_length=20, choices=RANK_CHOICES)
    parent = models.ForeignKey(
        'Taxonomy', related_name='direct_children',
        on_delete=models.SET_NULL,
        null=True, blank=True,
    )
    hierarchy = models.JSONField(null=True)

    def __str__(self):
        return f"{self.name}"

    def build_hierarchy(self):
        """
        Build and save parental hierarchy for an entry
        """
        hierarchy = {}
        if self.name != 'root':
            hierarchy[self.rank] = {
                'tax_id': self.tax_id,
                'name': self.name
            }
            if self.parent is not None:
                hierarchy = {**hierarchy, **getattr(self.parent, 'hierarchy', self.parent.build_hierarchy())}
        self.hierarchy = hierarchy
        self.save()
        return hierarchy

    def _compute_one_line_detailed_taxonomy(self) -> str:
        default_item = {
            'name': ''
        }
        if self.hierarchy is None:
            self.build_hierarchy()
        if self.hierarchy.get('species', None) is None:
            s = ''
        else:
            s = self.hierarchy.get('species')['name'].split()[-1]
        return "k__{k}; p__{p}; c__{c}; o__{o}; f__{f}; g__{g}; s__{s}".format(
            k=self.hierarchy.get('kingdom', self.hierarchy.get('superkingdom', default_item))['name'],
            p=self.hierarchy.get('phylum', default_item)['name'],
            c=self.hierarchy.get('class', default_item)['name'],
            o=self.hierarchy.get('order', default_item)['name'],
            f=self.hierarchy.get('family', default_item)['name'],
            g=self.hierarchy.get('genus', default_item)['name'],
            s=s
        )

    @property
    def one_line_detailed_taxonomy(self) -> str:
        if getattr(self, '_one_line_detailed_taxonomy', None) is None:
            self._one_line_detailed_taxonomy = self._compute_one_line_detailed_taxonomy()
        return self._one_line_detailed_taxonomy

    @property
    def csv_header(self) -> str:
        return self.CSV_HEADER

    @property
    def csv(self) -> str:
        return ','.join([self.tax_id, self.name, self.rank, self.one_line_detailed_taxonomy])

    class Meta:
        verbose_name_plural = "Taxonomy"
        ordering = ['-tax_id']
