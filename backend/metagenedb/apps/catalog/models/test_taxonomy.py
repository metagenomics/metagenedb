from rest_framework.test import APITestCase

from metagenedb.apps.catalog.factory import TaxonomyFactory


class TestBuildHierarchy(APITestCase):

    @classmethod
    def setUpTestData(cls):
        """
        Build some test data for different tests
        """
        cls.root = TaxonomyFactory.create(
            tax_id="1",
            name="root",
            rank="no_rank",
        )
        cls.kingdom = TaxonomyFactory(
            tax_id="2",
            name="KINGDOM",
            rank="kingdom",
            parent=cls.root
        )
        cls.phylum = TaxonomyFactory(
            tax_id="3",
            name="PHYLUM",
            rank="phylum",
            parent=cls.kingdom
        )
        cls.species = TaxonomyFactory(
            tax_id="4",
            name="Genus Species",
            rank="species",
            parent=cls.phylum
        )

    def test_build_hierarchy(self):
        expected_dict = {
            'phylum': {
                'tax_id': self.phylum.tax_id,
                'name': self.phylum.name
            },
            'kingdom': {
                'tax_id': self.kingdom.tax_id,
                'name': self.kingdom.name
            }
        }
        self.assertIsNone(getattr(self.phylum, 'hierarchy'))
        test_dict = self.phylum.build_hierarchy()
        self.assertDictEqual(test_dict, expected_dict)
        self.assertIsNotNone(getattr(self.phylum, 'hierarchy'))
        self.assertDictEqual(getattr(self.phylum, 'hierarchy'), expected_dict)

    def test_compute_one_line_detailed_taxonomy(self):
        expected_str = "k__; p__; c__; o__; f__; g__; s__"
        self.assertEqual(self.root.one_line_detailed_taxonomy, expected_str)
        expected_str = "k__KINGDOM; p__; c__; o__; f__; g__; s__"
        self.assertEqual(self.kingdom.one_line_detailed_taxonomy, expected_str)
        expected_str = "k__KINGDOM; p__PHYLUM; c__; o__; f__; g__; s__"
        self.assertEqual(self.phylum.one_line_detailed_taxonomy, expected_str)
        expected_str = "k__KINGDOM; p__PHYLUM; c__; o__; f__; g__; s__Species"
        self.assertEqual(self.species.one_line_detailed_taxonomy, expected_str)

    def test_csv(self):
        expected = (
            f"{self.species.tax_id},{self.species.name},"
            f"{self.species.rank},{self.species.one_line_detailed_taxonomy}"
        )
        self.assertEqual(self.species.csv, expected)
