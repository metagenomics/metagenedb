from rest_framework.test import APITestCase

from metagenedb.apps.catalog.factory import (
    GeneWithEggNOGFactory, GeneWithKeggFactory, GeneWithTaxonomyFactory
)


class TestGeneCSV(APITestCase):

    @classmethod
    def setUpTestData(cls):
        """
        Build some test data for different tests
        """
        cls.gene_eggnog = GeneWithEggNOGFactory.create()
        cls.gene_kegg = GeneWithKeggFactory()
        cls.gene_tax = GeneWithTaxonomyFactory()

    def test_csv_header(self):
        expected_header = 'gene_id,gene_name,gene_source,length,tax_id,tax_name,tax_rank,tax_full,kegg_id,eggnog_id'
        self.assertEqual(self.gene_tax.csv_header, expected_header)

    def test_csv_gene(self):
        expected = f"{self.gene_tax.name},{self.gene_tax.source},{self.gene_tax.length}"
        self.assertEqual(self.gene_tax.csv_gene, expected)

    def test_csv_tax(self):
        expected = (
            f"{self.gene_tax.taxonomy.tax_id},{self.gene_tax.taxonomy.name},"
            f"{self.gene_tax.taxonomy.rank},{self.gene_tax.taxonomy.one_line_detailed_taxonomy}"
        )
        self.assertEqual(self.gene_tax.csv_tax, expected)

    def test_csv_tax_empty(self):
        expected = ",,,"
        self.assertEqual(self.gene_kegg.csv_tax, expected)
        self.assertEqual(self.gene_eggnog.csv_tax, expected)

    def test_csv_functions(self):
        expected = f"{self.gene_kegg.functions.all()[0].function_id},"
        self.assertEqual(self.gene_kegg.csv_functions, expected)
        expected = f",{self.gene_eggnog.functions.all()[0].function_id}"
        self.assertEqual(self.gene_eggnog.csv_functions, expected)

    def test_csv_functions_empty(self):
        expected = ","
        self.assertEqual(self.gene_tax.csv_functions, expected)

    def test_csv(self):
        expected = (
            f"{self.gene_tax.gene_id},{self.gene_tax.name},{self.gene_tax.source},{self.gene_tax.length},"
            f"{self.gene_tax.taxonomy.tax_id},{self.gene_tax.taxonomy.name},"
            f"{self.gene_tax.taxonomy.rank},{self.gene_tax.taxonomy.one_line_detailed_taxonomy},"
            f","
        )
        self.assertEqual(self.gene_tax.csv, expected)
