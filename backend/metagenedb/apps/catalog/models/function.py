from django.db import models


class Function(models.Model):
    UNDEFINED = 'undef'
    KEGG = 'kegg'
    EGGNOG = 'eggnog'
    SOURCE_CHOICES = [
        (UNDEFINED, 'Undefined'),
        (KEGG, 'KEGG'),
        (EGGNOG, 'EggNOG')
    ]

    function_id = models.CharField(max_length=100, db_index=True, unique=True)
    name = models.CharField(max_length=500, blank=True)
    source = models.CharField(max_length=10, choices=SOURCE_CHOICES, default=UNDEFINED)

    def __str__(self):
        return self.function_id

    class Meta:
        ordering = ['-function_id']


class KeggOrthology(Function):
    SOURCE = 'kegg'

    ec_number = models.CharField(max_length=200, default='', blank=True)
    long_name = models.CharField(max_length=500)

    def __init__(self, *args, **kwargs):
        super(KeggOrthology, self).__init__(source=self.SOURCE, *args, **kwargs)

    class Meta:
        verbose_name = "KEGG orthology"
        verbose_name_plural = "KEGG orthologies"


class EggNogFunctionalCategory(models.Model):
    GROUP_CHOICES = [
        ('info_storage_processing', 'Information Storage and Processing'),
        ('cellular_processes_signaling', 'Cellular Processes and Signaling'),
        ('metabolism', 'Metabolism'),
        ('poorly_characterized', 'Poorly Characterized')
    ]

    category_id = models.CharField(max_length=1, db_index=True, unique=True)
    name = models.CharField(max_length=100)
    group = models.CharField(max_length=100, choices=GROUP_CHOICES)

    def __str__(self):
        return f"{self.category_id} ({self.name})"

    class Meta:
        verbose_name = "EggNOG functional category"
        verbose_name_plural = "EggNOG functional categories"


class EggNOG(Function):
    SOURCE = 'eggnog'
    VERSION_CHOICES = [
        ('3.0', '3.0'),
        ('5.0', '5.0')
    ]

    functional_categories = models.ManyToManyField(EggNogFunctionalCategory)
    version = models.CharField(max_length=5, choices=VERSION_CHOICES)

    def __init__(self, *args, **kwargs):
        super(EggNOG, self).__init__(source=self.SOURCE, *args, **kwargs)

    class Meta:
        verbose_name = "EggNOG"
        verbose_name_plural = "EggNOGs"
