from factory import (
    Faker, RelatedFactory, SubFactory, fuzzy
)
from factory.django import DjangoModelFactory

from metagenedb.apps.catalog import models

from .function import FunctionFactory, KeggOrthologyFactory, EggNOGFactory
from .taxonomy import TaxonomyFactory

GENE_SOURCES = [i[0] for i in models.Gene.SOURCE_CHOICES]


class GeneFactory(DjangoModelFactory):
    class Meta:
        model = models.Gene

    gene_id = Faker('bothify', text='gene-?#?#??-#??#?#')
    name = Faker('bothify', text='Gene_name-##-????')
    length = Faker('pyint', min_value=200, max_value=4200)
    source = fuzzy.FuzzyChoice(GENE_SOURCES)


class GeneWithTaxonomyFactory(GeneFactory):
    taxonomy = SubFactory(TaxonomyFactory)


class GeneFunctionFactory(DjangoModelFactory):
    class Meta:
        model = models.GeneFunction

    gene = SubFactory(GeneFactory)
    function = SubFactory(FunctionFactory)


class GeneKeggFactory(GeneFunctionFactory):
    function = SubFactory(KeggOrthologyFactory)


class GeneEggNOGFactory(GeneFunctionFactory):
    function = SubFactory(EggNOGFactory)


class GeneWithKeggFactory(GeneFactory):
    kegg = RelatedFactory(GeneKeggFactory, 'gene')


class GeneWithEggNOGFactory(GeneFactory):
    eggnog = RelatedFactory(GeneEggNOGFactory, 'gene')
