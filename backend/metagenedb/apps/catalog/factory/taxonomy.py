from collections import OrderedDict

from factory import fuzzy
from factory.django import DjangoModelFactory
from faker import Factory

from metagenedb.apps.catalog import models

from .fuzzy_base import FuzzyLowerText

faker = Factory.create()

SELECTED_RANK = [i[0] for i in models.Taxonomy.RANK_CHOICES]


class TaxonomyFactory(DjangoModelFactory):
    class Meta:
        model = models.Taxonomy

    rank = fuzzy.FuzzyChoice(SELECTED_RANK)
    tax_id = FuzzyLowerText(prefix='tax-', length=15)
    name = fuzzy.FuzzyText(length=20)


class DbGenerator:

    def __init__(self):
        self.created_ids = set()  # store already created IDs to skip them

    def generate_db_from_tree(self, tree):
        """
        Tree need to be an OrderedDict from higher to lower level
        """
        self.last_tax = None
        for rank, desc in tree.items():
            if desc['tax_id'] not in self.created_ids:
                self.last_tax = TaxonomyFactory.create(
                    tax_id=desc['tax_id'],
                    name=desc['name'],
                    rank=rank,
                    parent=getattr(self, "last_tax", None)
                )
            else:
                self.last_tax = models.Taxonomy.objects.get(tax_id=desc['tax_id'])
            self.created_ids.add(desc['tax_id'])
        self.last_tax.build_hierarchy()


def _generate_lactobacillus_db(db_generator):
    """
    Generate db with few ranks corresponding to Lactobacillus genus
    """
    tree = OrderedDict()
    tree['no_rank'] = {"name": "root", "tax_id": "1"}
    tree["superkingdom"] = {"name": "Bacteria", "tax_id": "2"}
    tree["phylum"] = {"name": "Firmicutes", "tax_id": "1239"}
    tree["class"] = {"name": "Bacilli", "tax_id": "91061"}
    tree["order"] = {"name": "Lactobacillales", "tax_id": "186826"}
    tree["family"] = {"name": "Lactobacillaceae", "tax_id": "33958"}
    tree["genus"] = {"name": "Lactobacillus", "tax_id": "1578"}
    tree["species_group"] = {"name": "Lactobacillus casei group", "tax_id": "655183"}
    db_generator.generate_db_from_tree(tree)


def _generate_escherichia_db(db_generator):
    tree = OrderedDict()
    tree["no_rank"] = {"name": "root", "tax_id": "1"}
    tree["superkingdom"] = {"name": "Bacteria", "tax_id": "2"}
    tree["phylum"] = {"name": "Proteobacteria", "tax_id": "1224"}
    tree["class"] = {"name": "Gammaproteobacteria", "tax_id": "1236"}
    tree["order"] = {"name": "Enterobacterales", "tax_id": "91347"}
    tree["family"] = {"name": "Enterobacteriaceae", "tax_id": "543"}
    tree["genus"] = {"name": "Escherichia", "tax_id": "561"}
    tree["species"] = {"name": "Escherichia coli", "tax_id": "562"}
    db_generator.generate_db_from_tree(tree)


def generate_simple_db():
    db_generator = DbGenerator()
    _generate_escherichia_db(db_generator)
    _generate_lactobacillus_db(db_generator)
