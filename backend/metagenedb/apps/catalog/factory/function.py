from factory import Faker, fuzzy
from factory.django import DjangoModelFactory

from metagenedb.apps.catalog import models


SELECTED_SOURCE = [i[0] for i in models.Function.SOURCE_CHOICES]
EGGNOG_VERSIONS = [i[0] for i in models.EggNOG.VERSION_CHOICES]


class FunctionFactory(DjangoModelFactory):
    class Meta:
        model = models.Function

    function_id = Faker('bothify', text='function-####')
    source = fuzzy.FuzzyChoice(SELECTED_SOURCE)


class EggNOGFactory(DjangoModelFactory):
    function_id = Faker('bothify', text='COG####')
    name = Faker('bothify', text='COG-????????')

    class Meta:
        model = models.EggNOG

    version = fuzzy.FuzzyChoice(EGGNOG_VERSIONS)


class KeggOrthologyFactory(DjangoModelFactory):
    function_id = Faker('bothify', text='K0####')

    class Meta:
        model = models.KeggOrthology


def _create_fake_kegg_db():
    KeggOrthologyFactory.create(function_id="K12345", name="Kegg1")
    KeggOrthologyFactory.create(function_id="K67890", name="Kegg2")


def _create_fake_eggnog_db():
    EggNOGFactory.create(function_id="COG1234", name="COG1")
    EggNOGFactory.create(function_id="COG5678", name="COG2")


def generate_fake_functions_db():
    _create_fake_eggnog_db()
    _create_fake_kegg_db()
