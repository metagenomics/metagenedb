import string

from factory import fuzzy


class FuzzyLowerText(fuzzy.FuzzyText):
    CHARS = string.ascii_lowercase + ''.join(map(str, range(1, 10)))

    def __init__(self, **kwargs):
        super(FuzzyLowerText, self).__init__(chars=self.CHARS, **kwargs)
