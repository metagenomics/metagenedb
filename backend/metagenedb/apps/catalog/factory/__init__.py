from .function import EggNOGFactory, FunctionFactory, KeggOrthologyFactory  # noqa
from .gene import GeneFactory, GeneWithEggNOGFactory, GeneWithKeggFactory, GeneWithTaxonomyFactory  # noqa
from .taxonomy import TaxonomyFactory  # noqa
