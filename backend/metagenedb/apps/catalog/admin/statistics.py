from django.contrib import admin

from metagenedb.apps.catalog.models import Statistics


@admin.register(Statistics)
class StatisticsAdmin(admin.ModelAdmin):
    list_display = ('stats_id',)
    search_fields = ('stats_id',)
