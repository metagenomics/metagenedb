from django.contrib import admin

from metagenedb.apps.catalog.models import Gene


@admin.register(Gene)
class GeneAdmin(admin.ModelAdmin):
    exclude = ('functions', 'taxonomy')

    list_display = ('gene_id', 'source', 'name', 'length', 'get_functions', 'get_taxonomy')
    list_filter = ('source',)
    search_fields = ('name',)

    def get_functions(self, obj):
        if obj.functions.all():
            return ",".join([str(f) for f in obj.functions.all()])
        return '-'
    get_functions.short_description = 'Functions'

    def get_taxonomy(self, obj):
        if obj.taxonomy:
            return f"{obj.taxonomy} ({obj.taxonomy.rank})"
        return '-'
    get_taxonomy.short_description = 'Taxonomy'
