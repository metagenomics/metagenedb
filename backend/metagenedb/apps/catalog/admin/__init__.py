from .gene import GeneAdmin  # noqa
from .function import FunctionAdmin, KeggOrthologyAdmin  # noqa
from .statistics import Statistics  # noqa
from .taxonomy import TaxonomyAdmin  # noqa
