from django.contrib import admin
from django_admin_listfilter_dropdown.filters import DropdownFilter

from metagenedb.apps.catalog.models import Taxonomy

RANK_DISPLAY = [f"get_{i}" for i in [
    'superkingdom', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species']
]


@admin.register(Taxonomy)
class TaxonomyAdmin(admin.ModelAdmin):
    exclude = ('parent',)

    list_display = (
        'tax_id', 'name', 'rank', 'get_parent',
    ) + tuple(RANK_DISPLAY)
    list_filter = (('rank', DropdownFilter),)
    search_fields = ('tax_id', 'name')

    def get_parent(self, obj):
        if obj.parent:
            return f"{obj.parent.name} ({obj.parent.rank})"
        return '-'
    get_parent.short_description = 'Parent'

    def _get_taxonomy(self, obj, rank):
        if obj.hierarchy:
            if obj.hierarchy.get(rank) is not None:
                return "{}".format(obj.hierarchy.get(rank).get('name', '-'))
        return '-'

    def get_superkingdom(self, obj):
        return self._get_taxonomy(obj, 'superkingdom')
    get_superkingdom.short_description = 'Superkingdom'

    def get_kingdom(self, obj):
        return self._get_taxonomy(obj, 'kingdom')
    get_kingdom.short_description = 'Kingdom'

    def get_phylum(self, obj):
        return self._get_taxonomy(obj, 'phylum')
    get_phylum.short_description = 'Phylum'

    def get_class(self, obj):
        return self._get_taxonomy(obj, 'class')
    get_class.short_description = 'Class'

    def get_order(self, obj):
        return self._get_taxonomy(obj, 'order')
    get_order.short_description = 'Order'

    def get_family(self, obj):
        return self._get_taxonomy(obj, 'family')
    get_family.short_description = 'Family'

    def get_genus(self, obj):
        return self._get_taxonomy(obj, 'genus')
    get_genus.short_description = 'Genus'

    def get_species(self, obj):
        return self._get_taxonomy(obj, 'species')
    get_species.short_description = 'Species'
