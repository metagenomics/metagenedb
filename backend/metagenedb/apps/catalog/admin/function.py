from django.contrib import admin

from metagenedb.apps.catalog.models import EggNOG, EggNogFunctionalCategory, Function, KeggOrthology


@admin.register(KeggOrthology)
class KeggOrthologyAdmin(admin.ModelAdmin):

    list_display = ('function_id', 'name', 'long_name', 'ec_number')
    search_fields = ('function_id', 'name')


@admin.register(Function)
class FunctionAdmin(admin.ModelAdmin):

    list_display = ('function_id', 'name', 'source')
    search_fields = ('function_id',)


@admin.register(EggNOG)
class EggNOGAdmin(admin.ModelAdmin):

    list_display = ('function_id', 'name', 'get_functional_categories', 'version')
    list_filter = ('version',)
    search_fields = ('function_id', 'name')

    def get_functional_categories(self, obj):
        if obj.functional_categories.all():
            return ", ".join([str(f) for f in obj.functional_categories.all()])
        return '-'
    get_functional_categories.short_description = 'Functional categories'


@admin.register(EggNogFunctionalCategory)
class EggNogFunctionalCategoryAdmin(admin.ModelAdmin):

    list_display = ('category_id', 'name', 'group')
    search_fields = ('category_id', 'name')
