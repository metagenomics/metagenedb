import logging

from django.core.management.base import BaseCommand

from metagenedb.apps.catalog.management.commands.commons.import_genes import BaseImportGenes
from metagenedb.common.utils.parsers import VirgoGeneLengthLineParser

logging.basicConfig(format='[%(asctime)s] %(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)


class ImportVirgoGenes(BaseImportGenes):

    IMPORT_TYPE = "Virgo gene length"  # For logs
    SELECTED_KEYS = ['gene_id', 'length']
    UPDATED_FIELDS = ['length', 'name', 'source']
    SOURCE = 'virgo'
    PARSER = VirgoGeneLengthLineParser


class Command(BaseCommand):
    help = 'Create or update all Virgo genes (name and length from `0.geneLength.txt` file).'

    def add_arguments(self, parser):
        parser.add_argument('annotation', help='0.geneLength.txt file from Virgo')
        parser.add_argument('--test', action='store_true', help='Run only on first 10000 entries.')

    def set_logger_level(self, verbosity):
        if verbosity > 2:
            logger.setLevel(logging.DEBUG)
        elif verbosity > 1:
            logger.setLevel(logging.INFO)

    def handle(self, *args, **options):
        self.set_logger_level(int(options['verbosity']))
        import_virgo = ImportVirgoGenes(options['annotation'])
        import_virgo.load_all(test=options['test'])
