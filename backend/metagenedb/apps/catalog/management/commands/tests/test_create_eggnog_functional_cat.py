from unittest import TestCase

from metagenedb.apps.catalog.management.commands.create_eggnog_functional_cat import ImportEggNOGFunctionalCategories


class TestUpdateGroupName(TestCase):

    def test_update_group_name_information(self):
        import_object = ImportEggNOGFunctionalCategories('test')
        payload = {
            'category_id': 'A',
            'name': 'Test',
            'group': 'Information storage and processing'
        }
        expected_output = {
            'category_id': 'A',
            'name': 'Test',
            'group': 'info_storage_processing'
        }
        self.assertDictEqual(import_object.update_group_name(payload), expected_output)

    def test_update_group_name_cellular(self):
        import_object = ImportEggNOGFunctionalCategories('test')
        payload = {
            'category_id': 'A',
            'name': 'Test',
            'group': 'Cellular processes and signaling'
        }
        expected_output = {
            'category_id': 'A',
            'name': 'Test',
            'group': 'cellular_processes_signaling'
        }
        self.assertDictEqual(import_object.update_group_name(payload), expected_output)

    def test_update_group_name_metabolism(self):
        import_object = ImportEggNOGFunctionalCategories('test')
        payload = {
            'category_id': 'A',
            'name': 'Test',
            'group': 'Metabolism'
        }
        expected_output = {
            'category_id': 'A',
            'name': 'Test',
            'group': 'metabolism'
        }
        self.assertDictEqual(import_object.update_group_name(payload), expected_output)

    def test_update_group_name_poorly(self):
        import_object = ImportEggNOGFunctionalCategories('test')
        payload = {
            'category_id': 'A',
            'name': 'Test',
            'group': 'Poorly characterized'
        }
        expected_output = {
            'category_id': 'A',
            'name': 'Test',
            'group': 'poorly_characterized'
        }
        self.assertDictEqual(import_object.update_group_name(payload), expected_output)
