import os

from rest_framework.test import APITestCase

from metagenedb.apps.catalog.models import Gene
from metagenedb.apps.catalog.management.commands.import_virgo_taxonomy import ImportVirgoGeneTaxonomyAnnotation
from metagenedb.apps.catalog.factory import GeneFactory
from metagenedb.apps.catalog.factory.taxonomy import generate_simple_db


class TestEndToEnd(APITestCase):

    @classmethod
    def setUpTestData(cls):
        generate_simple_db()
        for gene_id in ['v1', 'v2']:
            GeneFactory.create(gene_id=gene_id)

    def test_end_to_end(self):
        test_file = os.path.join(os.path.dirname(__file__), "./test_files/virgo_taxonomy.tsv")
        loader = ImportVirgoGeneTaxonomyAnnotation(test_file)
        expected_genes = {
            'v1': {
                'name': 'V1',
                'tax_id': '562',
            },
            'v2': {
                'name': 'V2',
                'tax_id': '1578',
            }
        }
        loader.load_all()
        created_genes = Gene.objects.all().prefetch_related('functions')
        for created_gene in created_genes:
            self.assertEqual(getattr(created_gene, 'name'), expected_genes[created_gene.gene_id]['name'])
            self.assertEqual(created_gene.taxonomy.tax_id, expected_genes[created_gene.gene_id]['tax_id'])
