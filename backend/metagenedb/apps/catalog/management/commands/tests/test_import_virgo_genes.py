import os

from rest_framework.test import APITestCase

from metagenedb.apps.catalog.models import Gene
from metagenedb.apps.catalog.management.commands.import_virgo_genes import ImportVirgoGenes


class TestEndToEnd(APITestCase):

    def test_end_to_end(self):
        test_file = os.path.join(os.path.dirname(__file__), "./test_files/virgo_gene_length.tsv")
        loader = ImportVirgoGenes(test_file)
        expected_genes = {
            'v1': {
                'source': 'virgo',
                'length': 101,
                'name': 'V1'
            },
            'v2': {
                'source': 'virgo',
                'length': 102,
                'name': 'V2'
            },
        }
        loader.load_all()
        created_genes = Gene.objects.all().values()
        for created_gene in created_genes:
            for key in ['source', 'length', 'name']:
                self.assertEqual(created_gene[key], expected_genes[created_gene['gene_id']][key])
