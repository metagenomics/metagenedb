import os

from rest_framework.test import APITestCase

from metagenedb.apps.catalog.models import Gene
from metagenedb.apps.catalog.management.commands.import_virgo_eggnog import ImportVirgoGeneEggNOGAnnotation
from metagenedb.apps.catalog.factory import GeneFactory
from metagenedb.apps.catalog.factory.function import generate_fake_functions_db


class TestEndToEnd(APITestCase):

    @classmethod
    def setUpTestData(cls):
        generate_fake_functions_db()
        GeneFactory.create(gene_id="v1")
        GeneFactory.create(gene_id="v2")

    def test_end_to_end(self):
        test_file = os.path.join(os.path.dirname(__file__), "./test_files/virgo_eggnog.tsv")
        loader = ImportVirgoGeneEggNOGAnnotation(test_file)
        expected_genes = {
            'v1': {
                'name': 'V1',
                'functions': {
                    'eggnog': 'COG1234',
                }
            },
            'v2': {
                'name': 'V2',
                'functions': {
                    'eggnog': 'COG5678',
                }
            },
        }
        loader.load_all()
        created_genes = Gene.objects.all().prefetch_related('functions')
        for created_gene in created_genes:
            self.assertEqual(getattr(created_gene, 'name'), expected_genes[created_gene.gene_id]['name'])
            # Check functions
            self.assertTrue(created_gene.functions.all())
            for function in created_gene.functions.all():
                self.assertIn(function.source, ['kegg', 'eggnog'])
                self.assertEqual(
                    function.function_id, expected_genes[created_gene.gene_id]['functions'][function.source]
                )
