from rest_framework.test import APITestCase

from metagenedb.apps.catalog.factory import TaxonomyFactory
from metagenedb.apps.catalog.models import Taxonomy

from metagenedb.apps.catalog.management.commands.build_hierarchy import HierarchyBuilder


class TestBuildHierarchy(APITestCase):

    @classmethod
    def setUpTestData(cls):
        """
        Build some test data for different tests
        """
        cls.root = TaxonomyFactory.create(
            tax_id="1",
            name="root",
            rank="no_rank",
        )
        cls.root.parent = cls.root
        cls.root.save()
        cls.kingdom = TaxonomyFactory(
            tax_id="2",
            name="KINGDOM",
            rank="kingdom",
            parent=cls.root
        )
        cls.phylum = TaxonomyFactory(
            tax_id="3",
            name="PHYLUM",
            rank="phylum",
            parent=cls.kingdom
        )

    def test_build_hierarchy(self):
        expected_dict = {
            'phylum': {
                'tax_id': self.phylum.tax_id,
                'name': self.phylum.name
            },
            'kingdom': {
                'tax_id': self.kingdom.tax_id,
                'name': self.kingdom.name
            }
        }
        self.assertIsNone(getattr(self.phylum, 'hierarchy'))
        hierarchy_builder = HierarchyBuilder(Taxonomy.objects.select_related('parent'))
        hierarchy_builder.build_all()
        updated_phylum = Taxonomy.objects.get(tax_id=self.phylum.tax_id)
        self.assertIsNotNone(getattr(updated_phylum, 'hierarchy'))
        self.assertDictEqual(getattr(updated_phylum, 'hierarchy'), expected_dict)
