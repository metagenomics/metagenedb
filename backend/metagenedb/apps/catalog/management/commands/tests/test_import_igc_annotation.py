import os
from rest_framework.test import APITestCase

from metagenedb.apps.catalog.models import Gene
from metagenedb.apps.catalog.management.commands.import_igc_annotation import ImportIGCGenes
from metagenedb.apps.catalog.factory.function import generate_fake_functions_db
from metagenedb.apps.catalog.factory.taxonomy import generate_simple_db


class TestEndToEnd(APITestCase):

    @classmethod
    def setUpTestData(cls):
        generate_simple_db()
        generate_fake_functions_db()

    def test_end_to_end(self):
        test_file = os.path.join(os.path.dirname(__file__), "./test_files/igc_annotation.tsv")
        loader = ImportIGCGenes(test_file)
        expected_genes = {
            'gene-1': {
                'source': 'igc',
                'length': 123,
                'name': 'Gene_1',
                'tax_id': '561',
                'functions': {
                    'kegg': 'K12345',
                    'eggnog': 'COG1234'
                }
            },
            'gene-2': {
                'source': 'igc',
                'length': 456,
                'name': 'Gene_2',
                'tax_id': '1239',  # Genus annotation Veillonella not in test db, but phylum yes
                'functions': {
                    'kegg': 'K67890',
                    'eggnog': 'COG5678'
                }
            },
        }
        loader.load_all()
        created_genes = Gene.objects.all().prefetch_related('functions')
        for created_gene in created_genes:
            for key in ['source', 'length', 'name']:
                self.assertEqual(getattr(created_gene, key), expected_genes[created_gene.gene_id][key])
            self.assertEqual(created_gene.taxonomy.tax_id, expected_genes[created_gene.gene_id]['tax_id'])
            # Check functions
            for function in created_gene.functions.all():
                self.assertIn(function.source, ['kegg', 'eggnog'])
                self.assertEqual(
                    function.function_id, expected_genes[created_gene.gene_id]['functions'][function.source]
                )
