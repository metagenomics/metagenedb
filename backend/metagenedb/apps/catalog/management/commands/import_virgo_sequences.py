import logging

from django.core.management.base import BaseCommand

from metagenedb.apps.catalog.management.commands.commons.import_gene_sequences import ImportGeneSequences

logging.basicConfig(format='[%(asctime)s] %(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)


class ImportVirgoGeneSequences(ImportGeneSequences):
    CATALOG = "Virgo"


class Command(BaseCommand):
    help = 'Create or update all Virgo gene equences (from `NT.fasta` file).'

    def add_arguments(self, parser):
        parser.add_argument(
            'fasta',
            help='NT.fasta file from Virgo. Genes need to exist in DB for this script to work.'
        )
        parser.add_argument('--test', action='store_true', help='Run only on first 10000 sequences.')
        parser.add_argument('--skip_n', type=int, default=0, help='Number of sequence to skip')

    def set_logger_level(self, verbosity):
        if verbosity > 2:
            logger.setLevel(logging.DEBUG)
        elif verbosity > 1:
            logger.setLevel(logging.INFO)

    def handle(self, *args, **options):
        self.set_logger_level(int(options['verbosity']))
        import_igc = ImportVirgoGeneSequences(options['fasta'])
        import_igc.load_all(test=options['test'], skip_n_sequences=options['skip_n'])
