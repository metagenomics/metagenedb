import logging

from django.core.management.base import BaseCommand
from django.core.exceptions import ValidationError

from metagenedb.apps.catalog.models import EggNOG, EggNogFunctionalCategory
from metagenedb.common.utils.chunks import file_len
from metagenedb.common.utils.parsers import EggNOGAnnotationLineParser

logging.basicConfig(format='[%(asctime)s] %(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)


class ImportEggNOG(object):

    def __init__(self, file_path):
        self.annotation_file = file_path
        self.eggnog_parser = EggNOGAnnotationLineParser()
        self.processed_count = 0
        self.created_count = 0
        self.updated_count = 0
        self.skipped_count = 0
        self.skipped_ids = []
        self.skipped_errors = []

    def _build_functional_category_dict(self):
        all_categories = EggNogFunctionalCategory.objects.all()
        if not all_categories:
            raise Exception("You need to create Functional categories first.")
        self.functional_cat_instances = {cat.category_id: cat for cat in all_categories}

    def add_functional_categories(self, eggnog, functional_category_keys):
        for key in functional_category_keys:
            if key not in self.functional_cat_instances.keys():
                logger.warning("%s is not a valid functional categories. 'S' used instead for %s",
                               key, eggnog.function_id)
                key = 'S'
            eggnog.functional_categories.add(self.functional_cat_instances[key])

    def load_all(self, test=False, version=None):
        self._build_functional_category_dict()
        self.total_eggnog_nb = file_len(self.annotation_file)
        logger.info("Preparing to load %s items...", self.total_eggnog_nb)
        with open(self.annotation_file, "r") as file:
            for line in file:
                eggnog_dict = self.eggnog_parser.get_dict(line)
                functional_category_keys = eggnog_dict.pop('functional_categories')  # link later
                payload = {k: v for k, v in eggnog_dict.items() if v != ""}
                payload['version'] = version
                eggnog = None
                try:
                    eggnog = EggNOG(**payload)
                    eggnog.full_clean()
                    eggnog.save()
                    self.created_count += 1
                except ValidationError as validation_error:
                    if 'function_id' in validation_error.error_dict.keys():
                        try:
                            eggnog = EggNOG.objects.get(function_id=payload.get('function_id'))
                            for k, v in payload.items():
                                setattr(eggnog, k, v)
                            eggnog.full_clean()
                            eggnog.save()
                            self.updated_count += 1
                        except ValidationError as validation_error:
                            logger.error(validation_error)
                            self.skipped_errors.append(validation_error)
                            self.skipped_ids.append(payload.get('function_id'))
                            self.skipped_count += 1
                            eggnog = None
                    else:
                        logger.error(validation_error)
                        self.skipped_errors.append(validation_error)
                        self.skipped_ids.append(payload.get('function_id'))
                        self.skipped_count += 1
                        eggnog = None
                if eggnog is not None:
                    self.add_functional_categories(eggnog, functional_category_keys)
                    eggnog.save()
                self.processed_count += 1
                if self.processed_count % 1000 == 0:
                    logger.info("%s/%s EggNOG processed so far...", self.processed_count, self.total_eggnog_nb)
                    if test:
                        break
        logger.info("[DONE] %s/%s EggNOG created.", self.created_count, self.total_eggnog_nb)
        logger.info("[DONE] %s/%s EggNOG updated.", self.updated_count, self.total_eggnog_nb)
        logger.info("[DONE] %s/%s EggNOG skipped. List: %s", self.skipped_count, self.total_eggnog_nb,
                    self.skipped_ids)


class Command(BaseCommand):
    help = 'Create or update all EggNOG entries from annotations.tsv file.'

    def add_arguments(self, parser):
        parser.add_argument('annotation', help='annotations.tsv file from EggNOG')
        parser.add_argument('--test', action='store_true', help='Run only on first 1000 entries.')
        parser.add_argument('--eggnog_version', choices=['3.0', '5.0'], required=True, help='version of EggNOG.')

    def set_logger_level(self, verbosity):
        if verbosity > 2:
            logger.setLevel(logging.DEBUG)
        elif verbosity > 1:
            logger.setLevel(logging.INFO)

    def handle(self, *args, **options):
        self.set_logger_level(int(options['verbosity']))
        import_eggnog = ImportEggNOG(options['annotation'])
        import_eggnog.load_all(test=options['test'], version=options['eggnog_version'])
