import logging
from typing import List

from celery.decorators import task
from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand
from slugify import slugify

from metagenedb.apps.catalog.models import Statistics
from metagenedb.apps.catalog.operations.statistics import GeneStatistics, GeneLengthDistribution

logging.basicConfig(format='[%(asctime)s] %(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)


class ComputeStatistics:

    def __init__(self, gene_source):
        self.gene_source = gene_source

    def _save_to_db(self, payload):
        try:
            statistics = Statistics(**payload)
            statistics.full_clean()
        except ValidationError:
            try:
                statistics = Statistics.objects.get(stats_id=payload['stats_id'])
                for k, v in payload.items():
                    setattr(statistics, k, v)
                statistics.full_clean()
            except ValidationError as validation_error:
                raise(validation_error)
        statistics.save()


class ComputeCounts(ComputeStatistics):
    METHODS = [
        'count_all', 'count_has_function', 'count_has_taxonomy', 'count_has_function_has_taxonomy'
    ]
    FUNCTION_SOURCES = ['kegg', 'eggnog']

    def compute_count(self, method, filters=None, **kwargs):
        if filters is None:
            filters = {}
        gene_stats = GeneStatistics(filters=filters)
        print_kwargs = '-'.join([str(k) + '-' + str(v) for k, v in kwargs.items() if v])
        print_filters = '-'.join([str(k) + '-' + str(v) for k, v in filters.items() if v])
        stats_id = slugify(f"GeneStatistics({self.gene_source}).{method}({print_kwargs})")
        logger.info(
            "Call GeneStatistics(%s).%s(%s) and saving under id <%s>",
            print_filters, method, print_kwargs, stats_id
        )
        payload = {
            'stats_id': stats_id,
            'body': {
                'count': getattr(gene_stats, method)(**kwargs)
            }
        }
        self._save_to_db(payload)

    def all(self):
        if self.gene_source == 'all':
            filters = {}
        else:
            filters = {'source': self.gene_source}
        for method in self.METHODS:
            self.compute_count(method, filters=filters)
        for source in self.FUNCTION_SOURCES:
            self.compute_count('count_has_function', filters=filters, source=source)


class ComputeGeneLength(ComputeStatistics):
    MIN_WINDOW_SIZE = 100
    MAX_STOP_AT = 5000
    WINDOW_SIZES = list(range(MIN_WINDOW_SIZE, 300, 100))
    STOP_ATS = list(range(1000, MAX_STOP_AT + 1, 1000))
    CATEGORIES = {
        'all': None,
        'with-functions': {'functions__isnull': False},
        'with-kegg': {'functions__source': 'kegg'},
        'with-eggnog': {'functions__source': 'eggnog'},
        'with-taxonomy': {'taxonomy__isnull': False},
        'with-function-tax': {
            'taxonomy__isnull': False,
            'functions__isnull': False
        },
    }

    def _compute_gene_length(self, filters, category):
        gene_stats = GeneLengthDistribution(
            window_size=self.MIN_WINDOW_SIZE, stop_at=self.MAX_STOP_AT, filters=filters
        )
        for window_size in self.WINDOW_SIZES:
            for stop_at in self.STOP_ATS:
                stats_id = slugify(f"GeneStatistics({self.gene_source}).gene-length-{window_size}-{stop_at}-{category}")
                logger.info(
                    "Call GeneStatistics.gene_length(%s, %s) for %s and saving under id <%s>",
                    window_size, stop_at, category, stats_id)
                payload = {
                    'stats_id': stats_id,
                    'body': gene_stats.get_distribution(window_size=window_size, stop_at=stop_at)
                }
                self._save_to_db(payload)

    def all(self):
        if self.gene_source == 'all':
            filters = {}
        else:
            filters = {'source': self.gene_source}
        for category, cat_filters in self.CATEGORIES.items():
            if cat_filters is not None:
                filters.update(**cat_filters)
            self._compute_gene_length(filters, category)


class ComputeTaxonomyRepartition(ComputeStatistics):
    ALL_LEVEL = [
        'kingdom', 'superkingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species'
    ]

    def all(self):
        if self.gene_source == 'all':
            filters = {}
        else:
            filters = {'source': self.gene_source}
        gene_stats = GeneStatistics(filters=filters)
        for level in self.ALL_LEVEL:
            stats_id = slugify(f"GeneStatistics({self.gene_source}).taxonomy_repartition({level})")
            logger.info(
                "Call GeneStatistics.taxonomy_repartition(%s) and saving under id <%s>",
                level, stats_id
            )
            payload = {
                'stats_id': stats_id,
                'body': gene_stats.taxonomy_repartition(level=level)
            }
            self._save_to_db(payload)


class ComputeTaxonomyPresence(ComputeStatistics):
    ALL_LEVEL = [
        'kingdom', 'superkingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species'
    ]

    def all(self):
        if self.gene_source == 'all':
            filters = {}
        else:
            filters = {'source': self.gene_source}
        gene_stats = GeneStatistics(filters=filters)
        for level in self.ALL_LEVEL:
            stats_id = slugify(f"GeneStatistics({self.gene_source}).present_taxonomy({level})")
            logger.info(
                "Call GeneStatistics.present_taxonomy(%s) and saving under id <%s>",
                level, stats_id
            )
            payload = {
                'stats_id': stats_id,
                'body': gene_stats.present_taxonomy(level=level)
            }
            self._save_to_db(payload)


def clean_db():
    logger.info("Deleting all statistics entries...")
    Statistics.objects.all().delete()


@task(name="compute_stats.compute_gene_counts")
def compute_gene_counts(gene_sources: List[str]):
    for gene_source in gene_sources:
        ComputeCounts(gene_source).all()


@task(name="compute_stats.compute_gene_length")
def compute_gene_length(gene_sources: List[str]):
    for gene_source in gene_sources:
        ComputeGeneLength(gene_source).all()


@task(name="compute_stats.compute_taxonomy_repartition")
def compute_taxonomy_repartition(gene_sources: List[str]):
    for gene_source in gene_sources:
        ComputeTaxonomyRepartition(gene_source).all()


@task(name="compute_stats.compute_taxonomy_presence")
def compute_taxonomy_presence(gene_sources: List[str]):
    for gene_source in gene_sources:
        ComputeTaxonomyPresence(gene_source).all()


class Command(BaseCommand):
    help = "Compute gene catalog statistics."
    STEP_CHOICES = ['clean', 'counts', 'gene-length', 'taxonomy_repartition', 'taxonomy_presence']
    SOURCE_CHOICES = ['all', 'virgo', 'igc']

    def add_arguments(self, parser):
        parser.add_argument('--only', help=f'Run only one step (choices: {self.STEP_CHOICES}).')
        parser.add_argument('--source', help=f'Run only one step (choices: {self.SOURCE_CHOICES}).')

    def set_logger_level(self, verbosity):
        if verbosity > 2:
            logger.setLevel(logging.DEBUG)
        elif verbosity > 1:
            logger.setLevel(logging.INFO)

    def _get_and_validate_only_step(self, only_step_str):
        if only_step_str is not None:
            if only_step_str not in self.STEP_CHOICES:
                logger.warning(
                    "Choice '%s' is not a valid choice. Please choose among %s",
                    only_step_str, self.STEP_CHOICES
                )
        return only_step_str

    def _get_and_validate_source(self, source_str):
        if source_str is not None:
            if source_str not in self.SOURCE_CHOICES:
                logger.warning(
                    "Choice '%s' is not a valid choice. Please choose among %s",
                    source_str, self.SOURCE_CHOICES
                )
                return []
            return [source_str]
        return self.SOURCE_CHOICES

    def handle(self, *args, **options):
        self.set_logger_level(int(options['verbosity']))
        only_step = self._get_and_validate_only_step(options['only'])
        gene_sources = self._get_and_validate_source(options['source'])
        if only_step == "clean":
            clean_db()
        if only_step is None or only_step == "counts":
            compute_gene_counts.delay(gene_sources)
        if only_step is None or only_step == "gene-length":
            compute_gene_length.delay(gene_sources)
        if only_step is None or only_step == "taxonomy_repartition":
            compute_taxonomy_repartition.delay(gene_sources)
        if only_step is None or only_step == "taxonomy_presence":
            compute_taxonomy_presence.delay(gene_sources)
