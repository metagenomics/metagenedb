import logging

from django.core.management.base import BaseCommand
from slugify import slugify

from metagenedb.apps.catalog.management.commands.commons.handle_functions import HandleFunctions
from metagenedb.apps.catalog.management.commands.commons.handle_taxonomy import HandleTaxonomy
from metagenedb.apps.catalog.management.commands.commons.import_genes import BaseImportGenes
from metagenedb.common.utils.parsers import IGCLineParser

logging.basicConfig(format='[%(asctime)s] %(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)


class ImportIGCGenes(BaseImportGenes, HandleFunctions, HandleTaxonomy):
    PHYLUM_COL = 'taxo_phylum'
    GENUS_COL = 'taxo_genus'
    SELECTED_KEYS = ['gene_id', 'length', 'kegg_ko', 'eggnog', PHYLUM_COL, GENUS_COL]
    IMPORT_TYPE = "IGC genes"  # For logs
    UPDATED_FIELDS = ['length', 'name', 'source', 'taxonomy']
    SOURCE = 'igc'
    PARSER = IGCLineParser

    def __init__(self, annotation_file, skip_tax=False, skip_functions=False):
        super().__init__(annotation_file)
        # Skip some insertion if specified in script options
        self.skip_tax = skip_tax
        self.skip_functions = skip_functions

    def _format_for_model(self, igc_dict):
        gene_dict = super()._format_for_model(igc_dict)
        if not self.skip_tax:
            taxonomy = self._retrieve_taxonomy(igc_dict.get('taxo_genus'), rank="genus")
            if taxonomy is None:
                taxonomy = self._retrieve_taxonomy(igc_dict.get('taxo_phylum'), rank="phylum")
        gene_dict['taxonomy'] = taxonomy
        return gene_dict

    def _handle_chunk(self, chunk_genes):
        gene_dict_list = [self._parse_gene(i) for i in chunk_genes]
        functions = self._remove_functions(gene_dict_list)
        gene_clean_dict = {slugify(i['gene_id']): self._format_for_model(i) for i in gene_dict_list}
        self.create_or_update_genes(gene_clean_dict)
        if not self.skip_functions:
            self.link_genes_to_functions(functions)


class Command(BaseCommand):
    help = 'Create or update IGC genes from IGC annotations file.'

    def add_arguments(self, parser):
        parser.add_argument('annotation', help='IGC.annotation_OF.summary file from IGC')
        parser.add_argument('--skip_taxonomy', action='store_true', help='Skip taxonomy information from genes.')
        parser.add_argument('--skip_functions', action='store_true', help='Skip functions information from genes.')
        parser.add_argument('--test', action='store_true', help='Run only on first 10000 entries.')

    def set_logger_level(self, verbosity):
        if verbosity > 2:
            logger.setLevel(logging.DEBUG)
        elif verbosity > 1:
            logger.setLevel(logging.INFO)

    def handle(self, *args, **options):
        self.set_logger_level(int(options['verbosity']))
        import_igc = ImportIGCGenes(options['annotation'], skip_tax=options['skip_taxonomy'],
                                    skip_functions=options['skip_functions'])
        import_igc.load_all(test=options['test'])
