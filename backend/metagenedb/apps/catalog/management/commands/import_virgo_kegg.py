import logging

from django.core.management.base import BaseCommand
from slugify import slugify

from metagenedb.apps.catalog.management.commands.commons.handle_functions import HandleFunctions
from metagenedb.apps.catalog.management.commands.commons.import_genes import BaseImportGenes
from metagenedb.common.utils.parsers import VirgoKEGGLineParser

logging.basicConfig(format='[%(asctime)s] %(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)


class ImportVirgoGeneKeggAnnotation(BaseImportGenes, HandleFunctions):

    IMPORT_TYPE = "Virgo KEGG annotations"  # For logs
    SELECTED_KEYS = ['gene_id', 'kegg_ko']
    UPDATED_FIELDS = ['name']
    SOURCE = 'virgo'
    PARSER = VirgoKEGGLineParser

    FUN_SOURCE_TO_DELETE = ['kegg']

    def _clean_functions(self, functions, unknown_val='unknown'):
        """
        Get rid of functions that are not in the db or entitled unknown
        """
        cleaned_functions = {}
        for gene_id, all_functions in functions.items():
            new_functions = []
            kegg_annotation = all_functions['kegg']
            if kegg_annotation == unknown_val:
                continue
            elif kegg_annotation in self.kegg_mapping.keys():
                new_functions.append(self.kegg_mapping[kegg_annotation])
            if new_functions:
                cleaned_functions[gene_id] = new_functions
        return cleaned_functions

    def _remove_functions(self, gene_dicts):
        functions = {}
        for gene_dict in gene_dicts:
            functions[slugify(gene_dict['gene_id'])] = {
                    'kegg': gene_dict.pop('kegg_ko'),
                }
        return functions

    def _format_for_model(self, ori_gene_dict):
        """
        @TODO remove in the future and makes function from parent class more modulable
        """
        gene_dict = {}
        gene_dict['gene_id'] = slugify(ori_gene_dict['gene_id'])
        gene_dict['name'] = ori_gene_dict['gene_id']
        gene_dict['source'] = self.SOURCE
        return gene_dict

    def _handle_chunk(self, chunk_genes):
        """
        Overide for all different sources
        """
        gene_dict_list = [self._parse_gene(i) for i in chunk_genes]
        functions = self._remove_functions(gene_dict_list)
        gene_clean_dict = {slugify(i['gene_id']): self._format_for_model(i) for i in gene_dict_list}
        self.create_or_update_genes(gene_clean_dict)
        self.link_genes_to_functions(functions)


class Command(BaseCommand):
    help = 'Create or update all KEGG annotation for Virgo genes (from `8.A.kegg.ortholog.txt` file).'

    def add_arguments(self, parser):
        parser.add_argument(
            'annotation',
            help='8.A.kegg.ortholog.txt file from Virgo. Genes need to exist in DB for this script to work.'
        )
        parser.add_argument('--test', action='store_true', help='Run only on first 10000 entries.')

    def set_logger_level(self, verbosity):
        if verbosity > 2:
            logger.setLevel(logging.DEBUG)
        elif verbosity > 1:
            logger.setLevel(logging.INFO)

    def handle(self, *args, **options):
        self.set_logger_level(int(options['verbosity']))
        import_annotations = ImportVirgoGeneKeggAnnotation(options['annotation'])
        import_annotations.load_all(test=options['test'])
