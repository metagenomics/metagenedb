from unittest import TestCase

from rest_framework.test import APITestCase

from metagenedb.apps.catalog.models import GeneFunction
from metagenedb.apps.catalog.management.commands.commons.handle_functions import HandleFunctions
from metagenedb.apps.catalog.factory import (
    FunctionFactory,
    GeneFactory,
)


class BaseTestHandleFunctions(TestCase):

    def setUp(self):
        self.handle_functions = HandleFunctions()


class TestRemoveFunctions(BaseTestHandleFunctions):

    def test_remove_functions(self):
        input_dicts = [{
            'gene_id': 'Test_gene',
            'kegg_ko': ['K0001'],
            'eggnog': ['COG1', 'COG2']
        }]
        expected_functions = {
            'test-gene': {
                'kegg': ['K0001'],
                'eggnog': ['COG1', 'COG2']
            }
        }
        tested_dict = self.handle_functions._remove_functions(input_dicts)
        self.assertDictEqual(tested_dict, expected_functions)


class TestCleanFunctions(APITestCase, BaseTestHandleFunctions):

    @classmethod
    def setUpTestData(cls):
        cls.kegg = FunctionFactory(source='kegg')
        cls.eggnog = FunctionFactory(source='eggnog')

    def test_clean_functions_kegg_only(self):
        functions = {
            'gene-kegg': {
                'kegg': [self.kegg.function_id, 'KO12345'],
                'eggnog': ['unknown']
            },
        }
        expected_functions = {
            'gene-kegg': [self.kegg]
        }
        self.assertDictEqual(self.handle_functions._clean_functions(functions), expected_functions)

    def test_clean_functions_eggnog_only(self):
        functions = {
            'gene-kegg': {
                'kegg': ['unknown'],
                'eggnog': [self.eggnog.function_id, 'COG12345']
            },
        }
        expected_functions = {
            'gene-kegg': [self.eggnog]
        }
        self.assertDictEqual(self.handle_functions._clean_functions(functions), expected_functions)

    def test_clean_functions_kegg_eggnog(self):
        functions = {
            'gene-kegg': {
                'kegg': [self.kegg.function_id, 'KO12345'],
                'eggnog': [self.eggnog.function_id, 'COG12345']
            },
        }
        expected_functions = {
            'gene-kegg': [self.kegg, self.eggnog]
        }
        self.assertDictEqual(self.handle_functions._clean_functions(functions), expected_functions)

    def test_clean_functions_both_unknown(self):
        functions = {
            'gene-kegg': {
                'kegg': ['unknown'],
                'eggnog': ['unknown']
            },
        }
        expected_functions = {}
        self.assertDictEqual(self.handle_functions._clean_functions(functions), expected_functions)


class TestLinkGenesToFunctions(APITestCase, BaseTestHandleFunctions):

    @classmethod
    def setUpTestData(cls):
        cls.kegg = FunctionFactory(source='kegg')
        cls.eggnog = FunctionFactory(source='eggnog')
        cls.gene = GeneFactory()

    def test_link_kegg_and_eggnog(self):
        self.assertEqual(GeneFunction.objects.all().count(), 0)
        functions = {
            self.gene.gene_id: {
                'kegg': [self.kegg.function_id],
                'eggnog': [self.eggnog.function_id]
            }
        }
        self.handle_functions.link_genes_to_functions(functions)
        gene_functions = GeneFunction.objects.all()
        self.assertEqual(gene_functions.count(), 2)
        for link in gene_functions:
            self.assertEqual(link.gene.gene_id, self.gene.gene_id)
