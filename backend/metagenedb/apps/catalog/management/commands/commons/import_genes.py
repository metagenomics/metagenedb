import logging
from itertools import islice

from slugify import slugify

from metagenedb.apps.catalog.models import Gene
from metagenedb.common.utils.chunks import file_len

logging.basicConfig(format='[%(asctime)s] %(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)


class BaseImportGenes(object):
    IMPORT_TYPE = "gene"  # For logs
    SELECTED_KEYS = ['gene_id', 'length']
    UPDATED_FIELDS = ['length', 'name', 'source']
    SOURCE = 'undef'
    PARSER = None

    def __init__(self, annotation_file):
        self.annotation_file = annotation_file
        self.total_genes = file_len(annotation_file)
        self._reset_counters()

    def _reset_counters(self):
        self.processed_genes = 0
        self.created_genes = 0
        self.updated_genes = 0
        self.skipped_genes = 0

    def _parse_gene(self, raw_line):
        gene_parser = self.PARSER
        all_dict = gene_parser.gene(raw_line)
        selected_dict = {k: v for k, v in all_dict.items() if k in self.SELECTED_KEYS}
        return selected_dict

    def _format_for_model(self, ori_gene_dict):
        gene_dict = {}
        gene_dict['gene_id'] = slugify(ori_gene_dict['gene_id'])
        gene_dict['name'] = ori_gene_dict['gene_id']
        gene_dict['length'] = ori_gene_dict['length']
        gene_dict['source'] = self.SOURCE
        return gene_dict

    def _update_genes(self, gene_instances, gene_dict):
        for gene_id, gene_instance in gene_instances.items():
            for key, value in gene_dict[gene_id].items():
                setattr(gene_instance, key, value)
        try:
            Gene.objects.bulk_update(
                list(gene_instances.values()),
                self.UPDATED_FIELDS
            )
            self.updated_genes += len(gene_instances.keys())
        except Exception as exception:
            logger.warning(exception)
            self.skipped_genes += len(gene_instances.keys())

    def _create_genes(self, gene_list):
        try:
            Gene.objects.bulk_create(
                [Gene(**item) for item in gene_list]
            )
            self.created_genes += len(gene_list)
        except Exception as exception:
            logger.warning(exception)
            self.skipped_genes += len(gene_list)

    def create_or_update_genes(self, gene_dict):
        update_instances = Gene.objects.in_bulk(gene_dict.keys(), field_name='gene_id')
        self._update_genes(update_instances, gene_dict)
        gene_ids_to_create = set(gene_dict.keys()) - set(update_instances.keys())
        if gene_ids_to_create:
            self._create_genes([gene_dict[gene_id] for gene_id in gene_ids_to_create])

    def _handle_chunk(self, chunk_genes):
        """
        Overide for all different sources
        """
        gene_dict_list = [self._parse_gene(i) for i in chunk_genes]
        gene_clean_dict = {slugify(i['gene_id']): self._format_for_model(i) for i in gene_dict_list}
        self.create_or_update_genes(gene_clean_dict)

    def load_all(self, test=False, chunk_size=10000):
        logger.info("Starting %s import (creation or update) to DB", self.IMPORT_TYPE)
        with open(self.annotation_file, 'r') as file:
            while True:
                chunk_genes = list(islice(file, chunk_size))
                if not chunk_genes:
                    break
                self._handle_chunk(chunk_genes)
                self.processed_genes += chunk_size
                logger.info("%s Genes processed so far...", self.processed_genes)
                if test is True:
                    break
        logger.info("[DONE] %s/%s Genes created.", self.created_genes, self.total_genes)
        logger.info("[DONE] %s/%s Genes updated.", self.updated_genes, self.total_genes)
        logger.info("[DONE] %s/%s Genes skipped.", self.skipped_genes, self.total_genes)
