import logging

import pyfastx
from slugify import slugify

from metagenedb.apps.catalog.models import Gene

logging.basicConfig(format='[%(asctime)s] %(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)


class ImportGeneSequences(object):
    CATALOG = "CAT_NAME"

    def __init__(self, sequence_file):
        self.sequence_file = sequence_file
        self._reset_counters()

    def _reset_counters(self):
        self.processed_genes = 0
        self.updated_genes = 0
        self.skipped_genes = 0

    def update_sequences(self, sequences):
        genes = Gene.objects.filter(gene_id__in=sequences.keys())
        genes_retrieved = genes.count()
        for gene in genes:
            gene.sequence = sequences[gene.gene_id]
        try:
            Gene.objects.bulk_update(genes, ['sequence'])
            self.updated_genes += genes_retrieved
            self.skipped_genes += len(sequences) - genes_retrieved
        except Exception:
            logger.warning("Could not update genes... skipped.")
            self.skipped_genes += len(sequences)

    def load_all(self, test=False, chunk_size=10000, skip_n_sequences=0):
        logger.info("Starting %s Gene sequences import (update) to DB", self.CATALOG)
        if skip_n_sequences > 0:
            logger.info("Skipping first %s sequences", skip_n_sequences)
        current_sequences = {}
        for name, seq in pyfastx.Fasta(self.sequence_file, build_index=False):
            if self.processed_genes < skip_n_sequences:
                self.processed_genes += 1
                self.skipped_genes += 1
                continue
            current_sequences[slugify(name.split()[0])] = seq
            self.processed_genes += 1
            if self.processed_genes % chunk_size == 0:
                self.update_sequences(current_sequences)
                logger.info("%s Gene sequences processed so far...", self.processed_genes)
                current_sequences = {}
                if test is True:
                    break
        if len(current_sequences) > 0:
            self.update_sequences(current_sequences)
        logger.info("[DONE] %s/%s Gene sequences updated.", self.updated_genes, self.processed_genes)
        logger.info("[DONE] %s/%s Genes skipped.", self.skipped_genes, self.processed_genes)
