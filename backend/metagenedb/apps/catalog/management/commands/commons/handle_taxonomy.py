import logging

from metagenedb.apps.catalog.models import Taxonomy

logging.basicConfig(format='[%(asctime)s] %(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)


class HandleTaxonomy:
    MANUAL_TAXO_MAPPING = {}

    def _build_taxo_mapping(self, rank):
        logger.info("Building local mapping for %s level...", rank)
        instances = Taxonomy.objects.filter(rank=rank)
        return {instance.name: instance for instance in instances}

    @property
    def phylum_mapping(self):
        if getattr(self, '_phylum_mapping', None) is None:
            self._phylum_mapping = self._build_taxo_mapping("phylum")
        return self._phylum_mapping

    @property
    def genus_mapping(self):
        if getattr(self, '_genus_mapping', None) is None:
            self._genus_mapping = self._build_taxo_mapping("genus")
        return self._genus_mapping

    @property
    def species_mapping(self):
        if getattr(self, '_species_mapping', None) is None:
            self._species_mapping = self._build_taxo_mapping("species")
        return self._species_mapping

    def _build_manual_mapping(self):
        mapping = {}
        for key, tax_id in self.MANUAL_TAXO_MAPPING.items():
            mapping[key] = Taxonomy.objects.get(tax_id=tax_id)
        return mapping

    @property
    def manual_mapping(self):
        if getattr(self, '_manual_mapping', None) is None:
            self._manual_mapping = self._build_manual_mapping()
        return self._manual_mapping

    def _retrieve_taxonomy(self, name, rank='species', unknown_val='unknown'):
        taxonomy_instance = None
        if name != unknown_val:
            taxonomy_instance = getattr(self, f"{rank}_mapping", {}).get(name, None)
        return taxonomy_instance
