from unittest import TestCase

import mock
from rest_framework.test import APITestCase

from metagenedb.apps.catalog.models import Gene
from metagenedb.apps.catalog.management.commands.commons.import_genes import BaseImportGenes
from metagenedb.apps.catalog.factory import (
    GeneFactory,
)


class ParserTest:
    """Simple parser for test purposes"""

    @staticmethod
    def gene(line):
        gene_info = line.rstrip().split('\t')
        return {
            'gene_id': gene_info[0],
            'length': gene_info[1],
        }


class BaseTestImportGenes(TestCase):

    def setUp(self):
        function_to_mock = 'metagenedb.apps.catalog.management.commands.commons.import_genes.file_len'
        with mock.patch(function_to_mock) as MockFileLen:
            MockFileLen.return_value = 10
            self.import_genes = BaseImportGenes('test')
            self.import_genes.PARSER = ParserTest


class TestParseGene(BaseTestImportGenes):

    def setUp(self):
        raw_data = [
            'gene_ID',
            'length',
        ]
        self.raw_line = "\t".join(raw_data)
        super().setUp()

    def test_parse_gene_default_selected_keys(self):
        """
        This test should failed and need to be updated when SELECTED_KEYS are changed
        """
        expected_dict = {
            'gene_id': 'gene_ID',
            'length': 'length',
        }
        tested_dict = self.import_genes._parse_gene(self.raw_line)
        self.assertDictEqual(tested_dict, expected_dict)


class TestCreateOrUpdateGenes(APITestCase, BaseTestImportGenes):

    @classmethod
    def setUpTestData(cls):
        cls.gene = GeneFactory()

    def test_create_1_update_1(self):
        gene_to_update = {
            'gene_id': self.gene.gene_id,
            'name': 'Updated Gene',
            'length': 2235,
        }
        gene_to_create = {
            'gene_id': 'gene-create-123',
            'name': 'Created Gene',
            'length': 5629,
        }
        gene_dict = {
            gene_to_update['gene_id']: gene_to_update,
            gene_to_create['gene_id']: gene_to_create
        }
        self.import_genes.create_or_update_genes(gene_dict)
        self.assertEqual(Gene.objects.all().count(), 2)
        # Check updated gene
        updated_gene = Gene.objects.get(gene_id=gene_to_update['gene_id'])
        for key, value in gene_to_update.items():
            self.assertEqual(getattr(updated_gene, key), value)
        # Check created gene
        created_gene = Gene.objects.get(gene_id=gene_to_create['gene_id'])
        for key, value in gene_to_create.items():
            self.assertEqual(getattr(created_gene, key), value)
