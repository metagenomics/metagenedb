import os

from rest_framework.test import APITestCase

from metagenedb.apps.catalog.models import Gene
from metagenedb.apps.catalog.management.commands.commons.import_gene_sequences import ImportGeneSequences
from metagenedb.apps.catalog.factory import (
    GeneFactory,
)


class TestUpdateSequences(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.gene = GeneFactory()

    def setUp(self):
        self.import_igc_seq = ImportGeneSequences("test")  # we never make real reference to the sequence_file

    def test_update_sequence(self):
        seq = "ACTG"
        sequences = {
            self.gene.gene_id: seq
        }
        self.assertFalse(Gene.objects.get(gene_id=self.gene.gene_id).sequence)
        self.import_igc_seq.update_sequences(sequences)
        self.assertEqual(Gene.objects.get(gene_id=self.gene.gene_id).sequence, seq)


class TestEndToEnd(APITestCase):

    @classmethod
    def setUpTestData(cls):
        GeneFactory.create(gene_id="gene1")
        GeneFactory.create(gene_id="gene2")

    def test_end_to_end(self):
        test_file = os.path.join(os.path.dirname(__file__), "./test_files/genes.fa")
        loader = ImportGeneSequences(test_file)
        expected_genes = {
            'gene1': {
                'sequence': 'ACGT'
            },
            'gene2': {
                'sequence': 'ATCG'
            },
        }
        loader.load_all()
        created_genes = Gene.objects.all()
        for created_gene in created_genes:
            self.assertEqual(getattr(created_gene, 'sequence'), expected_genes[created_gene.gene_id]['sequence'])
