from rest_framework.test import APITestCase

from metagenedb.apps.catalog.factory import (
    TaxonomyFactory,
)

from metagenedb.apps.catalog.management.commands.commons.handle_taxonomy import HandleTaxonomy


class TestRetrieveTaxonomy(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.genus = TaxonomyFactory(rank='genus')
        cls.phylum = TaxonomyFactory(rank='phylum')

    def setUp(self):
        self.unknown = 'unknown'
        self.handle_taxonomy = HandleTaxonomy()

    def test_genus_only(self):
        tested_taxonomy = self.handle_taxonomy._retrieve_taxonomy(
            self.genus.name, rank='genus', unknown_val=self.unknown
        )
        self.assertEqual(tested_taxonomy.tax_id, self.genus.tax_id)

    def test_genus_not_in_db(self):
        tested_taxonomy = self.handle_taxonomy._retrieve_taxonomy("Fake Name", rank="genus", unknown_val=self.unknown)
        self.assertEqual(tested_taxonomy, None)

    def test_phylum_only(self):
        tested_taxonomy = self.handle_taxonomy._retrieve_taxonomy(
            self.phylum.name, rank="phylum", unknown_val=self.unknown
        )
        self.assertEqual(tested_taxonomy.tax_id, self.phylum.tax_id)

    def test_phylum_not_in_db(self):
        tested_taxonomy = self.handle_taxonomy._retrieve_taxonomy(self.unknown, "Fake Name")
        self.assertEqual(tested_taxonomy, None)

    def test_both_unknown(self):
        tested_taxonomy = self.handle_taxonomy._retrieve_taxonomy(self.unknown)
        self.assertEqual(tested_taxonomy, None)

    def test_build_manual_mapping(self):
        self.handle_taxonomy.MANUAL_TAXO_MAPPING = {
            'test_manual': self.genus.tax_id
        }
        tested_taxonomy = self.handle_taxonomy._retrieve_taxonomy(
            'test_manual', rank='manual', unknown_val=self.unknown
        )
        self.assertEqual(tested_taxonomy.tax_id, self.genus.tax_id)
