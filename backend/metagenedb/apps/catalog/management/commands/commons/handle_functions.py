import logging

from slugify import slugify

from metagenedb.apps.catalog.models import Function, Gene, GeneFunction

logging.basicConfig(format='[%(asctime)s] %(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)


class HandleFunctions:
    FUN_SOURCE_TO_DELETE = ['kegg', 'eggnog']  # links to get rid of everytime

    def _build_function_mapping(self, source):
        logger.info("Building local mapping for %s function...", source)
        instances = Function.objects.filter(source=source)
        return {instance.function_id: instance for instance in instances}

    @property
    def eggnog_mapping(self):
        if getattr(self, '_eggnog_mapping', None) is None:
            self._eggnog_mapping = self._build_function_mapping("eggnog")
        return self._eggnog_mapping

    @property
    def kegg_mapping(self):
        if getattr(self, '_kegg_mapping', None) is None:
            self._kegg_mapping = self._build_function_mapping("kegg")
        return self._kegg_mapping

    def _clean_functions(self, functions, unknown_val='unknown'):
        """
        Get rid of functions that are not in the db or entitled unknown
        """
        cleaned_functions = {}
        for gene_id, all_functions in functions.items():
            new_functions = []
            for kegg in all_functions['kegg']:
                if kegg == unknown_val:
                    continue
                elif kegg in self.kegg_mapping.keys():
                    new_functions.append(self.kegg_mapping[kegg])
            for eggnog in all_functions['eggnog']:
                if eggnog == unknown_val:
                    continue
                elif eggnog in self.eggnog_mapping.keys():
                    new_functions.append(self.eggnog_mapping[eggnog])
            if new_functions:
                cleaned_functions[gene_id] = new_functions
        return cleaned_functions

    def _remove_functions(self, gene_dicts):
        functions = {}
        for gene_dict in gene_dicts:
            functions[slugify(gene_dict['gene_id'])] = {
                    'kegg': gene_dict.pop('kegg_ko'),
                    'eggnog': gene_dict.pop('eggnog')
                }
        return functions

    def _generate_gene_function_mapping(self, functions, genes):
        """
        Generate a list of GeneFunction pair to create relation between them
        """
        mapping = []
        for gene_id, function_list in functions.items():
            for function in function_list:
                mapping.append(GeneFunction(gene=genes[gene_id], function=function))
        return mapping

    def _delete_previous_annotations(self, genes):
        for function_source in self.FUN_SOURCE_TO_DELETE:
            GeneFunction.objects.filter(gene__in=genes.values(), function__source=function_source).delete()

    def link_genes_to_functions(self, functions):
        cleaned_functions = self._clean_functions(functions)
        genes = Gene.objects.in_bulk(cleaned_functions.keys(), field_name='gene_id')
        # Get all link with corresponding genes & Delete them
        self._delete_previous_annotations(genes)
        # Generate table for bulk_create of function <-> gene and create it
        GeneFunction.objects.bulk_create(
            self._generate_gene_function_mapping(cleaned_functions, genes)
        )
