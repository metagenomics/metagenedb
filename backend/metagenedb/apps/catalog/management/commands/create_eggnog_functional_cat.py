import logging

from django.core.management.base import BaseCommand
from django.core.exceptions import ValidationError

from metagenedb.apps.catalog.models import EggNogFunctionalCategory
from metagenedb.common.utils.parsers.eggnog import EggNOGFunctionalCategoriesParser

logging.basicConfig(format='[%(asctime)s] %(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)


class ImportEggNOGFunctionalCategories(object):
    LOOKUP_FIELD = 'category_id'
    MODEL = EggNogFunctionalCategory
    GROUP_MAPPING = {
        'Information storage and processing': 'info_storage_processing',
        'Cellular processes and signaling': 'cellular_processes_signaling',
        'Metabolism': 'metabolism',
        'Poorly characterized': 'poorly_characterized'
    }

    def __init__(self, file_path):
        self.parser = EggNOGFunctionalCategoriesParser(file_path)
        self.processed_count = 0
        self.created_count = 0
        self.updated_count = 0
        self.skipped_count = 0
        self.skipped_ids = []
        self.skipped_errors = []

    def _create_instance(self, payload):
        instance = self.MODEL(**payload)
        instance.full_clean()
        instance.save()
        self.created_count += 1

    def _update_instance(self, payload):
        instance = self.MODEL.objects.get(**{self.LOOKUP_FIELD: payload.get(self.LOOKUP_FIELD)})
        for k, v in payload.items():
            setattr(instance, k, v)
        instance.full_clean()
        instance.save()
        self.updated_count += 1

    def _handle_error(self, payload, error):
        logger.error(error)
        self.skipped_errors.append(error)
        self.skipped_ids.append(payload.get(self.LOOKUP_FIELD))
        self.skipped_count += 1

    def update_group_name(self, functional_category):
        functional_category['group'] = self.GROUP_MAPPING.get(functional_category['group'], None)
        return functional_category

    def load_all(self):
        for functional_category in self.parser.parse():
            functional_category = self.update_group_name(functional_category)
            try:
                self._create_instance(functional_category)
            except ValidationError as validation_error:
                if self.LOOKUP_FIELD in validation_error.error_dict.keys():
                    try:
                        self._update_instance(functional_category)
                    except ValidationError as validation_error:
                        self._handle_error(functional_category, validation_error)
            self.processed_count += 1
        logger.info("[DONE] %s EggNOG functional categories created.", self.created_count)
        logger.info("[DONE] %s EggNOG functional categories updated.", self.updated_count)
        logger.info("[DONE] %s EggNOG functional categories skipped. List: %s", self.skipped_count, self.skipped_ids)


class Command(BaseCommand):
    help = 'Create or update all EggNOG functional categories from COG_functional_categories.txt file.'

    def add_arguments(self, parser):
        parser.add_argument('functional_categories', help='COG_functional_categories.txt file from EggNOG')

    def set_logger_level(self, verbosity):
        if verbosity > 2:
            logger.setLevel(logging.DEBUG)
        elif verbosity > 1:
            logger.setLevel(logging.INFO)

    def handle(self, *args, **options):
        self.set_logger_level(int(options['verbosity']))
        import_functional_cat = ImportEggNOGFunctionalCategories(options['functional_categories'])
        import_functional_cat.load_all()
