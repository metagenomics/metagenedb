import logging

from django.core.management.base import BaseCommand

from metagenedb.apps.catalog.models import Taxonomy
from metagenedb.common.utils.chunks import dict_chunks

logging.basicConfig(format='[%(asctime)s] %(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)

SELECT_RELATED_PARENT = "parent"


class HierarchyBuilder:

    def __init__(self, queryset):
        self.queryset = queryset
        self.total_tax = queryset.count()
        self.processed_tax = 0
        self.hierarchy_built = 0
        self.hierarchy_failed = 0

    def get_local_taxo(self):
        logger.info("Building local db of all taxonomy entries...")
        self.taxo_dict = {item.tax_id: {
            'name': item.name, 'rank': item.rank, 'parent': item.parent.tax_id
        } for item in self.queryset.iterator(chunk_size=10000)}
        logger.info("[DONE] Local db of all taxonomy entries.")

    def _build_instance_hierarchy(self, tax_id):
        hierarchy = {}
        current_taxo = self.taxo_dict[tax_id]
        if current_taxo['name'] != 'root':
            hierarchy[current_taxo['rank']] = {
                'tax_id': tax_id,
                'name': current_taxo['name']
            }
            hierarchy = {**hierarchy, **self._build_instance_hierarchy(current_taxo['parent'])}
        return hierarchy

    def build_hierarchy(self, instances):
        for instance in instances:
            instance.hierarchy = self._build_instance_hierarchy(instance.tax_id)
        return instances

    def build_all(self, chunk_size=10000, test=False):
        logger.info("Building all hierarchy for all %s taxonomy items...", self.total_tax)
        self.get_local_taxo()
        for chunk in dict_chunks(self.taxo_dict, chunk_size):
            try:
                instances = Taxonomy.objects.filter(tax_id__in=chunk.keys())
                instances = self.build_hierarchy(instances)
                Taxonomy.objects.bulk_update(
                    instances,
                    ['hierarchy']
                )
                self.hierarchy_built += len(chunk)
            except Exception as exception:
                self.hierarchy_failed += len(chunk)
                logger.warning("An error occured, chunk skipped %s", exception)
            self.processed_tax += len(chunk)
            logger.info("%s/%s Taxonomy processed so far...", self.processed_tax, self.total_tax)
            if test is True:
                break
        logger.info("[DONE] %s/%s Hierarchy built.", self.hierarchy_built, self.total_tax)
        logger.info("[DONE] %s/%s Hierarchy build skipped.", self.hierarchy_failed, self.total_tax)


class Command(BaseCommand):
    help = 'Build hierarchy for taxonomy entries.'

    def add_arguments(self, parser):
        parser.add_argument('--test', action='store_true', help='Run only on first 10000 entries.')

    def set_logger_level(self, verbosity):
        if verbosity > 2:
            logger.setLevel(logging.DEBUG)
        elif verbosity > 1:
            logger.setLevel(logging.INFO)

    def get_queryset(self):
        return Taxonomy.objects.select_related(SELECT_RELATED_PARENT).all()

    def handle(self, *args, **options):
        self.set_logger_level(int(options['verbosity']))
        hierarchy_builder = HierarchyBuilder(self.get_queryset())
        hierarchy_builder.build_all(test=options['test'])
