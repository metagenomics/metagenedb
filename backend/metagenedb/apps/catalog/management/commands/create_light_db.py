import logging
from random import randint

from django.core.management.base import BaseCommand

from metagenedb.apps.catalog.factory import GeneFactory, GeneWithEggNOGFactory, GeneWithKeggFactory
from metagenedb.apps.catalog.factory.taxonomy import generate_simple_db as gen_tax_db
from metagenedb.apps.catalog.models import (
    Gene, Function, Taxonomy
)
from metagenedb.apps.catalog.management.commands.compute_stats import (
    ComputeCounts, ComputeGeneLength, ComputeTaxonomyRepartition, ComputeTaxonomyPresence
)
from metagenedb.apps.catalog.management.commands.compute_stats import clean_db as clean_db_stats

logging.basicConfig(format='[%(asctime)s] %(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger()


def empty_db():
    Gene.objects.all().delete()
    Taxonomy.objects.all().delete()
    Function.objects.all().delete()


def create_taxonomy_db():
    gen_tax_db()


def create_genes_db():
    GeneFactory.create_batch(50)
    GeneWithEggNOGFactory.create_batch(15)
    GeneWithKeggFactory.create_batch(12)
    for tax in Taxonomy.objects.all():
        GeneFactory.create_batch(randint(1, 10), taxonomy=tax)
        GeneWithEggNOGFactory.create(taxonomy=tax)
        GeneWithKeggFactory.create(taxonomy=tax)


def compute_stats():
    clean_db_stats()
    for gene_source in ['all', 'virgo', 'igc']:
        ComputeCounts(gene_source).all()
        ComputeGeneLength(gene_source).all()
        ComputeTaxonomyRepartition(gene_source).all()
        ComputeTaxonomyPresence(gene_source).all()


def create_small_db():
    empty_db()
    create_taxonomy_db()
    create_genes_db()
    compute_stats()


class Command(BaseCommand):
    help = 'Create a light DB with random items to illustrate functionnalities of the application.'

    def set_logger_level(self, verbosity):
        if verbosity > 2:
            logger.setLevel(logging.DEBUG)
        elif verbosity > 1:
            logger.setLevel(logging.INFO)

    def handle(self, *args, **options):
        self.set_logger_level(int(options['verbosity']))
        create_small_db()
