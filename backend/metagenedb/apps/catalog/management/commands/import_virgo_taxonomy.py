import logging

from django.core.management.base import BaseCommand
from slugify import slugify

from metagenedb.apps.catalog.management.commands.commons.handle_taxonomy import HandleTaxonomy
from metagenedb.apps.catalog.management.commands.commons.import_genes import BaseImportGenes
from metagenedb.common.utils.parsers import VirgoTaxonomyLineParser

logging.basicConfig(format='[%(asctime)s] %(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)


class ImportVirgoGeneTaxonomyAnnotation(BaseImportGenes, HandleTaxonomy):
    SELECTED_KEYS = ['gene_id', 'taxonomy']
    IMPORT_TYPE = "Virgo taxonomy annotations"  # For logs
    UPDATED_FIELDS = ['name', 'taxonomy']
    SOURCE = 'virgo'
    PARSER = VirgoTaxonomyLineParser
    MANUAL_TAXO_MAPPING = {
        'BVAB1': '699240',
        'Clostridiales Family': '186802',
        'Chlamydophila psittaci': '83554'
    }

    def _format_for_model(self, ori_gene_dict):
        """
        @TODO remove in the future and makes function from parent class more modulable
        """
        gene_dict = {}
        gene_dict['gene_id'] = slugify(ori_gene_dict['gene_id'])
        gene_dict['name'] = ori_gene_dict['gene_id']
        gene_dict['source'] = self.SOURCE
        taxonomy_term = ori_gene_dict.get('taxonomy').replace('_', ' ')
        taxonomy = self._retrieve_taxonomy(taxonomy_term, rank="species")
        if taxonomy is None:
            # Use manually created mapping dict
            taxonomy = self._retrieve_taxonomy(taxonomy_term.split(' ')[0], rank="genus")
        if taxonomy is None:
            # Try to at least retrieve the genus from the first part of the taxonomy
            taxonomy = self._retrieve_taxonomy(taxonomy_term, rank="genus")
            # @TODO need to find a way of handling other cases
        if taxonomy is None:
            # Use manually created mapping dict
            taxonomy = self._retrieve_taxonomy(taxonomy_term, rank="manual")
        if taxonomy is None:
            self.skipped_genes += 1
            logger.warning("Could not retrieve %s for %s", ori_gene_dict.get('taxonomy'), ori_gene_dict['gene_id'])
        gene_dict['taxonomy'] = taxonomy
        return gene_dict


class Command(BaseCommand):
    help = 'Create or update all Taxonomy annotations for Virgo genes (from `1.taxon.tbl.txt` file).'

    def add_arguments(self, parser):
        parser.add_argument(
            'annotation',
            help='1.taxon.tbl.txt file from Virgo. Genes need to exist in DB for this script to work.'
        )
        parser.add_argument('--test', action='store_true', help='Run only on first 10000 entries.')

    def set_logger_level(self, verbosity):
        if verbosity > 2:
            logger.setLevel(logging.DEBUG)
        elif verbosity > 1:
            logger.setLevel(logging.INFO)

    def handle(self, *args, **options):
        self.set_logger_level(int(options['verbosity']))
        import_annotations = ImportVirgoGeneTaxonomyAnnotation(options['annotation'])
        import_annotations.load_all(test=options['test'])
