"""
Settings specific to metageneDB app and independant of Django
"""
import environ


root = environ.Path(__file__) - 3  # get root of the project
env = environ.Env()
environ.Env.read_env(root('.env'))  # reading .env file

# Maximum number of FASTA genes able to retrieve through API
MAX_DOWNLOAD_GENES = env.str('MAX_DOWNLOAD_GENES', default=100000)
