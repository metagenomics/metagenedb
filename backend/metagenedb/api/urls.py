from django.urls import include, path
from django.conf.urls import url
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions


schema_view = get_schema_view(
   openapi.Info(
      title="Metagenedb API",
      default_version='v1',
      description="API documentation for metagenedb",
      contact=openapi.Contact(email="kehillio@pasteur.fr"),
      license=openapi.License(name="License not defined"),
   ),
   public=False,
   permission_classes=(permissions.AllowAny,),
)


urlpatterns = [
    path('auth/', include(('metagenedb.api.accounts.urls', 'auth'))),
    path('catalog/', include(('metagenedb.api.catalog.urls', 'catalog'))),
    url(r'swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
