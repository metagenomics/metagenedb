from django_filters import rest_framework as filters
from metagenedb.apps.catalog.models import Function, EggNOG


class FunctionFilter(filters.FilterSet):

    class Meta:
        model = Function
        fields = ['source']


class EggNOGFilter(filters.FilterSet):

    class Meta:
        model = EggNOG
        fields = ['version']
