from django_filters import rest_framework as filters
from metagenedb.apps.catalog.models import Function, Gene, Taxonomy


class GeneFilter(filters.FilterSet):
    has_taxonomy = filters.BooleanFilter(
        field_name='taxonomy', lookup_expr="isnull", exclude=True
    )
    has_functions = filters.BooleanFilter(
        field_name='functions', lookup_expr="isnull", distinct=True, exclude=True
    )
    taxonomy_rank = filters.ChoiceFilter(
        choices=Taxonomy.RANK_CHOICES, field_name='taxonomy__rank'
    )
    name = filters.CharFilter(
        field_name='name', lookup_expr='icontains'
    )
    length__gt = filters.NumberFilter(
        field_name='length', lookup_expr='gt'
    )
    length__lt = filters.NumberFilter(
        field_name='length', lookup_expr='lt'
    )
    tax_id = filters.CharFilter(
        method='filter_annotated_tax'
    )
    function = filters.ModelMultipleChoiceFilter(
        queryset=Function.objects.all(),
        field_name='functions__function_id',
        to_field_name='function_id'
    )

    def filter_annotated_tax(self, queryset, name, value):
        tax_rank = Taxonomy.objects.get(tax_id=value).rank
        lookup = f"taxonomy__hierarchy__{tax_rank}__tax_id"
        return queryset.filter(**{lookup: value})

    class Meta:
        model = Gene
        fields = ['length', 'name', 'source']
