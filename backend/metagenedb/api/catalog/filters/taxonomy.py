from django_filters import rest_framework as filters
from metagenedb.apps.catalog.models import Taxonomy


class TaxonomyFilter(filters.FilterSet):

    class Meta:
        model = Taxonomy
        fields = ['rank', 'name']
