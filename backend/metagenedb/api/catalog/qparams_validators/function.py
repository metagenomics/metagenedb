from marshmallow import fields
from marshmallow.validate import OneOf

from metagenedb.apps.catalog import models
from metagenedb.common.django_default.qparams_validators import PaginatedQueryParams

SELECTED_SOURCE = [i[0] for i in models.Function.SOURCE_CHOICES]
EGGNOG_VERSIONS = [i[0] for i in models.EggNOG.VERSION_CHOICES]


class FunctionQueryParams(PaginatedQueryParams):
    source = fields.String(validate=OneOf(choices=SELECTED_SOURCE))


class KeggQueryParams(PaginatedQueryParams):
    detailed = fields.Boolean()


class EggNOGQueryParams(PaginatedQueryParams):
    version = fields.String(validate=OneOf(choices=EGGNOG_VERSIONS))
