from marshmallow import fields

from metagenedb.common.django_default.qparams_validators import PaginatedQueryParams


class TaxonomyQueryParams(PaginatedQueryParams):
    rank = fields.String()
    name = fields.String()
