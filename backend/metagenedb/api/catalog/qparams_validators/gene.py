from marshmallow import fields

from metagenedb.common.django_default.qparams_validators import PaginatedQueryParams


class GeneQueryParams(PaginatedQueryParams):
    has_taxonomy = fields.Boolean()
    has_functions = fields.Boolean()
    length = fields.Integer()
    length__gt = fields.Integer()
    length__lt = fields.Integer()
    name = fields.String()
    taxonomy_rank = fields.String()
    tax_id = fields.Integer()
    function = fields.String()
    source = fields.String()
    fasta = fields.Boolean()
    csv = fields.Boolean()
