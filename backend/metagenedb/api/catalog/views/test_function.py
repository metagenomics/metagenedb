from rest_framework.test import APITestCase

import mock

from metagenedb.apps.catalog.factory import EggNOGFactory, KeggOrthologyFactory
from metagenedb.common.utils.mocks.metagenedb import (
    MetageneDBCatalogFunctionAPIMock, MetageneDBCatalogKeggOrthologyAPIMock
)


class TestKeggOrthologyViewSet(APITestCase):

    def setUp(self):
        self.function_api = MetageneDBCatalogFunctionAPIMock(self.client)
        self.kegg_ortho_api = MetageneDBCatalogKeggOrthologyAPIMock(self.client)
        self.kegg_function = KeggOrthologyFactory.create()
        self.eggnog_function = EggNOGFactory.create()

    def test_retrieve(self):
        for function in [self.kegg_function, self.eggnog_function]:
            expected_function = {
                'function_id': function.function_id,
                'name': function.name,
                'source': function.source
            }
            self.assertDictEqual(self.function_api.get(function.function_id), expected_function)

    def test_retrieve_detailed_available(self):
        query_params = {
            'detailed': 'true'
        }
        class_to_mock = 'metagenedb.api.catalog.views.function.GetFunctionExternalInfo'
        detailed_kegg = {
            'function_id': self.kegg_function.function_id,
            'name': self.kegg_function.name,
            'details': 'some details'
        }
        with mock.patch(class_to_mock) as MockGetFunctionExternalInfo:
            MockGetFunctionExternalInfo.return_value.get_details.return_value = detailed_kegg
            tested_dict = self.kegg_ortho_api.get(self.kegg_function.function_id, params=query_params)
        self.assertDictEqual(tested_dict, detailed_kegg)
