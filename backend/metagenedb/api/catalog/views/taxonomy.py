from drf_yasg.utils import swagger_auto_schema

from metagenedb.api.catalog.filters import TaxonomyFilter
from metagenedb.api.catalog.qparams_validators.taxonomy import TaxonomyQueryParams
from metagenedb.apps.catalog.models import Taxonomy
from metagenedb.apps.catalog.serializers import TaxonomySerializer

from .base import BulkViewSet


class TaxonomyViewSet(BulkViewSet):
    queryset = Taxonomy.objects.select_related(
        "parent").all()
    serializer_class = TaxonomySerializer
    lookup_field = 'tax_id'
    filterset_class = TaxonomyFilter
    query_params_parser = TaxonomyQueryParams

    @swagger_auto_schema(
        tags=['Taxonomy'],
    )
    def list(self, *args, **kwargs):
        return super().list(*args, **kwargs)

    @swagger_auto_schema(
        tags=['Taxonomy'],
    )
    def retrieve(self, *args, **kwargs):
        return super().retrieve(*args, **kwargs)

    @swagger_auto_schema(
        tags=['Taxonomy'],
    )
    def create(self, *args, **kwargs):
        return super().create(*args, **kwargs)

    @swagger_auto_schema(
        tags=['Taxonomy'],
    )
    def update(self, *args, **kwargs):
        return super().update(*args, **kwargs)

    @swagger_auto_schema(
        tags=['Taxonomy'],
    )
    def partial_update(self, *args, **kwargs):
        return super().partial_update(*args, **kwargs)
