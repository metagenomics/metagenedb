from io import StringIO

from django.conf import settings
from django.http import HttpResponse
from drf_yasg.utils import swagger_auto_schema
from marshmallow.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_422_UNPROCESSABLE_ENTITY, HTTP_500_INTERNAL_SERVER_ERROR
)
from metagenedb.apps.catalog.models import Gene
from metagenedb.api.catalog.filters import GeneFilter
from metagenedb.api.catalog.qparams_validators.gene import GeneQueryParams
from metagenedb.apps.catalog.serializers import GeneSerializer
from metagenedb.common.utils.cache import queryset_count_cached

from .base import BulkViewSet


MAX_DOWNLOAD_GENES = settings.MAX_DOWNLOAD_GENES


class GeneViewSet(BulkViewSet):
    queryset = Gene.objects.select_related('taxonomy').prefetch_related('functions')
    serializer_class = GeneSerializer
    filterset_class = GeneFilter
    query_params_parser = GeneQueryParams
    lookup_field = 'gene_id'

    def _get_queryset_count(self, queryset):
        return queryset_count_cached(queryset)

    @property
    def too_many_genes_error_response(self):
        error_message = f'Too many genes in the query, can obtain download up to {MAX_DOWNLOAD_GENES} genes.'
        return Response({'message': error_message}, status=HTTP_500_INTERNAL_SERVER_ERROR)

    def _check_too_many_genes(self, queryset):
        count = self._get_queryset_count(queryset)
        return count >= MAX_DOWNLOAD_GENES

    def _build_fasta_response(self):
        queryset = self.filter_queryset(self.get_queryset())
        if self._check_too_many_genes(queryset):
            return self.too_many_genes_error_response
        with StringIO() as fasta_file:
            for gene in queryset.iterator():
                fasta_file.write(gene.fasta)
            # generate the file
            response = HttpResponse(fasta_file.getvalue(), content_type='text/fasta')
            filename = 'metagenedb_sequences.fasta'
            response['Content-Disposition'] = 'attachment; filename=%s' % filename
            return response

    def _build_csv_response(self):
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.select_related("taxonomy").prefetch_related("functions")
        if self._check_too_many_genes(queryset):
            return self.too_many_genes_error_response
        with StringIO() as csv_file:
            # Write header
            header = Gene.CSV_HEADER
            csv_file.write(f"{header}\n")
            for gene in queryset.iterator():
                csv_file.write(f"{gene.csv}\n")
            # generate the file
            response = HttpResponse(csv_file.getvalue(), content_type='text/csv')
            filename = 'metagenedb.csv'
            response['Content-Disposition'] = 'attachment; filename=%s' % filename
            return response

    @swagger_auto_schema(
        tags=['Genes'],
    )
    def list(self, request, *args, **kwargs):
        try:
            query_params = self.query_params_parser().load(request.query_params)
        except ValidationError as validation_error:
            error_message = validation_error.normalized_messages()
            error_message.update({
                'allowed_query_params': ', '.join(self.query_params_parser().declared_fields.keys())
            })
            return Response(error_message, status=HTTP_422_UNPROCESSABLE_ENTITY)
        if query_params.get('fasta', False) is True:
            return self._build_fasta_response()
        if query_params.get('csv', False) is True:
            return self._build_csv_response()
        return super().list(request, *args, **kwargs)

    @swagger_auto_schema(
        tags=['Genes'],
    )
    def retrieve(self, *args, **kwargs):
        return super().retrieve(*args, **kwargs)

    @swagger_auto_schema(
        tags=['Genes'],
    )
    def create(self, *args, **kwargs):
        return super().create(*args, **kwargs)

    @swagger_auto_schema(
        tags=['Genes'],
    )
    def update(self, *args, **kwargs):
        return super().update(*args, **kwargs)

    @swagger_auto_schema(
        tags=['Genes'],
    )
    def partial_update(self, *args, **kwargs):
        return super().partial_update(*args, **kwargs)
