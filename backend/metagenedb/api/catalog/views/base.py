from marshmallow.exceptions import ValidationError
from rest_framework import status
from rest_framework.mixins import (
    CreateModelMixin, ListModelMixin, RetrieveModelMixin, UpdateModelMixin,
)
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from metagenedb.common.django_default.permissions import ListAndRetrieveAll
from metagenedb.common.django_default.qparams_validators import PaginatedQueryParams


class QParamsValidationViewSet(
    CreateModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
    ListModelMixin,
    GenericViewSet
):
    query_params_parser = PaginatedQueryParams

    def _get_qparams(self, raw_query_params):
        return self.query_params_parser().load(raw_query_params)

    def list(self, request, *args, **kwargs):
        try:
            query_params = self._get_qparams(request.query_params)  # noqa
        except ValidationError as validation_error:
            error_message = validation_error.normalized_messages()
            error_message.update({
                'allowed_query_params': ', '.join(self.query_params_parser().declared_fields.keys())
            })
            return Response(error_message, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        try:
            query_params = self._get_qparams(request.query_params)  # noqa
        except ValidationError as validation_error:
            error_message = validation_error.normalized_messages()
            error_message.update({
                'allowed_query_params': ', '.join(self.query_params_parser().declared_fields.keys())
            })
            return Response(error_message, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class BulkViewSet(QParamsValidationViewSet):
    permission_classes = [ListAndRetrieveAll]

    def get_objects(self, instance_ids):
        return self.queryset.in_bulk(instance_ids, field_name=self.lookup_field)

    def _created_payload(self, serializer, request):
        if isinstance(request.data, list):
            return {
                'path': request.path_info,
                'created': {
                    'count': len(serializer.data)
                }
            }
        return serializer.data

    def _updated_payload(self, create_serializer, update_serializer, request):
        return {
            'path': request.path_info,
            'created': {
                'count': len(create_serializer.data) if create_serializer else 0
            },
            'updated': {
                'count': len(update_serializer.data) if update_serializer else 0
            }
        }

    def _get_create_serializer(self, data):
        if isinstance(data, list):
            serializer = self.get_serializer(data=data, many=True)
        else:
            serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return serializer

    def _get_update_serializer(self, instances, data):
        serializer = self.get_serializer(instances, data=data, many=True)
        serializer.is_valid(raise_exception=True)
        if data:
            self.perform_update(serializer)
        return serializer

    def create(self, request, *args, **kwargs):
        serializer = self._get_create_serializer(request.data)
        headers = self.get_success_headers(serializer.data)
        return Response(
            self._created_payload(serializer, request),
            status=status.HTTP_201_CREATED, headers=headers
        )

    def update(self, request, *args, **kwargs):
        if self.lookup_field in kwargs.keys():
            # perform the classic update
            return super().update(request, *args, **kwargs)
        instance_ids = set([element[self.lookup_field] for element in request.data])
        instances = self.get_objects(instance_ids)
        data_to_update = []
        data_to_create = []
        for item in request.data:
            if item.get(self.lookup_field) in instances.keys():
                data_to_update.append(item)
            else:
                data_to_create.append(item)
        update_serializer = self._get_update_serializer(instances, data_to_update)
        create_serializer = self._get_create_serializer(data_to_create)

        headers = self.get_success_headers(update_serializer.data)
        return Response(
            self._updated_payload(create_serializer, update_serializer, request),
            status=status.HTTP_201_CREATED, headers=headers
        )
