from requests.exceptions import HTTPError
from rest_framework.test import APITestCase

from metagenedb.apps.catalog.factory import FunctionFactory
from metagenedb.common.utils.mocks.metagenedb import MetageneDBCatalogFunctionAPIMock
from metagenedb.common.utils.tests.apitestbase import AdminUserBasedTest


class TestOperationsBulkViewSetNoCredentials(APITestCase):

    def setUp(self):
        self.function_api = MetageneDBCatalogFunctionAPIMock(self.client)

    def test_create_function(self):
        data = {
            "function_id": 'k_test1',
            "source": "kegg",
            "name": "Kegg Test 1"
        }
        with self.assertRaises(HTTPError):
            self.function_api.post(data)

    def test_update_existing_function(self):
        function = FunctionFactory()
        data = {
            "function_id": function.function_id,
            "source": function.source,
            "name": "Kegg Test 1"
        }
        with self.assertRaises(HTTPError):
            self.function_api.put(data, function.function_id)


class TestOperationsBulkViewSet(AdminUserBasedTest):
    """
    We are testing the different functions through the API directly through the mock redirecting
    requests to the test database.

    The extent is a bit more than a unittest since it is not just involving concerned methods.
    """

    def setUp(self):
        super().setUp()
        self.function_api = MetageneDBCatalogFunctionAPIMock(self.client, jwt_token=self.jwt_token)

    def test_create_function(self):
        data = {
            "function_id": 'k_test1',
            "source": "kegg",
            "name": "Kegg Test 1"
        }
        response = self.function_api.post(data)
        self.assertDictEqual(response, data)
        self.assertEqual(self.function_api.get_all()['count'], 1)
        self.assertDictEqual(self.function_api.get(data['function_id']), data)

    def test_create_functions(self):
        data = [
            {
                "function_id": "k_test1",
                "source": "kegg",
                "name": "Kegg Test 1"
            },
            {
                "function_id": "k_test2",
                "source": "kegg",
                "name": "Kegg Test 2"
            }
        ]
        expected_response = {
            'path': '/api/catalog/v1/functions/',
            'created': {
                'count': len(data)
            }
        }
        response = self.function_api.post(data)
        self.assertDictEqual(response, expected_response)
        self.assertEqual(self.function_api.get_all()['count'], 2)
        for element in data:
            self.assertDictEqual(self.function_api.get(element['function_id']), element)

    def test_update_existing_function(self):
        function = FunctionFactory()
        data = {
            "function_id": function.function_id,
            "source": function.source,
            "name": "Kegg Test 1"
        }
        self.function_api.put(data, function.function_id)
        self.assertDictEqual(self.function_api.get(function.function_id), data)

    def test_update_existing_functions(self):
        functions = FunctionFactory.create_batch(2)
        data = [
            {
                "function_id": functions[0].function_id,
                "source": functions[0].source,
                "name": "Test 1"
            },
            {
                "function_id": functions[1].function_id,
                "source": functions[1].source,
                "name": "Test 2"
            }
        ]
        for element in data:
            self.assertNotEqual(self.function_api.get(element['function_id']), element)
        response = self.function_api.put(data)
        self.assertEqual(response.get('created').get('count'), 0)
        self.assertEqual(response.get('updated').get('count'), 2)
        self.assertEqual(self.function_api.get_all()['count'], 2)
        for element in data:
            self.assertDictEqual(self.function_api.get(element['function_id']), element)

    def test_create_through_update_functions(self):
        functions = FunctionFactory.build_batch(2)
        data = [
            {
                "function_id": functions[0].function_id,
                "source": functions[0].source,
                "name": "Test 1"
            },
            {
                "function_id": functions[1].function_id,
                "source": functions[1].source,
                "name": "Test 2"
            }
        ]
        response = self.function_api.put(data)
        self.assertEqual(response.get('created').get('count'), 2)
        self.assertEqual(response.get('updated').get('count'), 0)
        self.assertEqual(self.function_api.get_all()['count'], 2)
        for element in data:
            self.assertDictEqual(self.function_api.get(element['function_id']), element)

    def test_create_and_update_functions(self):
        functions = FunctionFactory.create_batch(2)
        new_functions = FunctionFactory.build_batch(2)
        data = [
            {
                "function_id": functions[0].function_id,
                "source": functions[0].source,
                "name": "Test 1"
            },
            {
                "function_id": functions[1].function_id,
                "source": functions[1].source,
                "name": "Test 2"
            },
            {
                "function_id": new_functions[0].function_id,
                "source": new_functions[0].source,
                "name": "New Test 1"
            },
            {
                "function_id": new_functions[1].function_id,
                "source": new_functions[1].source,
                "name": "New Test 2"
            }
        ]
        response = self.function_api.put(data)
        self.assertEqual(response.get('created').get('count'), 2)
        self.assertEqual(response.get('updated').get('count'), 2)
        self.assertEqual(self.function_api.get_all()['count'], 4)
        for element in data:
            self.assertDictEqual(self.function_api.get(element['function_id']), element)

    def test_get_item(self):
        function = FunctionFactory.create(name="Test")
        response = self.function_api.get(function.function_id)
        self.assertEqual(response['name'], 'Test')
        # Use wrong query params, expect 422 returned
        fake_qparams = {'qparam': 'fake'}
        with self.assertRaises(HTTPError):
            response = self.function_api.get(function.function_id, params=fake_qparams)

    def test_get_items(self):
        FunctionFactory.create_batch(5)
        response = self.function_api.get_all()
        self.assertEqual(response['count'], 5)
        # Use wrong query params, expect 422 returned
        fake_qparams = {'qparam': 'fake'}
        with self.assertRaises(HTTPError):
            response = self.function_api.get_all(params=fake_qparams)
