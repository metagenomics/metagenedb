import logging

from celery.decorators import task
from rest_framework.decorators import api_view
from rest_framework.response import Response

logger = logging.getLogger(__name__)


@task(name="test_celery_task")
def test_task(msg):
    logger.info("Test Celery Task")
    return msg


@api_view()
def test_task_view(request):
    msg = test_task.delay("I am alive")
    return Response({"message": f"Tested celery task: {msg.get(timeout=60)}"})
