import logging

from drf_yasg.utils import swagger_auto_schema
# from drf_yasg.openapi import Response
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from metagenedb.apps.catalog.management.commands.compute_stats import (
    compute_gene_counts, compute_gene_length, compute_taxonomy_presence, compute_taxonomy_repartition
)

logger = logging.getLogger(__name__)
SOURCE_CHOICES = ['all', 'virgo', 'igc']


class AdminCommandsAPI(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [permissions.IsAdminUser]


class ComputeStatisticsAPI(AdminCommandsAPI):
    compute_function = None
    error_message = "Could not compute stats due to server error"
    success_message = "Stats successfully computing on worker...."

    def get(self, request, format=None):
        try:
            self.compute_function.delay(SOURCE_CHOICES)
        except Exception as exception:
            logger.warning(exception)
            return Response({"message": self.error_message},
                            status=HTTP_500_INTERNAL_SERVER_ERROR)
        return Response({"message": self.success_message})


class ComputeCountsAPI(ComputeStatisticsAPI):
    compute_function = compute_gene_counts
    success_message = "Gene counts successfully computing on worker...."

    @swagger_auto_schema(
        operation_description="Compute counts for the different annotations of the genes",
        operation_summary="API to compute counts of annotations.",
        tags=['admin-commands'],
    )
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)


class ComputeGeneLengthAPI(ComputeStatisticsAPI):
    compute_function = compute_gene_length
    success_message = "Gene length successfully computing on worker...."

    @swagger_auto_schema(
        operation_description="Compute gene length distribution",
        operation_summary="API to compute gene length distribution.",
        tags=['admin-commands'],
    )
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)


class ComputeTaxonomyRepartitionAPI(ComputeStatisticsAPI):
    compute_function = compute_taxonomy_repartition
    success_message = "Taxonomy repartition successfully computing on worker...."

    @swagger_auto_schema(
        operation_description="Compute repartition of taxonomy among genes",
        operation_summary="API to compute repartition of taxonomy.",
        tags=['admin-commands'],
    )
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)


class ComputeTaxonomyPresenceAPI(ComputeStatisticsAPI):
    compute_function = compute_taxonomy_presence
    success_message = "Taxonomy presence successfully computing on worker...."

    @swagger_auto_schema(
        operation_description="Compute presence of taxonomy among genes",
        operation_summary="API to compute presence of taxonomy.",
        tags=['admin-commands'],
    )
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)
