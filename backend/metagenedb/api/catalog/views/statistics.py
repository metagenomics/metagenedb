from drf_yasg.utils import swagger_auto_schema

from metagenedb.apps.catalog.models import Statistics
from metagenedb.apps.catalog.serializers import StatisticsSerializer

from .base import BulkViewSet


class StatisticsViewSet(BulkViewSet):
    queryset = Statistics.objects.all()
    serializer_class = StatisticsSerializer
    lookup_field = 'stats_id'

    @swagger_auto_schema(
        operation_description="List all pre-computed statistics",
        operation_summary="API to list all pre-computed statistics.",
        tags=['Statistics'],
    )
    def list(self, *args, **kwargs):
        return super().list(*args, **kwargs)

    @swagger_auto_schema(
        operation_description="Retrieve pre-computed statistics from an ID",
        operation_summary="API to retrieve pre-computed statistics.",
        tags=['Statistics'],
    )
    def retrieve(self, *args, **kwargs):
        return super().retrieve(*args, **kwargs)

    @swagger_auto_schema(
        tags=['Statistics'],
    )
    def create(self, *args, **kwargs):
        return super().create(*args, **kwargs)

    @swagger_auto_schema(
        tags=['Statistics'],
    )
    def update(self, *args, **kwargs):
        return super().update(*args, **kwargs)

    @swagger_auto_schema(
        tags=['Statistics'],
    )
    def partial_update(self, *args, **kwargs):
        return super().partial_update(*args, **kwargs)
