import logging

from drf_yasg.utils import swagger_auto_schema
from marshmallow.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.status import HTTP_422_UNPROCESSABLE_ENTITY

from metagenedb.api.catalog.filters import FunctionFilter, EggNOGFilter
from metagenedb.api.catalog.qparams_validators.function import (
    EggNOGQueryParams, FunctionQueryParams, KeggQueryParams
)
from metagenedb.apps.catalog.models import EggNOG, Function, KeggOrthology
from metagenedb.apps.catalog.serializers import EggNOGSerializer, FunctionSerializer, KeggOrthologySerializer
from metagenedb.common.utils.external_api.kegg_api import GetFunctionExternalInfo

from .base import BulkViewSet


logger = logging.getLogger(__name__)


class FunctionViewSet(BulkViewSet):
    queryset = Function.objects.all()
    serializer_class = FunctionSerializer
    lookup_field = 'function_id'
    filterset_class = FunctionFilter
    query_params_parser = FunctionQueryParams

    @swagger_auto_schema(
        tags=['Functions'],
    )
    def list(self, *args, **kwargs):
        return super().list(*args, **kwargs)

    @swagger_auto_schema(
        tags=['Functions'],
    )
    def retrieve(self, *args, **kwargs):
        return super().retrieve(*args, **kwargs)

    @swagger_auto_schema(
        tags=['Functions'],
    )
    def create(self, *args, **kwargs):
        return super().create(*args, **kwargs)

    @swagger_auto_schema(
        tags=['Functions'],
    )
    def update(self, *args, **kwargs):
        return super().update(*args, **kwargs)

    @swagger_auto_schema(
        tags=['Functions'],
    )
    def partial_update(self, *args, **kwargs):
        return super().partial_update(*args, **kwargs)


class KeggOrthologyViewSet(BulkViewSet):
    queryset = KeggOrthology.objects.all()
    serializer_class = KeggOrthologySerializer
    lookup_field = 'function_id'
    query_params_parser = KeggQueryParams

    def _get_external_info(self, db_data):
        detailed_info_retriever = GetFunctionExternalInfo(db_data['function_id'], 'kegg')
        try:
            detailed_data = detailed_info_retriever.get_details()
        except NotImplementedError as not_implemented_error:
            logger.warning("Could not found API for the source, returning item from the DB. Error: %s" %
                           not_implemented_error)
            detailed_data = db_data
        return detailed_data

    @swagger_auto_schema(
        tags=['KEGG'],
    )
    def retrieve(self, request, *args, **kwargs):
        try:
            query_params = self.query_params_parser().load(request.query_params)
        except ValidationError as validation_error:
            error_message = validation_error.normalized_messages()
            error_message.update({
                'allowed_query_params': ', '.join(self.query_params_parser().declared_fields.keys())
            })
            return Response(error_message, status=HTTP_422_UNPROCESSABLE_ENTITY)
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        returned_data = serializer.data
        if query_params.get('detailed', False) is True:
            returned_data = self._get_external_info(returned_data)
        return Response(returned_data)

    @swagger_auto_schema(
        tags=['KEGG'],
    )
    def list(self, *args, **kwargs):
        return super().list(*args, **kwargs)

    @swagger_auto_schema(
        tags=['KEGG'],
    )
    def create(self, *args, **kwargs):
        return super().create(*args, **kwargs)

    @swagger_auto_schema(
        tags=['KEGG'],
    )
    def update(self, *args, **kwargs):
        return super().update(*args, **kwargs)

    @swagger_auto_schema(
        tags=['KEGG'],
    )
    def partial_update(self, *args, **kwargs):
        return super().partial_update(*args, **kwargs)


class EggNOGViewSet(BulkViewSet):
    queryset = EggNOG.objects.all()
    serializer_class = EggNOGSerializer
    lookup_field = 'function_id'
    filterset_class = EggNOGFilter
    query_params_parser = EggNOGQueryParams

    @swagger_auto_schema(
        tags=['EggNOG'],
    )
    def list(self, *args, **kwargs):
        return super().list(*args, **kwargs)

    @swagger_auto_schema(
        tags=['EggNOG'],
    )
    def retrieve(self, *args, **kwargs):
        return super().retrieve(*args, **kwargs)

    @swagger_auto_schema(
        tags=['EggNOG'],
    )
    def create(self, *args, **kwargs):
        return super().create(*args, **kwargs)

    @swagger_auto_schema(
        tags=['EggNOG'],
    )
    def update(self, *args, **kwargs):
        return super().update(*args, **kwargs)

    @swagger_auto_schema(
        tags=['EggNOG'],
    )
    def partial_update(self, *args, **kwargs):
        return super().partial_update(*args, **kwargs)
