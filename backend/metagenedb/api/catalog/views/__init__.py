from .function import EggNOGViewSet, KeggOrthologyViewSet, FunctionViewSet  # noqa
from .gene import GeneViewSet  # noqa
from .statistics import StatisticsViewSet  # noqa
from .taxonomy import TaxonomyViewSet  # noqa
