from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from metagenedb.api.catalog.views import GeneViewSet


class GeneViewSetMock(GeneViewSet):
    """
    Make mock in case complex instantiation occurs since tests here are independant from the class itself
    """

    def __init__(self):
        pass


class TestGenes(TestCase):

    def test_get_genes_no_auth(self):
        """
        Unauthenticated users should be able to access genes
        """
        url = reverse('api:catalog:v1:genes-list')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
