from django.urls import path

from metagenedb.api.catalog.views import admin_commands


urlpatterns = [
    path('compute-counts/', admin_commands.ComputeCountsAPI.as_view()),
    path('compute-gene-length/', admin_commands.ComputeGeneLengthAPI.as_view()),
    path('compute-taxo-repartition/', admin_commands.ComputeTaxonomyRepartitionAPI.as_view()),
    path('compute-taxo-presence/', admin_commands.ComputeTaxonomyPresenceAPI.as_view()),
]
