from django.urls import path
from django.conf.urls import include
from django.urls import re_path
from rest_framework.routers import DefaultRouter, DynamicRoute, Route

from metagenedb.api.catalog import views

from metagenedb.api.catalog.views.celery_test import test_task_view


class CustomRouter(DefaultRouter):

    routes = [
        # List route.
        Route(
            url=r'^{prefix}{trailing_slash}$',
            mapping={
                'get': 'list',
                'put': 'update',
                'post': 'create'
            },
            name='{basename}-list',
            detail=False,
            initkwargs={'suffix': 'List'}
        ),
        # Dynamically generated list routes. Generated using
        # @action(detail=False) decorator on methods of the viewset.
        DynamicRoute(
            url=r'^{prefix}/{url_path}{trailing_slash}$',
            name='{basename}-{url_name}',
            detail=False,
            initkwargs={}
        ),
        # Detail route.
        Route(
            url=r'^{prefix}/{lookup}{trailing_slash}$',
            mapping={
                'get': 'retrieve',
                'put': 'update',
                'patch': 'partial_update',
                'delete': 'destroy'
            },
            name='{basename}-detail',
            detail=True,
            initkwargs={'suffix': 'Instance'}
        ),
        # Dynamically generated detail routes. Generated using
        # @action(detail=True) decorator on methods of the viewset.
        DynamicRoute(
            url=r'^{prefix}/{lookup}/{url_path}{trailing_slash}$',
            name='{basename}-{url_name}',
            detail=True,
            initkwargs={}
        ),
    ]


api_router = CustomRouter()
api_router.register(r'functions', views.FunctionViewSet, basename='functions')
api_router.register(r'kegg-orthologies', views.KeggOrthologyViewSet, basename='kegg-orthologies')
api_router.register(r'eggnogs', views.EggNOGViewSet, basename='eggnogs')
api_router.register(r'genes', views.GeneViewSet, basename='genes')
api_router.register(r'taxonomy', views.TaxonomyViewSet, basename='taxonomy')
api_router.register(r'statistics', views.StatisticsViewSet, basename='statistics')


urlpatterns = [
    re_path(r'v1/', include((api_router.urls, 'v1'))),
    path('admin/', include(('metagenedb.api.catalog.admin_urls', 'admin'))),
    path('celery-task-test/', test_task_view, name='celery-task-test'),
]
