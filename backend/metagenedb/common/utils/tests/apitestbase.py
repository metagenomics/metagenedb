from django.contrib.auth.models import User
from rest_framework.test import APITestCase
from rest_framework_jwt.settings import api_settings


class AdminUserBasedTest(APITestCase):
    """
    We are testing the different functions through the API directly through the mock redirecting
    requests to the test database.

    The extent is a bit more than a unittest since it is not just involving concerned methods.
    """

    def setUp(self):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        user = User.objects.create_user(username='user_admin', email='user@admin.com', password='pass')
        user.is_active = True
        user.is_staff = True
        user.save()
        payload = jwt_payload_handler(user)
        self.jwt_token = jwt_encode_handler(payload)
