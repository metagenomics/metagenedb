from string import ascii_lowercase


def generate_color_code(string):
    """
    Generate a color code from a string.
    """
    value_mapping = {ascii_lowercase[i]: i for i in range(0, len(ascii_lowercase))}
    value_mapping.update({' ': 0})
    color = list("#968579")
    indices = [1, 3, 5, 2, 4, 6]
    iteration = 0
    for letter in string[:6]:
        letter_value = value_mapping[letter.lower()]
        color[indices[iteration]] = str(hex(letter_value + int(color[indices[iteration]])))[-1]
        iteration += 1
    return "".join(color)
