from itertools import islice


def list_chunks(full_list, chunk_size):
    """Yield successive n-sized chunks from full_list."""
    for i in range(0, len(full_list), chunk_size):
        yield full_list[i:i + chunk_size]


def dict_chunks(full_dict, chunk_size=10000):
    """Yield successiv n-sized chunks from full-dict."""
    it = iter(full_dict)
    for i in range(0, len(full_dict), chunk_size):
        yield {k: full_dict[k] for k in islice(it, chunk_size)}


def file_len(file_path):
    with open(file_path) as f:
        for i, l in enumerate(f):
            pass
    return i + 1
