from requests.exceptions import HTTPError

from dabeplech import MetageneDBCatalogGeneAPI
from django.urls import reverse


class MetageneDBAPIMock(MetageneDBCatalogGeneAPI):
    """
    Just a simple mock to go through the Test client. The idea is to test the upsert behaviour and not
    the insertion to the db.
    """
    KEY_ID = ''
    BASE_REVERSE = 'api'
    REVERSE_PATH = ''
    BAD_REQUESTS = range(400, 452)

    def __init__(self, client, jwt_token=None):
        self.client = client
        self.reverse_path = ':'.join([self.BASE_REVERSE, self.REVERSE_PATH])
        self.headers = {}
        if jwt_token is not None:
            self.headers.update({
                'HTTP_AUTHORIZATION': f"JWT {jwt_token}",
            })

    def get_all(self, params=None):
        response = self.client.get(reverse(f'{self.reverse_path}-list'), params, **self.headers)
        if response.status_code in self.BAD_REQUESTS:
            raise HTTPError
        return response.json()

    def get(self, entry_id, params=None):
        response = self.client.get(reverse(f'{self.reverse_path}-detail', kwargs={self.KEY_ID: entry_id}),
                                   params, **self.headers)
        if response.status_code in self.BAD_REQUESTS:
            raise HTTPError
        return response.json()

    def post(self, data):
        response = self.client.post(reverse(f'{self.reverse_path}-list'), data, format='json', **self.headers)
        if response.status_code in self.BAD_REQUESTS:
            raise HTTPError
        return response.json()

    def put(self, data, entry_id=None):
        if entry_id:
            response = self.client.put(reverse(f'{self.reverse_path}-detail', kwargs={self.KEY_ID: entry_id}),
                                       data, format='json', **self.headers)
        else:
            response = self.client.put(reverse(f'{self.reverse_path}-list'), data, format='json', **self.headers)
        if response.status_code in self.BAD_REQUESTS:
            raise HTTPError
        return response.json()


class MetageneDBCatalogGeneAPIMock(MetageneDBAPIMock):
    KEY_ID = 'gene_id'
    REVERSE_PATH = 'catalog:v1:genes'

    def get_gene_length(self, params=None):
        reverse_path = f"{self.reverse_path}-gene-length"
        response = self.client.get(reverse(reverse_path), params)
        if response.status_code in self.BAD_REQUESTS:
            raise HTTPError
        if response.status_code == 204:  # no content
            return {}
        return response.json()

    def get_tax_counts(self, params=None):
        reverse_path = f"{self.reverse_path}-taxonomy-counts"
        response = self.client.get(reverse(reverse_path), params)
        if response.status_code in self.BAD_REQUESTS:
            raise HTTPError
        if response.status_code == 204:  # no content
            return {}
        return response.json()


class MetageneDBCatalogTaxonomyAPIMock(MetageneDBAPIMock):
    KEY_ID = 'gene_id'
    REVERSE_PATH = 'catalog:v1:taxonomy'


class MetageneDBCatalogFunctionAPIMock(MetageneDBAPIMock):
    KEY_ID = 'function_id'
    REVERSE_PATH = 'catalog:v1:functions'


class MetageneDBCatalogKeggOrthologyAPIMock(MetageneDBCatalogFunctionAPIMock):
    REVERSE_PATH = 'catalog:v1:kegg-orthologies'


class MetageneDBCatalogEggNOGAPIMock(MetageneDBCatalogFunctionAPIMock):
    REVERSE_PATH = 'catalog:v1:eggnogs'
