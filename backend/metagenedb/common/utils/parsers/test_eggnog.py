from unittest import TestCase

from metagenedb.common.utils.parsers.eggnog import (
    EggNOGAnnotationLineParser,
    EggNOGFunctionalCategoriesParser
)


class TestEggNOGAnnotationLineParser(TestCase):

    def test_get_dict(self):
        ko_line = "1\t28H54\tK\ttranslational termination\n"
        expected_dict = {
                'function_id': "28H54",
                'name': "translational termination",
                'functional_categories': ["K"]
            }
        test_dict = EggNOGAnnotationLineParser.get_dict(ko_line)
        self.assertDictEqual(test_dict, expected_dict)

    def test_get_dict_no_name(self):
        ko_line = "1\t28H50\tS\t\n"
        expected_dict = {
                'function_id': "28H50",
                'name': "",
                'functional_categories': ["S"]
            }
        test_dict = EggNOGAnnotationLineParser.get_dict(ko_line)
        self.assertDictEqual(test_dict, expected_dict)

    def test_get_dict_long_name(self):
        ko_line = "1\t28H50\tS\tGlucose-responsive transcription factor that regulates expression of several glucose transporter (HXT) genes in response to glucose. In the absence of glucose, it functions as a transcriptional repressor, whereas high concentrations of glucose cause it to function as a transcriptional activator. In cells growing on low levels of glucose, has a neutral role, neither repressing nor activating transcription (By similarity)\n"  # noqa
        expected_dict = {
                'function_id': "28H50",
                'name': "Glucose-responsive transcription factor that regulates expression of several glucose transporter (HXT) genes in response to glucose",  # noqa
                'functional_categories': ["S"]
            }
        test_dict = EggNOGAnnotationLineParser.get_dict(ko_line)
        self.assertDictEqual(test_dict, expected_dict)

    def test_get_dict_long_name_other_split_char(self):
        ko_line = "1\t28H50\tS\t6,7-Dimethyl-8-Ribityllumazine synthase; RibE; 6,7-diimethyl-8-ribityllumazine synthase; DMRL synthase; lumazine synthase; beta subunit of riboflavin synthase; condenses 5-amino-6-(1'-D)-ribityl-amino-2,4-pyrimidinedione with L-3,4-dihydrohy-2-butanone-4-phosphate to generate 6,6-dimethyl-8-lumazine ; riboflavin synthase then uses 2 molecules of DMRL to produce riboflavin ; involved in the last steps of riboflavin biosynthesis; forms a 60mer in both Bacillus subtilis and Escherichia coli; in Bacillus subtilis this 60mer is associated with the riboflavin synthase subunit while in Escherichia coli it is not\n"  # noqa
        expected_dict = {
                'function_id': "28H50",
                'name': "6,7-Dimethyl-8-Ribityllumazine synthase",  # noqa
                'functional_categories': ["S"]
            }
        test_dict = EggNOGAnnotationLineParser.get_dict(ko_line)
        self.assertDictEqual(test_dict, expected_dict)

    def test_get_dict_multi_categories(self):
        ko_line = "1\t28H54\tKS\ttranslational termination\n"
        expected_dict = {
                'function_id': "28H54",
                'name': "translational termination",
                'functional_categories': ["K", "S"]
            }
        test_dict = EggNOGAnnotationLineParser.get_dict(ko_line)
        self.assertDictEqual(test_dict, expected_dict)

    def test_get_dict_wrong_format(self):
        ko_line = "This is a wrong line format, with; information   and tab"
        with self.assertRaises(Exception) as context:  # noqa
            EggNOGAnnotationLineParser.get_dict(ko_line)


class TestEggNOGFunctionalCategoriesParser(TestCase):

    def test_parse_file(self):
        parser = EggNOGFunctionalCategoriesParser("test")
        fake_file_handler = [
           "FIRST GROUP\n", " [A] Categorie name A\n", " [B] Categorie name B\n", "\n",
           "SECOND GROUP\n", " [C] Categorie name C\n", " [D] Categorie name D\n",
        ]
        expected_list = [
            {'category_id': 'A', 'group': 'First group', 'name': 'Categorie name A'},
            {'category_id': 'B', 'group': 'First group', 'name': 'Categorie name B'},
            {'category_id': 'C', 'group': 'Second group', 'name': 'Categorie name C'},
            {'category_id': 'D', 'group': 'Second group', 'name': 'Categorie name D'}
        ]
        self.assertListEqual(parser.handle_parsing(fake_file_handler), expected_list)
