from unittest import TestCase

from metagenedb.common.utils.parsers import KEGGLineParser


class TestKEGGLineParser(TestCase):

    def test_ko_list(self):
        ko_line = "ko:K00809	DHPS, dys; deoxyhypusine synthase [EC:2.5.1.46]"
        expected_dict = {
                'function_id': "K00809",
                'name': "DHPS, dys",
                'long_name': "deoxyhypusine synthase [EC:2.5.1.46]",
                'ec_number': "2.5.1.46"
            }
        test_dict = KEGGLineParser.ko_list(ko_line)
        self.assertDictEqual(test_dict, expected_dict)

    def test_ko_list_wrong_format(self):
        ko_line = "This is a wrong line format, with; information   and tab"
        with self.assertRaises(Exception) as context:  # noqa
            KEGGLineParser.ko_list(ko_line)
