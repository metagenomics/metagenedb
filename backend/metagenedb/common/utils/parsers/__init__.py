from .eggnog import EggNOGAnnotationLineParser  # noqa
from .igc import IGCLineParser  # noqa
from .kegg import KEGGLineParser  # noqa
from .ncbi_taxonomy import NCBITaxonomyLineParser  # noqa
from .virgo import (  # noqa
    VirgoGeneLengthLineParser, VirgoKEGGLineParser, VirgoEggNOGLineParser, VirgoTaxonomyLineParser
)
