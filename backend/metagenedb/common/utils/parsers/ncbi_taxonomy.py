import logging

_LOGGER = logging.getLogger(__name__)


class NCBITaxonomyLineParser(object):

    @staticmethod
    def node(line):
        """
        parse line from ncbi nodes.dmp file

        From documentation:

        nodes.dmp file consists of taxonomy nodes.
        The description for each node includes the following fields:

            tax_id                                  -- node id in GenBank taxonomy database
            parent tax_id                           -- parent node id in GenBank taxonomy database
            rank                                    -- rank of this node (superkingdom, kingdom, ...)
            embl code                               -- locus-name prefix; not unique
            division id                             -- see division.dmp file
            inherited div flag  (1 or 0)            -- 1 if node inherits division from parent
            genetic code id                         -- see gencode.dmp file
            inherited GC  flag  (1 or 0)            -- 1 if node inherits genetic code from parent
            mitochondrial genetic code id           -- see gencode.dmp file
            inherited MGC flag  (1 or 0)            -- 1 if node inherits mitochondrial gencode from parent
            GenBank hidden flag (1 or 0)            -- 1 if name is suppressed in GenBank entry lineage
            hidden subtree root flag (1 or 0)       -- 1 if this subtree has no sequence data yet
            comments                                -- free-text comments and citations
        """
        elements = line.rstrip().split('|')
        elements = [element.strip() for element in elements]
        try:
            return {
                    "tax_id": elements[0],
                    "parent_tax_id": elements[1],
                    "rank": elements[2].replace(' ', '_'),
                    "embl_code": elements[3],
                    "division_id": elements[4],
                    "inherited_div_flag": elements[5],
                    "genetic_code_id": elements[6],
                    "inherited_GC_flag": elements[7],
                    "mitochondrial_genetic_code_id": elements[8],
                    "inherited_MGC_flag": elements[9],
                    "GenBank_hidden_flag": elements[10],
                    "hidden_subtree_root_flag": elements[11],
                    "comments": elements[12]
                }
        except Exception:
            _LOGGER.error(f"Could not parse: {line.rstrip()}. Are you sure it comes from nodes.dmp file?")
            raise

    @staticmethod
    def name(line):
        """
        parse line from ncbi names.dmp file

        From documentation:

        Taxonomy names file (names.dmp):
            tax_id                                  -- the id of node associated with this name
            name_txt                                -- name itself
            unique name                             -- the unique variant of this name if name not unique
            name class                              -- (synonym, common name, ...)
        """
        elements = line.rstrip().split('|')
        try:
            return {
                    "tax_id": elements[0].strip(),
                    "name_txt": elements[1].strip(),
                    "unique_name": elements[2].strip(),
                    "name_class": elements[3].strip(),
                }
        except Exception:
            _LOGGER.error(f"Could not parse: {line.rstrip()}. Are you sure it comes from nodes.dmp file?")
            raise
