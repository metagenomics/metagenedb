import logging

from .base import FileParser

_LOGGER = logging.getLogger(__name__)


class EggNOGAnnotationLineParser:

    @staticmethod
    def get_dict(line):
        """
        Parse line from Eggnog annotations.tsv file to return organized dict
        """
        try:
            elements = line.split('\t')
            return {
                'functional_categories': list(elements[2]),
                'function_id': elements[1],
                'name': elements[3].rstrip().split('.')[0].split(';')[0],
            }
        except Exception:
            _LOGGER.error(f"Could not parse: {line.rstrip()}. Are you sure it comes from eggnog annotations.tsv?")
            raise Exception("Impossible to parse given line as eggnog from annotation.tsv file")


class EggNOGFunctionalCategoriesParser(FileParser):
    """
    Parse functional categories file from EggNOG
    """

    def handle_parsing(self, file_handler):
        functional_categories = []
        current_group = "Unknown"
        for line in file_handler:
            line = line.strip()
            if line.startswith('['):  # It is a category
                elements = line.split(maxsplit=1)
                functional_categories.append({
                    'category_id': elements[0][1],
                    'name': elements[1],
                    'group': current_group.capitalize()
                })
            elif line:  # It is a group of a category
                current_group = line
        return functional_categories
