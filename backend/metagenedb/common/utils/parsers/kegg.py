import logging

_LOGGER = logging.getLogger(__name__)


class KEGGLineParser(object):

    @staticmethod
    def ko_list(line):
        """
        Parse line from kegg KO list (http://rest.kegg.jp/list/ko) to return organized dict
        """
        try:
            elements = line.split('\t')
            function_id = elements[0].split(':')[1]
            if ';' in elements[1]:
                names = elements[1].split(';')
            else:
                _LOGGER.warning(f"Parsing issue with {function_id}, corresponding line: {line}")
                names = [elements[1], '']  # Ugly fix to handle one specific case with no name: K23479
            if '[EC:' in names[1]:
                ec_number = names[1].split('[EC:')[1].rstrip(']')
            else:
                ec_number = ''
            return {
                'function_id': function_id,
                'name': names[0],
                'long_name': names[1].lstrip(),
                'ec_number': ec_number
            }
        except Exception:
            _LOGGER.error(f"Could not parse: {line.rstrip()}. Are you sure it comes from KEGG KO list?")
            raise
