from unittest import TestCase

from metagenedb.common.utils.parsers import NCBITaxonomyLineParser


class TestNCBITaxonomyLineParser(TestCase):

    def test_node(self):
        node_line = "6	|	335928	|	genus	|		|	0	|	1	|	11	|	1	|	0	|	1	|	0	|	0	|		|\n"
        expected_dict = {
            "tax_id": "6",
            "parent_tax_id": "335928",
            "rank": "genus",
            "embl_code": "",
            "division_id": "0",
            "inherited_div_flag": "1",
            "genetic_code_id": "11",
            "inherited_GC_flag": "1",
            "mitochondrial_genetic_code_id": "0",
            "inherited_MGC_flag": "1",
            "GenBank_hidden_flag": "0",
            "hidden_subtree_root_flag": "0",
            "comments": ""
        }
        test_dict = NCBITaxonomyLineParser.node(node_line)
        self.assertDictEqual(test_dict, expected_dict)

    def test_node_wrong_format(self):
        node_line = "This is a wrong line format, with; information   and tab"
        with self.assertRaises(Exception) as context:  # noqa
            NCBITaxonomyLineParser.node(node_line)

    def test_name(self):
        name_line = "2	|	Bacteria	|	Bacteria <prokaryotes>	|	scientific name	|\n"
        expected_dict = {
            "tax_id": "2",
            "name_txt": "Bacteria",
            "unique_name": "Bacteria <prokaryotes>",
            "name_class": "scientific name",
        }
        test_dict = NCBITaxonomyLineParser.name(name_line)
        self.assertDictEqual(test_dict, expected_dict)

    def test_name_wrong_format(self):
        name_line = "This is a wrong line format, with; information   and tab"
        with self.assertRaises(Exception) as context:  # noqa
            NCBITaxonomyLineParser.name(name_line)
