class FileParser:

    def __init__(self, file_path):
        self.file_path = file_path

    def handle_parsing(self, file_handler):
        """
        This method need to be overloaded to really handle the parsing
        """
        for line in file_handler:
            print(line.rstrip())
        return None

    def parse(self):
        with open(self.file_path, 'r') as file:
            return self.handle_parsing(file)
