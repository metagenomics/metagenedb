from unittest import TestCase

from metagenedb.common.utils.parsers import (
    VirgoGeneLengthLineParser, VirgoKEGGLineParser, VirgoEggNOGLineParser, VirgoTaxonomyLineParser
)


class TestVirgoGeneLengthLineParser(TestCase):

    def test_gene(self):
        raw_data = [
            'gene_id',
            'length',
        ]
        raw_line = "\t".join(raw_data)
        expected_dict = {
            'gene_id': raw_data[0],
            'length': raw_data[1],
        }
        test_dict = VirgoGeneLengthLineParser.gene(raw_line)
        self.assertDictEqual(test_dict, expected_dict)

    def test_gene_wrong_format(self):
        raw_line = "This is a wrong line format, with; information   and tab"
        with self.assertRaises(Exception) as context:  # noqa
            VirgoGeneLengthLineParser.gene(raw_line)


class TestVirgoKEGGLineParser(TestCase):

    def test_gene(self):
        raw_data = [
            'gene_id',
            'kegg_ko',
            'kegg_gene',
            'more_information',
        ]
        raw_line = "\t".join(raw_data)
        expected_dict = {
            'gene_id': raw_data[0],
            'kegg_ko': raw_data[1],
            'kegg_gene': raw_data[2],
            'more_info': raw_data[3],
        }
        test_dict = VirgoKEGGLineParser.gene(raw_line)
        self.assertDictEqual(test_dict, expected_dict)

    def test_gene_wrong_format(self):
        raw_line = "This is a wrong line format, with; information   and tab"
        with self.assertRaises(Exception) as context:  # noqa
            VirgoKEGGLineParser.gene(raw_line)


class TestVirgoEggNOGLineParser(TestCase):

    def test_gene(self):
        raw_data = [
            'cluster_id',
            'gene_id',
            'ortholog',
            'kegg_pathway',
            'funcat',
            'name',
            'eggnog'
        ]
        raw_line = "\t".join(raw_data)
        expected_dict = {
            'cluster_id': raw_data[0],
            'gene_id': raw_data[1],
            'ortholog': raw_data[2],
            'kegg_pathway': raw_data[3],
            'eggnog_funcat': raw_data[4],
            'function_name': raw_data[5],
            'eggnog': raw_data[6],
        }
        test_dict = VirgoEggNOGLineParser.gene(raw_line)
        self.assertDictEqual(test_dict, expected_dict)

    def test_missing_kegg_pathway(self):
        raw_data = [
            'cluster_id',
            'gene_id',
            'ortholog',
            '',
            'funcat',
            'name',
            'eggnog'
        ]
        raw_line = "\t".join(raw_data)
        expected_dict = {
            'cluster_id': raw_data[0],
            'gene_id': raw_data[1],
            'ortholog': raw_data[2],
            'kegg_pathway': raw_data[3],
            'eggnog_funcat': raw_data[4],
            'function_name': raw_data[5],
            'eggnog': raw_data[6],
        }
        test_dict = VirgoEggNOGLineParser.gene(raw_line)
        self.assertDictEqual(test_dict, expected_dict)

    def test_gene_wrong_format(self):
        raw_line = "This is a wrong line format, with; information   and tab"
        with self.assertRaises(Exception) as context:  # noqa
            VirgoEggNOGLineParser.gene(raw_line)


class TestVirgoTaxonomyLineParser(TestCase):

    def test_gene(self):
        raw_data = [
            'cluster_id',
            'gene_id',
            'taxonomy',
            '1234',
        ]
        raw_line = "\t".join(raw_data)
        expected_dict = {
            'cluster_id': raw_data[0],
            'gene_id': raw_data[1],
            'taxonomy': raw_data[2],
            'length': raw_data[3],
        }
        test_dict = VirgoTaxonomyLineParser.gene(raw_line)
        self.assertDictEqual(test_dict, expected_dict)

    def test_gene_wrong_format(self):
        raw_line = "This is a wrong line format, with; information   and tab"
        with self.assertRaises(Exception) as context:  # noqa
            VirgoEggNOGLineParser.gene(raw_line)
