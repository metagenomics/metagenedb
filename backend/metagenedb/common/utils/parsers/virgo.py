import logging

_LOGGER = logging.getLogger(__name__)


class VirgoGeneLengthLineParser(object):

    @staticmethod
    def gene(line):
        """
        Parse line from Virgo KEGG annotations to return organized dict (8.A.kegg.ortholog.txt)

        IGC annotation columns:
            0: Gene ID	                            Unique ID
            1: Gene Length	                        Length of nucleotide sequence
        """
        try:
            gene_info = line.rstrip().split('\t')
            return {
                'gene_id': gene_info[0],
                'length': gene_info[1],
            }
        except Exception:
            _LOGGER.error(f"Could not parse: {line.rstrip()}. Are you sure it comes from Virgo KEGG annotation file?")
            raise


class VirgoKEGGLineParser(object):

    @staticmethod
    def gene(line):
        """
        Parse line from Virgo KEGG annotations to return organized dict (8.A.kegg.ortholog.txt)

        IGC annotation columns:
            0: Gene ID	                            Unique ID
            1: KEGG KO Annotation	                Annotated KO(s) for a gene
            2: KEGG Gene
            2: More information	                    Information separated by ;
        """
        try:
            gene_info = line.rstrip().split('\t')
            return {
                'gene_id': gene_info[0],
                'kegg_ko': gene_info[1],
                'kegg_gene': gene_info[2],
                'more_info': gene_info[3],
            }
        except Exception:
            _LOGGER.error(f"Could not parse: {line.rstrip()}. Are you sure it comes from Virgo KEGG annotation file?")
            raise


class VirgoEggNOGLineParser(object):

    @staticmethod
    def gene(line):
        """
        Parse line from Virgo EggNOG annotations to return organized dict (3.eggnog.NOG.txt)

        IGC annotation columns:
            0: Cluster ID
            1: Gene ID
            2: Ortholog
            3: KEGG pathway?
            4: EggNOG Functional category
            5: Name
            6: EggNOG annotation
        """
        try:
            gene_info = line.rstrip().split('\t')
            return {
                'cluster_id': gene_info[0],
                'gene_id': gene_info[1],
                'ortholog': gene_info[2],
                'kegg_pathway': gene_info[3],
                'eggnog_funcat': gene_info[4],
                'function_name': gene_info[5],
                'eggnog': gene_info[6],
            }
        except Exception:
            _LOGGER.error(f"Could not parse: {line.rstrip()}. Are you sure it comes from Virgo EggNOG annotation file?")
            raise


class VirgoTaxonomyLineParser(object):

    @staticmethod
    def gene(line):
        """
        Parse line from Virgo Taxonomy annotations to return organized dict (1.taxon.tbl.txt)

        IGC annotation columns:
            0: Cluster ID
            1: Gene ID
            2: Taxonomy annotation
            3: Gene length
        """
        try:
            gene_info = line.rstrip().split('\t')
            return {
                'cluster_id': gene_info[0],
                'gene_id': gene_info[1],
                'taxonomy': gene_info[2],
                'length': gene_info[3],
            }
        except Exception:
            _LOGGER.error(f"Could not parse: {line.rstrip()}. Are you sure it comes from Virgo taxonomy file?")
            raise
