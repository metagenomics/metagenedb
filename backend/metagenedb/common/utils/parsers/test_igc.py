from unittest import TestCase

from metagenedb.common.utils.parsers import IGCLineParser


class TestIGCLineParser(TestCase):

    def test_gene(self):
        raw_data = [
            'gene_id',
            'name',
            'length',
            'gene_completeness_status',
            'cohort_origin',
            'taxo_phylum',
            'taxo_genus',
            'kegg',
            'eggnog',
            'sample_occurence_freq',
            'ind_occurence_freq',
            'kegg_functional_cat',
            'eggnog_functional_cat',
            'cohort_assembled'
        ]
        raw_line = "\t".join(raw_data)
        expected_dict = {
            'igc_id': raw_data[0],
            'gene_id': raw_data[1],
            'length': raw_data[2],
            'gene_completeness_status': raw_data[3],
            'cohort_origin': raw_data[4],
            'taxo_phylum': raw_data[5],
            'taxo_genus': raw_data[6],
            'kegg_ko': [raw_data[7]],
            'eggnog': [raw_data[8]],
            'sample_occurence_frequency': raw_data[9],
            'individual_occurence_frequency': raw_data[10],
            'kegg_functional_categories': raw_data[11],
            'eggnog_functional_categories': raw_data[12],
            'cohort_assembled': raw_data[13]
        }
        test_dict = IGCLineParser.gene(raw_line)
        self.assertDictEqual(test_dict, expected_dict)

    def test_gene_wrong_format(self):
        raw_line = "This is a wrong line format, with; information   and tab"
        with self.assertRaises(Exception) as context:  # noqa
            IGCLineParser.gene(raw_line)

    def test_multiple_functions(self):
        raw_data = [
            'gene_id',
            'name',
            'length',
            'gene_completeness_status',
            'cohort_origin',
            'taxo_phylum',
            'taxo_genus',
            'kegg;kegg2',
            'eggnog1;eggnog2',
            'sample_occurence_freq',
            'ind_occurence_freq',
            'kegg_functional_cat',
            'eggnog_functional_cat',
            'cohort_assembled'
        ]
        raw_line = "\t".join(raw_data)
        expected_function = {
            'kegg_ko': ['kegg', 'kegg2'],
            'eggnog': ['eggnog1', 'eggnog2'],
        }
        test_dict = IGCLineParser.gene(raw_line)
        self.assertEqual(len(test_dict['kegg_ko']), len(expected_function['kegg_ko']))
        self.assertEqual(len(test_dict['eggnog']), len(expected_function['eggnog']))
        for function in test_dict['kegg_ko']:
            self.assertIn(function, expected_function['kegg_ko'])
        for function in test_dict['eggnog']:
            self.assertIn(function, expected_function['eggnog'])

    def test_multiple_same_functions(self):
        raw_data = [
            'gene_id',
            'name',
            'length',
            'gene_completeness_status',
            'cohort_origin',
            'taxo_phylum',
            'taxo_genus',
            'kegg',
            'eggnog1;eggnog1',
            'sample_occurence_freq',
            'ind_occurence_freq',
            'kegg_functional_cat',
            'eggnog_functional_cat',
            'cohort_assembled'
        ]
        raw_line = "\t".join(raw_data)
        expected_dict = {
            'igc_id': raw_data[0],
            'gene_id': raw_data[1],
            'length': raw_data[2],
            'gene_completeness_status': raw_data[3],
            'cohort_origin': raw_data[4],
            'taxo_phylum': raw_data[5],
            'taxo_genus': raw_data[6],
            'kegg_ko': ['kegg'],
            'eggnog': ['eggnog1'],
            'sample_occurence_frequency': raw_data[9],
            'individual_occurence_frequency': raw_data[10],
            'kegg_functional_categories': raw_data[11],
            'eggnog_functional_categories': raw_data[12],
            'cohort_assembled': raw_data[13]
        }
        test_dict = IGCLineParser.gene(raw_line)
        self.assertDictEqual(test_dict, expected_dict)
