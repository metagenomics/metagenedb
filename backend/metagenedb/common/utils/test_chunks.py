from unittest import TestCase

from metagenedb.common.utils.chunks import list_chunks, dict_chunks, file_len


class TestListChunks(TestCase):

    def setUp(self):
        self.full_list = range(0, 10)

    def test_chunks(self):
        chunk_size = 5
        for i in list_chunks(self.full_list, chunk_size):
            self.assertEqual(len(i), 5)

    def test_chunks_last_truncated(self):
        chunk_size = 4
        chunks = list(list_chunks(self.full_list, chunk_size))
        self.assertEqual(len(chunks), 3)
        self.assertEqual(len(chunks[-1]), 2)

    def test_chunks_no_chunks(self):
        chunk_size = 14
        chunks = list(list_chunks(self.full_list, chunk_size))
        self.assertEqual(len(chunks), 1)
        self.assertEqual(len(chunks[-1]), 10)


class TestDictChunks(TestCase):

    def setUp(self):
        self.full_dict = {f"key_{n}": n for n in range(0, 10)}

    def test_chunks(self):
        chunk_size = 5
        for i in dict_chunks(self.full_dict, chunk_size):
            self.assertEqual(len(i), 5)

    def test_chunks_last_truncated(self):
        chunk_size = 4
        chunks = list(dict_chunks(self.full_dict, chunk_size))
        self.assertEqual(len(chunks), 3)
        self.assertEqual(len(chunks[-1]), 2)

    def test_chunks_no_chunks(self):
        chunk_size = 14
        chunks = list(dict_chunks(self.full_dict, chunk_size))
        self.assertEqual(len(chunks), 1)
        self.assertEqual(len(chunks[-1]), 10)


class TestFileLength(TestCase):

    def test_file_length(self):
        file_path = "./dev_data/IGC_sample.annotation_OF.summary"
        self.assertEqual(file_len(file_path), 1002)
