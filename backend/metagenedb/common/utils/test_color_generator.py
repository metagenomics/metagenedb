from unittest import TestCase

from metagenedb.common.utils.color_generator import generate_color_code


class TestGenerateColorCode(TestCase):

    def test_method(self):
        input_string = "testest"
        expected_color = "#c9c99b"
        self.assertEqual(generate_color_code(input_string), expected_color)

    def test_method_short_string(self):
        input_string = "test"
        expected_color = "#c9c599"
        self.assertEqual(generate_color_code(input_string), expected_color)
