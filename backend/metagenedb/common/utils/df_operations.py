def get_mask(df, range, col_name):
    """
    Get mask for a dataframe for a given range on a column defined by col_name
    Lower is included but not upper:
      e.g (4,8): 4, 5, 6 and 7 are selected
    """
    return (df[col_name] >= range[0]) & (df[col_name] < range[1])
