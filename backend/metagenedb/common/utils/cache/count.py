import hashlib
import inspect

from django.conf import settings
from django.core.cache import cache
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.utils.inspect import method_has_no_args


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


def queryset_count_cached(queryset):
    # Create has from SQL query for REDIS cache
    hash_object = hashlib.md5(str(queryset.query).encode('utf-8'))
    redis_key = hash_object.hexdigest()
    if redis_key in cache:
        return cache.get(redis_key)
    else:
        c = getattr(queryset, 'count', None)
        if callable(c) and not inspect.isbuiltin(c) and method_has_no_args(c):
            count = c()
        else:
            count = len(queryset)
        cache.set(redis_key, count, timeout=CACHE_TTL)
        return count
