from unittest import TestCase

from metagenedb.common.utils import dict


class TestExtractLabelsAndValues(TestCase):

    def setUp(self):
        self.dict_ints = {
            'a': 3,
            'b': 1,
            'c': 5
        }
        self.dict_str = {
            'a': 'toto',
            'b': 'abc',
            'c': 'king'
        }

    def test_method_int(self):
        expected_labels = ['a', 'b', 'c']
        expected_values = [3, 1, 5]
        tested_tuple = dict.extract_labels_and_values(self.dict_ints)
        self.assertListEqual(tested_tuple[0], expected_labels)
        self.assertListEqual(tested_tuple[1], expected_values)

    def test_method_int_sort(self):
        expected_labels = ['b', 'a', 'c']
        expected_values = [1, 3, 5]
        tested_tuple = dict.extract_labels_and_values(self.dict_ints, sort=True)
        self.assertListEqual(tested_tuple[0], expected_labels)
        self.assertListEqual(tested_tuple[1], expected_values)

    def test_method_int_sort_reverse(self):
        expected_labels = ['c', 'a', 'b']
        expected_values = [5, 3, 1]
        tested_tuple = dict.extract_labels_and_values(self.dict_ints, sort=True, reverse=True)
        self.assertListEqual(tested_tuple[0], expected_labels)
        self.assertListEqual(tested_tuple[1], expected_values)

    def test_method_str_sort(self):
        expected_labels = ['b', 'c', 'a']
        expected_values = ['abc', 'king', 'toto']
        tested_tuple = dict.extract_labels_and_values(self.dict_str, sort=True)
        self.assertListEqual(tested_tuple[0], expected_labels)
        self.assertListEqual(tested_tuple[1], expected_values)
