from unittest import TestCase

import mock
from django.conf import settings

from metagenedb.common.utils.external_api.kegg_api import GetFunctionExternalInfo


class TestGetFunctionExternalInfo(TestCase):

    def test_get_details_unknown_source(self):
        with self.assertRaises(NotImplementedError):
            external_info_retriever = GetFunctionExternalInfo("test_id", "unknown")
            external_info_retriever.get_details()

    def test_get_details_kegg(self):
        test_url = "http://test.com/"
        test_id = "id123"
        test_last_url = "http://test.com/request_url"
        with mock.patch('metagenedb.common.utils.external_api.kegg_api.KEGGAPI') as MockKEGGAPI:
            MockKEGGAPI.return_value.get.return_value.dict.return_value = {"info": "some_info"}
            MockKEGGAPI.return_value.url = test_url
            MockKEGGAPI.return_value.last_url_requested = test_last_url
            expected_dict = {
                'info': 'some_info',
                settings.API_KEY_ADDITIONAL_INFO: {
                    'comment': f"Information retrieved from external source: {test_url}",
                    'url': test_last_url
                }
            }
            external_info_retriever = GetFunctionExternalInfo(test_id, "kegg")
            self.assertDictEqual(external_info_retriever.get_details(), expected_dict)
