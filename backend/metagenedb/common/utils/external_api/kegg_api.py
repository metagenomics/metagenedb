import logging

from django.conf import settings

from dabeplech import KEGGAPI


logger = logging.getLogger(__name__)


class GetFunctionExternalInfo:

    def __init__(self, function_id, source):
        self.function_id = function_id
        self.source = source

    def _get_unknown_source(self):
        raise NotImplementedError("No source of information for %s from %s" % (self.function_id, self.source))

    def _get_kegg(self):
        """
        Get detailed information from KEGG orthology through Togows.
        """
        logger.info("Retrieving information from KEGG API")
        kegg_api = KEGGAPI()
        response = kegg_api.get(self.function_id).dict()
        response[settings.API_KEY_ADDITIONAL_INFO] = {
            'comment': f"Information retrieved from external source: {kegg_api.url}",
            'url': f"{kegg_api.last_url_requested}"
        }
        return response

    def get_details(self):
        return getattr(self, f"_get_{self.source}", self._get_unknown_source)()
