def extract_labels_and_values(classic_dict, sort=False, reverse=False):
    labels = []
    values = []
    if sort is True:
        classic_dict = {k: v for k, v in sorted(classic_dict.items(), key=lambda item: item[1], reverse=reverse)}
    for k, v in classic_dict.items():
        labels.append(k)
        values.append(v)
    return (labels, values)
