from django.core.paginator import Paginator
from django.utils.functional import cached_property

from metagenedb.common.utils.cache import queryset_count_cached


class CachedCountPaginator(Paginator):

    @cached_property
    def count(self):
        """Return the total number of objects, across all pages."""
        return queryset_count_cached(self.object_list)
