from rest_framework.permissions import BasePermission


class ListAndRetrieveAll(BasePermission):
    """
    Custom permission to only allow access to lists for admins
    """

    def has_permission(self, request, view):
        return view.action in ['list', 'retrieve'] or request.user.is_staff
