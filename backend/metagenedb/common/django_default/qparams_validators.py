from marshmallow import Schema, fields


class PaginatedQueryParams(Schema):
    page = fields.Integer()
    page_size = fields.Integer()
