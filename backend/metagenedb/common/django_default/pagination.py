from rest_framework.pagination import PageNumberPagination

from metagenedb.common.django_default.paginator import CachedCountPaginator


class DefaultPageNumberPagination(PageNumberPagination):
    django_paginator_class = CachedCountPaginator
    page_size_query_param = 'page_size'
    max_page_size = 5000
