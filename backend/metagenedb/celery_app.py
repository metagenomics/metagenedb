import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'metagenedb.settings')
app = Celery('metagenedb')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'display_time-20-seconds': {
        'task': 'demoapp.tasks.display_time',
        'schedule': 20.0
    },
}
