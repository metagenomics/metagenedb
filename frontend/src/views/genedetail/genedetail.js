import axios from 'axios';
import EggNogCard from '@/components/eggnogcard/eggnogcard.vue';
import KeggCard from '@/components/keggcard/keggcard.vue';
import TaxonomyCard from '@/components/taxonomycard/taxonomycard.vue';
import SimpleListing from '@/components/listing/simplelisting/simplelisting.vue';

export default {
  components: { KeggCard, TaxonomyCard, EggNogCard, SimpleListing },
  name: 'gene-detail',
  data() {
    return {
      geneId: '',
      geneDetail: [],
      sequence: '',
      keggIds: [],
      taxonomyId: '',
      eggnogIds: [],
      notFound: false,
    };
  },
  computed: {
    sourceUrls() {
      return {
        'igc': 'https://db.cngb.org/microbiome/genecatalog/genecatalog_human/',
        'virgo': 'https://virgo.igs.umaryland.edu/',
        'undef': '',
      }
    },
    labelUrls() {
      return {
        'igc': 'Visit IGC website',
        'virgo': 'Visit Virgo website',
        'undef': '',
      }
    },
  },
  mounted() {
    this.geneId = this.$route.params.id;
    this.getGeneDetail();
  },
  methods: {
    getGeneDetail() {
      axios.get(`/api/catalog/v1/genes/${this.geneId}`, {
        headers: {
          Accept: 'application/json',
        },
      })
        .then((response) => {
          this.geneDetail = [
            {
              title: 'ID',
              content: response.data.gene_id,
            },
            {
              title: 'Name',
              content: response.data.name,
            },
            {
              title: 'Length (bp)',
              content: response.data.length,
            },
            {
              title: 'Source',
              content: response.data.source,
              url: this.sourceUrls[response.data.source],
              url_label: this.labelUrls[response.data.source],
            },
          ];
          if (response.data.sequence) {
            this.sequence = '>' + response.data.gene_id + '\n' + response.data.sequence;
          }
          if (response.data.functions.length > 0) {
            response.data.functions.forEach(funcAnnot => {
              if (funcAnnot.source == 'kegg') {
                this.keggIds.push(funcAnnot.function_id);
              }
              else if (funcAnnot.source == 'eggnog') {
                this.eggnogIds.push(funcAnnot.function_id);
              }
            })

          } else {
            this.keggId = '';
          }
          if (response.data.taxonomy) {
            this.taxonomyId = response.data.taxonomy.tax_id;
          } else {
            this.taxonomyId = '';
          }
        })
        .catch((error) => {
          console.log(error);
          this.notFound = true;
        });
    },
    copyFasta(){
      var copyText = document.getElementById("fastaseq");
      copyText.select();
      document.execCommand("copy");
    }
  },
};