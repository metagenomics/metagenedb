import axios from 'axios';
import Doughnut from '@/components/graphs/doughnut/doughnut.vue';


export default {
  name: 'compare',
  data() {
    return {
      // Gene sources
      firstGeneSource: 'IGC',
      secondGeneSource: 'Virgo',
      // Taxonomy repartition
      firstTaxoCounts: {},
      secondTaxoCounts: {},
      // Gene counts
      firstGeneCounts: null,
      secondGeneCounts: null,
      // Requests status
      firstRequestDone: false,
      secondRequestDone: false,
      taxLevel: 'phylum',
    };
  },
  computed: {
    selectLevel() {
      return ['kingdom', 'superkingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species'];
    },
    geneSources() {
      return [
        'All', 'IGC', 'Virgo'
      ]
    },
  },
  mounted() {
    this.getAllStats();
  },
  methods: {
    getTaxoCounts(endpoint, objName, statusName) {
      axios.get(endpoint.toLowerCase(), {
        headers: {
          Accept: 'application/json',
        },
      })
      .then((response) => {
        this[objName] = {
          data: response.data.body.counts,
          labels: response.data.body.labels,
          colors: response.data.body.colors,
          level: this.taxLevel,
        };
      })
      .catch((error) => {
        console.error(error);
      }).then(() => this[statusName] = true);
    },
    getFirstTaxoCounts() {
      var endpoint = `/api/catalog/v1/statistics/genestatistics-${this.firstGeneSource}-taxonomy-repartition-${this.taxLevel}`;
      this.getTaxoCounts(endpoint, 'firstTaxoCounts', 'firstRequestDone');
    },
    getSecondTaxoCounts() {
      var endpoint = `/api/catalog/v1/statistics/genestatistics-${this.secondGeneSource}-taxonomy-repartition-${this.taxLevel}`;
      this.getTaxoCounts(endpoint, 'secondTaxoCounts', 'secondRequestDone');
    },
    getGeneCountsGeneric(stats_id, geneCountObj) {
      axios.get('/api/catalog/v1/statistics/' + stats_id.toLowerCase(), {
        headers: {
          Accept: 'application/json',
        },
      })
        .then((response) => {
          this[geneCountObj] = response.data.body.count;
        })
        .catch((error) => {
          console.error(error);
        });
    },
    getFirstGeneCounts() {
      this.getGeneCountsGeneric(
        `genestatistics-${this.firstGeneSource}-count-all`,
        'firstGeneCounts');
    },
    getSecondGeneCounts() {
      this.getGeneCountsGeneric(
        `genestatistics-${this.secondGeneSource}-count-all`,
        'secondGeneCounts');
    },
    getAllFirstStats() {
      this.getFirstGeneCounts();
      this.getFirstTaxoCounts();
    },
    getAllSecondStats() {
      this.getSecondGeneCounts();
      this.getSecondTaxoCounts();
    },
    getAllStats() {
      this.getAllFirstStats();
      this.getAllSecondStats();
    },
  },
  components: {
    doughnut: Doughnut,
  },
};