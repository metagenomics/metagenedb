import Logos from '@/components/logos/logos.vue';

export default {
  components: {
    logos: Logos,
  },
  data() {
    return {
      geneId: '',
    }
  },
  computed: {
    menu() {
      return [
        { title: 'Catalog Statistics', icon: 'far fa-chart-bar', route: '/stats' },
        { title: 'Compare Taxonomy', icon: 'far fa-clone', route: '/compare' },
        { title: 'List genes', icon: 'format_list_bulleted', route: '/genes' },
        { title: 'About', icon: 'contact_support', route: '/about' },
        { title: 'Contact us', icon: 'fas fa-address-book'}
      ];
    },
    disableGo() {
      if (this.geneId) {
        return false;
      }
      return true;
    },
    geneIdLower() {
      return this.geneId.toLowerCase();
    }
  },
  methods: {
    goToGeneDetail() {
      this.$router.push({
        path: `/gene-detail/${this.geneIdLower}`
      })
    }
  }
}