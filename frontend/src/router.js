import Vue from 'vue';
import Router from 'vue-router';

import About from '@/views/about/about.vue';
import Catalogstats from '@/views/stats/stats.vue';
import Compare from '@/views/compare/compare.vue'
import GeneDetail from '@/views/genedetail/genedetail.vue';
import Genes from '@/views/genes/genes.vue';
import Home from '@/views/home/home.vue';

Vue.use(Router);

const appTitle = process.env.VUE_APP_TITLE;

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        auth: true,
        title: `${appTitle} - Home`,
      }
    },
    {
      path: '/compare',
      name: 'compare',
      component: Compare,
    },
    {
      path: '/stats',
      name: 'stats',
      component: Catalogstats,
    },
    {
      path: '/genes',
      name: 'genes',
      component: Genes,
    },
    {
      path: '/gene-detail/:id',
      name: 'gene-detail',
      component: GeneDetail,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
  ],
});
