import axios from 'axios';
import SimpleExpand from '@/components/listing/simpleexpand/simpleexpand.vue';
import SimpleListing from '@/components/listing/simplelisting/simplelisting.vue';


export default {
  components: {
    SimpleExpand, SimpleListing,
  },
  name: 'KeggCard',
  props: {
    keggId: String,
    color: String,
  },
  mounted() {
    this.getKeggDetail();
  },
  data() {
    return {
      keggDetails: [],
      keggExpandDetails: [],
      keggReferences: [],
      requestDone: false,
    };
  },
  methods: {
    getKeggDetail() {
      if (this.keggId == 'no_kegg') {
        this.requestDone = true;
        return;
      }
      axios.get(`/api/catalog/v1/kegg-orthologies/${this.keggId}?detailed=true`, {
        headers: {
          Accept: 'application/json',
        },
      })
        .then((response) => {
          this.buildKeggDetails(response);
          this.buildKeggExpandDetails(response);
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
          this.requestDone = true;
        });
    },
    buildKeggDetails(response) {
      this.keggDetails = [
        {
          title: 'ID',
          content: response.data.entry_id,
          url: `https://www.genome.jp/dbget-bin/www_bget?ko:${response.data.entry_id}`,
          url_label: 'Open in KEGG'
        },
        {
          title: 'Name(s)',
          content: response.data.names.join(),
        },
        {
          title: 'Definition',
          content: response.data.definition,
        },
      ];
    },
    buildKeggExpandDetails(response) {
      this.keggExpandDetails = [
        this.buildEcNumbers(response),
        this.buildPathways(response),
        this.buildDiseases(response),
        this.buildModules(response),
        this.buildReferences(response),
      ];
    },
    buildEcNumbers(response) {
      var ec_numbers = {
        title: 'EC numbers',
        icon: 'fas fa-exchange-alt',
        content: [],
      };
      if(response.data.ec_numbers) {
        Object.entries(response.data.ec_numbers).forEach(([key, value]) => {
          var link_id = value.split('.-')[0]
          ec_numbers.content.push(
            {
              id: value,
              url: `https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=${link_id}`,
              url_label: "Open in IntEnz",
            },
          );
        });
      }
      return ec_numbers;
    },
    buildPathways(response) {
      var pathways = {
        title: 'Pathways',
        icon: 'fas fa-bezier-curve',
        content: [],
      };
      if(response.data.pathways) {
        Object.entries(response.data.pathways).forEach(([key, value]) => {
          pathways.content.push(
            {
              id: key,
              name: value,
              url: `https://www.genome.jp/kegg-bin/show_pathway?${key}+${this.keggId}`,
              url_label: "Open in KEGG",
              fetch: true,
            },
          );
        });
      }
      return pathways;
    },
    buildModules(response) {
      var modules = {
        title: 'Modules',
        icon: 'fas fa-bezier-curve',
        content: [],
      };
      if(response.data.modules) {
        Object.entries(response.data.modules).forEach(([key, value]) => {
          modules.content.push(
            {
              id: key,
              name: value,
              url: `https://www.genome.jp/kegg-bin/show_module?${key}+${this.keggId}`,
              url_label: "Open in KEGG"
            },
          );
        });
      }
      return modules;
    },
    buildDiseases(response) {
      var diseases = {
        title: 'Diseases',
        icon: 'fas fa-laptop-medical',
        content: [],
      };
      if(response.data.diseases) {
        Object.entries(response.data.diseases).forEach(([key, value]) => {
          diseases.content.push(
            {
              id: key,
              name: value,
              url: `https://www.genome.jp/dbget-bin/www_bget?ds:${key}`,
            },
          );
        });
      }
      return diseases;
    },
    buildReferences(response) {
      var references = {
        title: 'References',
        icon: 'fas fa-book-open',
        content: [],
      };
      if (response.data.references) {
        for (let i = 0; i < response.data.references.length; i++) {
          var url = null
          if (response.data.references[i].doi){
            var url = `https://doi.org/${response.data.references[i].doi}`;
            var url_label = "Open in doi.org";
          }
          else if (response.data.references[i].pubmed_id) {
            var url = `https://www.ncbi.nlm.nih.gov/pubmed/${response.data.references[i].pubmed_id}`;
            var url_label = "Open in Pubmed";
          }
          if (url) {
            references.content.push(
              {
                id: response.data.references[i].title,
                name: `${response.data.references[i].authors[0]} et al. ${response.data.references[i].journal}`,
                url: url,
                url_label: url_label,
              },
            );
          }
          else {
            references.content.push(
              {
                id: response.data.references[i].title,
                name: `${response.data.references[i].authors[0]} et al. ${response.data.references[i].journal}`,
              },
            );
          }
        };
      }
      return references;
    },
  },
};