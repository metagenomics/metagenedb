import Chart from 'chart.js';

export default {
  props: {
    histoData: {
      type: Object,
      required: true,
    },
    chartId: String,
    requestDone: Boolean,
  },
  data() {
    return {
      myChart: {},
      options: {},
      noGraph: true,
    };
  },
  methods: {
    createChart() {
      const ctx = document.getElementById(this.chartId);
      ctx.height = 120;
      this.myChart = new Chart(ctx, {
        type: 'bar',
      });
    },
    updateChart() {
      this.updateChartData();
      this.updateChartOptions();
    },
    updateChartOptions() {
      this.options = {
        legend: {
          position: 'top',
          labels: {
            boxWidth: 12,
            padding: 8,
          },
        },
      };
      this.myChart.options = this.options;
      this.myChart.update();
    },
    updateChartData() {
      this.myChart.data = {
        labels: this.histoData.labels,
        datasets: this.histoData.datasets,
      };
      this.myChart.update();
    },
  },
  watch: {
    histoData(val) {
      for (const data of val.datasets) {
        if (Object.keys(data).length === 0) {
          return;
        }
      }
      if (this.noGraph) {
        this.noGraph = false;
        this.createChart();
      }
      this.updateChart();
    },
  },
};