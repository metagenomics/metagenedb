export default {
    computed: {
      pasteurLogo() {
        return {
          'src': require("/app/public/img/logos/pasteur.png"),
          'alt': 'Institut Pasteur'
        };
      },
      bpiLogo() {
        return {
          'src': require("/app/public/img/logos/bpifrance.png"),
          'alt': 'BPI France'
        }
      },
      elvesysLogo() {
        return {
          'src': require("/app/public/img/logos/elvesys.png"),
          'alt': 'Elvesys'
        }
      },
      riskInPregnancyLogo() {
        return {
          'src': require("/app/public/img/logos/riskinpregnancy.png"),
          'alt': 'Risk in Pregnancy'
        }
      },
      aphpLogo() {
        return {
          'src': require("/app/public/img/logos/aphp.png"),
          'alt': 'APHP'
        }
      },
      insermLogo() {
        return {
          'src': require("/app/public/img/logos/inserm.png"),
          'alt': 'Inserm'
        }
      },
      uniParisLogo() {
        return {
          'src': require("/app/public/img/logos/uniparis.png"),
          'alt': 'Université de Paris'
        }
      },
      bforcureLogo() {
        return {
          'src': require("/app/public/img/logos/bforcure.png"),
          'alt': 'B for Cure'
        }
      },
      mediumLogos() {
        return [
          this.pasteurLogo,
          this.aphpLogo,
          this.uniParisLogo,
          this.insermLogo,
        ]
      },
      smallLogos() {
        return [
          this.bforcureLogo,
          this.riskInPregnancyLogo,
          this.bpiLogo,
        ]
      }
    }
  }