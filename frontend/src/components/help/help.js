export default {
  data () {
    return {
      dialog: false,
    }
  },
  computed: {
    help_links() {
      return [
        {
          title: 'API documentation',
          info: 'Documentation about different endpoints of metageneDB API',
          icon: 'fas fa-code',
          url: '/api/swagger',
        },
        {
          title: 'GitLab repository',
          info: 'Access to source code of the project',
          icon: 'fab fa-gitlab',
          url: 'https://gitlab.pasteur.fr/metagenomics/metagenedb',
        },
      ];
    },
  }
}