export default {
  name: 'SimpleListing',
  props: {
    listData: Array,
    color: String,
  },
  computed: {
    colors() {
      return {
        lighten_4: `${this.color} lighten-4`,
        lighten_5: `${this.color} lighten-5`,
      }
    }
  }
};