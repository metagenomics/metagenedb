export default {
  props: {
    title: {
      type: String,
      required: true,
    },
    text: String,
    color: {
      type: String,
      required: true,
    },
  },
};