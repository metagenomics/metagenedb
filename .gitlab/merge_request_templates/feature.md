## Description

*Description of the new feature (if different from the one described in the corresponding issue)*

## Definition of Done

* [ ] New code added is tested (*only apply for Python at the moment*)
* [ ] Merge request has been revised and validated

## Related issue(s)

Closes *Issue number (e.g. #35)*
