#!/bin/sh

yum install -y gettext

# Secrets
## Gitlab registry
kubectl delete secret registry-gitlab -n ${NAMESPACE} --ignore-not-found=true
kubectl create secret docker-registry -n ${NAMESPACE} registry-gitlab --docker-server=registry-gitlab.pasteur.fr --docker-username=${DEPLOY_USER} --docker-password=${DEPLOY_TOKEN} --docker-email=kubernetes@pasteur.fr
## SECRET_KEY for Django
kubectl delete secret backend-secret -n ${NAMESPACE} --ignore-not-found=true
kubectl create secret generic backend-secret -n ${NAMESPACE} --from-literal=secret_key=${SECRET_KEY}
## Credentials for postgres
kubectl delete secret postgresql-credentials -n ${NAMESPACE} --ignore-not-found=true
kubectl create secret generic postgresql-credentials -n ${NAMESPACE} --from-literal=username=${POSTGRES_USER} --from-literal=password=${POSTGRES_PASSWORD} --from-literal=database=${POSTGRES_DB}
## Cron admin user
kubectl delete secret backend-cron-credentials -n ${NAMESPACE} --ignore-not-found=true
kubectl create secret generic backend-cron-credentials -n ${NAMESPACE} --from-literal=username=${CRON_USER} --from-literal=password=${CRON_PASSWORD}

# Deployement
## DB
envsubst < ci/kubernetes/postgresql.yaml | kubectl apply -n ${NAMESPACE} -f -
kubectl -n ${NAMESPACE} wait --for=condition=available --timeout=600s deployment/postgresql
## Redis
envsubst < ci/kubernetes/redis.yaml | kubectl apply -n ${NAMESPACE} -f -
kubectl -n ${NAMESPACE} wait --for=condition=available --timeout=600s deployment/redis
## Backend
envsubst < ci/kubernetes/backend.yaml | kubectl apply -n ${NAMESPACE} -f -
kubectl -n ${NAMESPACE} patch deployment backend -p "{\"spec\":{\"template\":{\"metadata\":{\"labels\":{\"date\":\"`date +'%s'`\"}}}}}"
## Celery worker
envsubst < ci/kubernetes/celery-worker.yaml | kubectl apply -n ${NAMESPACE} -f -
kubectl -n ${NAMESPACE} patch deployment celery-worker -p "{\"spec\":{\"template\":{\"metadata\":{\"labels\":{\"date\":\"`date +'%s'`\"}}}}}"
## Frontend
envsubst < ci/kubernetes/frontend.yaml | kubectl apply -n ${NAMESPACE} -f -
kubectl -n ${NAMESPACE} patch deployment frontend -p "{\"spec\":{\"template\":{\"metadata\":{\"labels\":{\"date\":\"`date +'%s'`\"}}}}}"
## Ingress
envsubst < ci/kubernetes/ingress.yaml | kubectl apply -n ${NAMESPACE} -f -

# Cron jobs
envsubst < ci/kubernetes/cronjob.yaml | kubectl apply -n ${NAMESPACE} -f -
