#!/bin/sh

docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY

# Build backend
docker build -t "$CI_REGISTRY_IMAGE/backend:${CI_COMMIT_REF_NAME}" backend/
docker tag "$CI_REGISTRY_IMAGE/backend:${CI_COMMIT_REF_NAME}" "$CI_REGISTRY_IMAGE/backend:latest"
docker push "$CI_REGISTRY_IMAGE/backend:${CI_COMMIT_REF_NAME}"

# Build image to serve static from django
docker build -t "$CI_REGISTRY_IMAGE/django-static:${CI_COMMIT_REF_NAME}" nginx/prod_django_static/
docker tag "$CI_REGISTRY_IMAGE/django-static:${CI_COMMIT_REF_NAME}" "$CI_REGISTRY_IMAGE/django-static:latest"
docker push "$CI_REGISTRY_IMAGE/django-static:${CI_COMMIT_REF_NAME}"
