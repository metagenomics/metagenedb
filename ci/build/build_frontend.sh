#!/bin/sh

# First create .env for build stage
echo VUE_APP_GIT_COMMIT=${CI_COMMIT_SHORT_SHA} > frontend/.env
echo VUE_APP_TITLE=${VUE_APP_TITLE} >> frontend/.env

docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
docker build -t "$CI_REGISTRY_IMAGE/frontend:${CI_COMMIT_REF_NAME}" frontend/
docker tag "$CI_REGISTRY_IMAGE/frontend:${CI_COMMIT_REF_NAME}" "$CI_REGISTRY_IMAGE/frontend:latest"
docker push "$CI_REGISTRY_IMAGE/frontend:${CI_COMMIT_REF_NAME}"
